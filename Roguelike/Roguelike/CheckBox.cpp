#include "CheckBox.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


CheckBox::CheckBox(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	currentTexture = 0;
	clicked = false;
}

CheckBox::~CheckBox()
{
}


 
void CheckBox::update()
{
	//checks for mouseover
	if (!status)
	{
		//TODO: REMOVE MAGIC NUMBERS
		if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
		{
			currentTexture = 1;
		}
		else if (clicked)
		{
			currentTexture = 3;
		}
		else
		{
			currentTexture = 0;
		}
	}
	else
	{
		if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
		{
			currentTexture = 3;
		}
		else if (clicked)
		{
			currentTexture = 1;
		}
		else
		{
			currentTexture = 2;
		}
	}
	const char * tempName = name.c_str();
	//post a message with the name of the check box and if it is currently true or false to run function based on the state. 
	this->postMessage(Message(this, tempName, status));
}

bool CheckBox::checkMouseClickLeft()
{
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		clicked = true;
		return clicked;
	}
	
	clicked = false;
	return clicked;
}

bool CheckBox::checkMouseClickRight()
{
	return false;
}

bool CheckBox::checkMouseRelease()
{
	clicked = false;
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		if (status)
		{
			status = false;
		}
		else
		{
			status = true;
		}
		return true;
	}
	return false;
}


void CheckBox::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSizeX, textureAtlasSizeY));
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadInt(U_TEXTURE1, 1);
	shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
	shader->loadFloat(U_SCALE, 1.5f);
	texture->bind(0);
	mesh->draw();
}

void CheckBox::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	
	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}
	
	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("defaultStatus"))
	{
		this->originNode->FirstChildElement("defaultStatus")->QueryBoolText(&defaultStatus);
	}
	else if (xmlDebug)
	{
		printf("Missing defaultStatus\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->texture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (this->originNode->FirstChildElement("textureAtlasSizeX"))
	{
		this->originNode->FirstChildElement("textureAtlasSizeX")->QueryIntText(&textureAtlasSizeX);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeX\n");
	}

	if (this->originNode->FirstChildElement("textureAtlasSizeY"))
	{
		this->originNode->FirstChildElement("textureAtlasSizeY")->QueryIntText(&textureAtlasSizeY);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeY\n");
	}

	transform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
	status = defaultStatus;
}

void CheckBox::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

GuiState* CheckBox::getParent()
{
	return parentGuiState;
}

std::string CheckBox::getName()
{
	return name;
}
