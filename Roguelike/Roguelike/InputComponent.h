//#pragma once
//#include "IInputComponent.h"
//#include "Observable.h"
//#include "Timer.h"
///**
// * @class	InputComponent
// *
// * @brief	An input component. the messages received come from the sdl eventhandler
// * 			
// */
//class InputComponent : public IInputComponent
//{
//public:
//	InputComponent(tinyxml2::XMLElement *xmlElement);
//	virtual ~InputComponent() ;
//	void init(Engine* parentEngine) override;
//	void update(float deltaTime) override 
//	void receiveMessage(const Message &message);
//	void receiveNetworkInput(InputValues newInput);
//	InputValues getInputValues();
//public:
//};
//
