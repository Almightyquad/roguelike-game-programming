#pragma once

#include <vector>
#include <algorithm>

//Forward-declare the Message class and the Receiver interfaces
class Message;
class IReceiver;

//Declare the Observable interface
class IObservable
{
public:
	virtual ~IObservable();
	
	void addSubscriber(IReceiver* receiver);
	void removeSubscriber(IReceiver* receiver);
	
protected:
	void postMessage(const Message &msg) const;
	
private:
	std::vector<IReceiver*> observerList;
};

inline IObservable::~IObservable() {}

