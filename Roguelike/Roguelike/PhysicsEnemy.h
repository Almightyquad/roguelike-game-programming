#pragma once
#include "PhysicsComponent.h"

class PhysicsEnemy final : public PhysicsComponent
{
public:
	PhysicsEnemy(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};