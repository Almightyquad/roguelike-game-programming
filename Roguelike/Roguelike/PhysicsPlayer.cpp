#include "PhysicsPlayer.h"
#include "Actor.h"
#include "Engine.h"
void PhysicsPlayer::onCollision(int fixtureUserData, Actor * collidedActor)
{
	int damage;
	switch (fixtureUserData)
	{
	case CATEGORY_PICKUP:
		if (parentEngine->getJoining())
		{
			return;
		}
		if (dynamic_cast<IPickUpComponent*>(collidedActor->getComponent("PickUpComponent"))->checkSavable())
		{
			if (dynamic_cast<InventoryComponent*>(parentActor->getComponent("InventoryComponent"))->checkForFreeSpace())
			{
				dynamic_cast<InventoryComponent*>(parentActor->getComponent("InventoryComponent"))->addItem(collidedActor);
			}
			else
			{
				
			}
		}
		else
		{
			collidedActor->onDeath();
			dynamic_cast<IPickUpComponent*>(collidedActor->getComponent("PickUpComponent"))->onPickup(parentActor);	
			parentEngine->getActorFactory()->addToDeleteQueue(collidedActor);
		}
		break;
	case CATEGORY_ENEMY_PROJECTILE:
	case CATEGORY_ENEMY:
		damage = dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->getDamage();
		dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->reduceHealth(damage);
		if (parentActor == parentEngine->getPlayer())
		{
			parentEngine->getGuiHandler()->setBarValue("healthBar", static_cast<float>(dynamic_cast<CombatComponent*>(parentActor->getComponent("CombatComponent"))->getHealth()));
		}	
		break;
	default:
		break;
	}
}
