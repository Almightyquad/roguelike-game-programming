#pragma once
#include "LevelLoader.h"
#include <glm\glm.hpp>
#include <array>
#include <vector>
#include "Handler.h"

//TODO: Scrap the entire ChunkHandler and LevelLoader and write it from scratch
//Use JSONs to load it.

class ChunkHandler : public Handler
{
public:
	ChunkHandler();
	~ChunkHandler();
	void init();
	void generateLevel();
	LevelLoader & getLevelLoader();
private:
	static const int CHUNKS = 3;
	std::array<std::array<int, CHUNKS>, CHUNKS> chunk;
	std::vector<int> levels;
	

	LevelLoader levelLoader;
};

