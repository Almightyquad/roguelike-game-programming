#include "PickUpCreator.h"
#include "PickUpFactory.h"

PickUpCreator::PickUpCreator(const std::string & classname, PickUpFactory* pickUpFactory)
{
	pickUpFactory->registerIt(classname, this);
}