#pragma once
#include "ActorComponent.h"
/**
 * @class	IPhysicsComponent
 *
 * @brief	The physics component interface. Used by most game objects
 */
class IPhysicsComponent : public ActorComponent
{
public:
	IPhysicsComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	//virtual ~IPhysicsComponent() = 0;
	//virtual void setPosition(glm::vec2 position) = 0;
	//virtual void applyForce() = 0;
	//virtual void createShape(int shapeIndex) = 0;
public:
	glm::vec3 position;
	glm::vec3 newPosition;

	float angle;
	float newAngle;
};

