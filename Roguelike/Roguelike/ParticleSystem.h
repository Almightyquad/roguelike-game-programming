#pragma once
#include <glm\glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <glu.h>
#else
#include <GL\glew.h>
#endif
#include <vector>
#include <array>
#include "Shader.h"
#include "Texture.h"
#include "Mesh.h"

class Engine;

class ParticleSystem
{
private:
	struct Particle
	{
		glm::vec3 velocity = { 0.f, 0.f, 0.f };
		float life = 1.f;
	};

public:
	/**
	 * @fn	ParticleSystem::ParticleSystem();
	 *
	 * @brief	Default constructor.
	 */
	ParticleSystem();

	/**
	 * @fn	ParticleSystem::~ParticleSystem();
	 *
	 * @brief	Destructor.
	 */
	~ParticleSystem();

	/**
	 * @fn	void ParticleSystem::init(Engine* engine, float size, Texture* texture, float maxVelocity, bool gravity);
	 *
	 * @brief	Initialises this object.
	 *
	 * @param [in]	engine 		Pointer to engine.
	 * @param	size		   	The size.
	 * @param [in]	texture		Pointer to texture.
	 * @param	maxVelocity	   	The maximum velocity.
	 * @param	gravity		   	true to gravity.
	 */
	void init(Engine* engine, float size, Texture* texture, float maxVelocity, bool gravity);

	/**
	 * @fn	void ParticleSystem::createPoint(int amount, glm::vec3 position, int maxLife);
	 *
	 * @brief	Creates a point.
	 *
	 * @param	amount  	The amount.
	 * @param	position	The position.
	 * @param	maxLife 	The maximum life.
	 */
	void createPoint(int amount, glm::vec3 position, int maxLife);

	/**
	 * @fn	void ParticleSystem::createPoint(float pps, float deltaTime, glm::vec3 position, int maxLife, glm::vec4 color, glm::vec3 velocity =
	 *
	 * @brief	Creates a point.
	 *
	 * @param	pps		 	Particles per second.
	 * @param	deltaTime	The delta time.
	 * @param	position 	The position.
	 * @param	maxLife  	The maximum life.
	 * @param	color	 	The color.
	 */
	void createPoint(float pps, float deltaTime, glm::vec3 position, int maxLife, glm::vec4 color, glm::vec3 velocity = { 0,0,0 });

	/**
	 * @fn	void ParticleSystem::draw(glm::mat4 view);
	 *
	 * @brief	Draws the given view.
	 *
	 * @param	view	The view projection.
	 */
	void draw(glm::mat4 view);

	/**
	 * @fn	virtual void ParticleSystem::update(float deltaTime);
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	virtual void update(float deltaTime);

protected:
	std::vector<Particle> particles;
	std::vector<glm::vec3> positions;
	std::vector<glm::vec4> colors;
	bool gravity;
	float maxVelocity;

private:
	float size;
	
	enum particleBuffers
	{
		VERTEX_VB,
		TEXTURE_VB,
		INDEX_VB,
		POSITION_VB,
		COLOR_VB,
		NUM_BUFFERS	
	};

	std::vector<glm::vec2> texCoords =
	{
		glm::vec2(0.f, 0.f),
		glm::vec2(0.f, 1.f),
		glm::vec2(1.f, 1.f),
		glm::vec2(1.f, 0.f)
	};

	std::array<GLfloat, 12> vertex;
	std::array<unsigned int, 4> indices;

	Texture *texture;
	Shader *shader;
	GLuint vertexArrayObject;
	GLuint vertexArrayBuffer[particleBuffers::NUM_BUFFERS];
};