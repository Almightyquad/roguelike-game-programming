#pragma once
#include "ActorFactory.h"
#include "ActorComponent.h"
#include "GraphicsComponent.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"
#include "InputComponent.h"
#include "IAIComponent.h"
#include "IPickUpComponent.h"
#include "CombatComponent.h"
#include "AudioComponent.h"
#include "InventoryComponent.h"
#include "ParticleComponent.h"
#include <unordered_map>
#include "Observable.h"

/**
 * @class	Actor
 *
 * @brief	The actor.
 * @details Each object in the game is an actor.
 * 			Actors have many different properties. Which properties they have is decided by the components defined in the xml file.
 * 			All components are independent.
 */
class Actor : public IObservable
{
public:
	Actor(tinyxml2::XMLElement *xmlElement);
	~Actor();
	
	void init(Engine* parentEngine, glm::vec3 position, glm::vec2 tilePosition = glm::vec2{0, 0});
	void update(float deltaTime);
	void onDeath();
	void onHit();
	void onSpawn();

	void setParent(ActorFactory& parentPtr);
	ActorComponent* getComponent(std::string componentId);
		

	actorID getActorId();
	void setActorId(actorID actorId);
	std::string getCategory();
	void setCategory(std::string category);

	void addComponentSubscriber(const char* messenger, const char* subscriber);
	void removeComponentSubscriber(const char* messenger, const char* subscriber);
	void setActive(bool flag);
	bool isActive();

private:
	krem::Vector2f position;
	ActorFactory* parentActorFactory;
	actorID actorId;
	std::string category;
	Actor* equippedWeapon;
	Engine* parentEngine;

	bool active;

	std::unordered_map<std::string, std::unique_ptr<ActorComponent>> actorComponentMap;
	void addChild(ActorComponent* actorComponent);
	tinyxml2::XMLElement *xmlElement;
};