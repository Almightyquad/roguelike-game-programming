#pragma once
#include <string>
#include "ActorComponent.h"
#include <iostream>


extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

class Actor;
//class ActorFactory;

/**
* @class	IAIComponent
*
* @brief	The interface for all AI components used by actors.
* @defails	All actors will be using lua combined with the physics engine to move around.
* 			A bullet will for instance use lua to know that it just moved forward every step.
*/
class IAIComponent : public ActorComponent
{
public:
	IAIComponent(tinyxml2::XMLElement *xmlElement) ;
	IAIComponent();
	virtual ~IAIComponent();
	

	virtual void init(Engine* parentEngine);
	virtual void update(float deltatime) = 0;
	virtual void onDeath() {};
	
	virtual void receiveMessage(const Message &message) {};
	
protected:
	Actor* player;
	std::string scriptPath;
	lua_State* luaState;
	int noLuaParameters;
	int noLuaReturnValues;
};