#include "InputCreator.h"
#include "InputFactory.h"

InputCreator::InputCreator(const std::string & classname, InputFactory* aIFactory)
{
	aIFactory->registerIt(classname, this);
}
