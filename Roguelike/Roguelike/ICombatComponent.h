#pragma once
#include "ActorComponent.h"
#include "Timer.h"
//class Timer;
/**
 * @class	ILifeComponent
 *
 * @brief	The life component interface. Used by all actors who have limited life.
 * @defails	Tracks whether the actor is alive.
 * 			Dies whenever health or timer reacher zero(whichever occurs first).
 */
class ICombatComponent : public ActorComponent
{
public:
	ICombatComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	virtual ~ICombatComponent(){};
protected:
	bool alive;
	int maxHealth;
	int maxMana;
	int health;
	int damage;
	int mana;
	float lifetime;
	Timer timer;
};

