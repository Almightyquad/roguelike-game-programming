#include "RandomSpawn.h"
#include "Engine.h"

RandomSpawn::RandomSpawn()
{
}

RandomSpawn::~RandomSpawn()
{
}

bool RandomSpawn::spawnRandomEnemy(std::string actorToSpawn)
{
	auto tempVec = parentEngine->getLevelLoader()->tilePositions;
	int tempRand = rand() % tempVec.size();;
	if (actorToSpawn != "")
	{
		parentEngine->getActorFactory()->createActor("res/actors/enemy/" + actorToSpawn + ".xml", glm::vec3(tempVec[tempRand].x, tempVec[tempRand].y, 0.f));
		return true;
	}
	return false;
}

bool RandomSpawn::spawnRandomItem(std::string actorToSpawn)
{
	auto tempVec = parentEngine->getLevelLoader()->tilePositions;
	int tempRand = rand() % tempVec.size();
	if (actorToSpawn != "")
	{
		parentEngine->getActorFactory()->createActor("res/actors/pickup/Pickup" + actorToSpawn + ".xml", glm::vec3(tempVec[tempRand].x, tempVec[tempRand].y, 0.f));
		return true;
	}
	return false;
}
