#include "ContactListener.h"
#include "Actor.h"

ContactListener::ContactListener()
{

}

void ContactListener::BeginContact(b2Contact * contact)
{
	bool horse = true;
	fixturePtrA = contact->GetFixtureA();
	fixturePtrB = contact->GetFixtureB();

	fixtureUserDataA = fixturePtrA->GetUserData();
	fixtureUserDataB = fixturePtrB->GetUserData();

	bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
	bodyUserDataB = fixturePtrB->GetBody()->GetUserData();

	bool aIsSensor = isSensor(fixtureUserDataA);
	bool bIsSensor = isSensor(fixtureUserDataB);

	if (!aIsSensor && !bIsSensor)
	{
		if ((int)(fixtureUserDataA) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataB)->onCollision((int)(fixtureUserDataA), nullptr);
		}
		else if ((int)(fixtureUserDataB) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->onCollision((int)(fixtureUserDataB), nullptr);
		}
		else
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->onCollision((int)(fixtureUserDataB), static_cast<PhysicsComponent*>(bodyUserDataB)->getParentActor());
			static_cast<PhysicsComponent*>(bodyUserDataB)->onCollision((int)(fixtureUserDataA), static_cast<PhysicsComponent*>(bodyUserDataA)->getParentActor());
		}
	}
	else
	{
		if (aIsSensor && ((int)(fixtureUserDataB) == CATEGORY_SCENERY))
		{
			bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataA)->startContact(fixtureUserDataA);
		}
		if (bIsSensor && ((int)(fixtureUserDataA) == CATEGORY_SCENERY))
		{
			bodyUserDataB = fixturePtrB->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataB)->startContact(fixtureUserDataB);
		}
	}
}


void ContactListener::EndContact(b2Contact * contact)
{
	fixturePtrA = contact->GetFixtureA();
	fixturePtrB = contact->GetFixtureB();
	fixtureUserDataA = fixturePtrA->GetUserData();
	fixtureUserDataB = fixturePtrB->GetUserData();


	if (isSensor(fixtureUserDataA) && ((int)(fixtureUserDataB) == CATEGORY_SCENERY))
	{
		bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
		static_cast<PhysicsComponent*>(bodyUserDataA)->endContact(fixtureUserDataA);
	}

	if (isSensor(fixtureUserDataB) && ((int)(fixtureUserDataA) == CATEGORY_SCENERY))
	{
		bodyUserDataB = fixturePtrB->GetBody()->GetUserData();
		static_cast<PhysicsComponent*>(bodyUserDataB)->endContact(fixtureUserDataB);
	}
}

bool ContactListener::isSensor(void * fixtureUserData)
{
	if (fixtureUserData == "jump")// || fixtureUserData == "wallLeft" || fixtureUserData == "wallRight") 
	{
		return true;
	}
	if (fixtureUserData == "wallLeft")
	{
		return true;
	}
	if (fixtureUserData == "wallRight")
	{
		return true;
	}
	return false;
}
