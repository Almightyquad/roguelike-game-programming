#pragma once
#include "IAIComponent.h"

class AIChuck final : public IAIComponent
{
public:
	AIChuck(tinyxml2::XMLElement *xmlElement)
		: IAIComponent(xmlElement){};
	~AIChuck();
	
	void update(float deltaTime) override;
	void onDeath();
private:
	//void(ActorFactory::*createActor) (const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity);
};
