#pragma once
#include "ActorComponent.h"
/**
 * @class	INetworkComponent
 *
 * @brief	Interface for network component. Protected has a bool for all components which need to be syncronized
 */
class INetworkComponent : public ActorComponent
{
public:
	
protected:
	bool AIComponent = false;
	bool AudioComponent = false;
	bool CombatComponent = true;
	//bool InputComponent; //doesn't really make sense
	//bool NetworkComponent; //doesn't really make sense
	bool PhysicsComponent = true;
	bool PickUpComponent = false;
};