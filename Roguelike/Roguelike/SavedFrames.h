#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include <list>
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "common.h"
#include <iostream>

class SavedFrames
{
public:
	SavedFrames();
	~SavedFrames();
	void setPosition(glm::vec3 position);
	void setRotation(float rotation);
	void setVelocity(glm::vec2 velocity);
	void setFrameSpawned(int frameSpawned);
	void MoveFrameSpawned();
	void removeTimeFrame(int timeFrame);
	glm::vec3 getPosition(int timeFrame);
	float getRotation(int timeFrame);
	glm::vec2 getVelocity(int timeFrame);
	int getFrameSpawned();
private:	
	int frameSpawned;
	std::vector<glm::vec3> positionVector;
	std::vector<float> rotationVector;
	std::vector<glm::vec2> velocityVector;
};

