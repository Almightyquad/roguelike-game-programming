#include "MousePicker.h"
#ifdef __ANDROID__
#define GLdouble GLfloat
#define glGetDoublev glGetFloatv
#endif
MousePicker::MousePicker()
{
}

MousePicker::~MousePicker()
{
}

void MousePicker::calculateMouseInSpace(float mouseX, float mouseY)
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	GLdouble modelview[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	GLdouble projection[16];
	glGetDoublev(GL_PROJECTION_MATRIX, projection);

	glm::vec2 mouse = { mouseX, mouseY };
	GLdouble winX, winY, winZ;
	winX = (float)mouse.x;
	winY = (float)mouse.y;
	winY = (float)viewport[3] - winY;
	glReadPixels((GLint)winX, (GLint)winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
	GLdouble posX, posY, posZ;
	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

	position = glm::vec3(posX, posY, posZ);
}

const glm::vec3 MousePicker::getPosition() const
{
	return position;
}