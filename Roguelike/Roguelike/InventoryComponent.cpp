#include "InventoryComponent.h"
#include "Engine.h"
#include "Actor.h"
#include "Message.h"


InventoryComponent::InventoryComponent(tinyxml2::XMLElement *xmlElement)
	: IInventoryComponent(xmlElement),
	activeItem(nullptr),
	preSetItems(false)
{
	containerString = "";
	drawable = false;
	dropable = false;
	firstFreeSlot = 0;
}


InventoryComponent::~InventoryComponent()
{
	if (this->originNode->FirstChildElement("containerString"))
	{
		containerString = this->originNode->FirstChildElement("containerString")->GetText();
		addSubscriber(this->parentEngine->getGuiHandler()->getContainer(containerString));
		//this->parentEngine->getGuiHandler()->getContainer(containerString)->addSubscriber(this);
	}
	items.clear();
}

void InventoryComponent::init(Engine * parentEngine)
{
	tinyxml2::XMLElement *itemElement;

	this->parentEngine = parentEngine;

	if (this->originNode->FirstChildElement("maxNumberOfItems"))
	{
		this->originNode->FirstChildElement("maxNumberOfItems")->QueryIntText(&maxNumberOfItems);
	}
	else if (xmlDebug)
	{
		printf("Missing maxSize\n");
	}
	if (this->originNode->FirstChildElement("containerString"))
	{
		containerString = this->originNode->FirstChildElement("containerString")->GetText();
		addSubscriber(this->parentEngine->getGuiHandler()->getContainer(containerString));
		//this->parentEngine->getGuiHandler()->getContainer(containerString)->addSubscriber(this);
	}
	else if (xmlDebug)
	{
		printf("Missing containerString\n");
	}
	if (this->originNode->FirstChildElement("dropable"))
	{
		this->originNode->FirstChildElement("dropable")->QueryBoolText(&dropable);
	}
	else if (xmlDebug)
	{
		printf("Missing dropable\n");
	}
	if (this->originNode->FirstChildElement("presetItems"))
	{
		std::string tempCheck;
		std::string tempItemName;
		preSetItems = true;
		itemElement = this->originNode->FirstChildElement("presetItems")->FirstChildElement("itemName");
		tempCheck = this->originNode->FirstChildElement("presetItems")->FirstChildElement("itemName")->Value();
		while (tempCheck == "itemName")
		{
			tempItemName = itemElement->GetText();
			Actor* newItem = parentEngine->getActorFactory()->createActor(("res/actors/Pickup/" + tempItemName + ".xml"), glm::vec3{ 0,0,0 });
			newItem->setActive(false);
			addItem(newItem);
			//parentEngine->getTextureHandler()->loadTexture("res/textures/pickups/" + tempItemName + ".png");
			//preSetItemsList.push_back(tempItemName);
			//preSetItemsList.emplace_back(newItem);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				tempCheck = itemElement->Value();
			}
			else
			{
				tempCheck = "end of itemNames";
			}
		}
	}
}

void InventoryComponent::update(float deltaTime)
{

}

bool InventoryComponent::checkForFreeSpace()
{
	if (items.size() < maxNumberOfItems)
	{
		return true;
	}
	return false;
}

void InventoryComponent::addItem(Actor * pickUpActor)
{
	//if (parentEngine->getJoining())
	//{
	//	return;
	//}
	if (items.size() < maxNumberOfItems)
	{
		bool newSlot = false;
		int i = 0;

		for (auto it : items)
		{
			if (it.first != i && !newSlot)
			{
				if (i <= firstFreeSlot)
				{
					newSlot = true;
					firstFreeSlot = i;
				}
			}
			i++;
		}
		if (!newSlot)
		{
			firstFreeSlot = i;
		}

		dynamic_cast<IPickUpComponent*>(pickUpActor->getComponent("PickUpComponent"))->onPickup(parentActor);
		items.insert(std::make_pair(firstFreeSlot, pickUpActor));
 		printf("Item added to new slot\n");
		if (containerString != "")
		{
			Texture* actorTexture = dynamic_cast<GraphicsComponent*>(pickUpActor->getComponent("GraphicsComponent"))->getTexture();
			//this sends its message to a container 
			//if (parentActor == parentEngine->getPlayer())
			{
				this->postMessage(Message(this, "addItemToContainer", actorTexture));
			}
			parentEngine->getActorFactory()->addToSwapActiveQueue(pickUpActor);
		}
	}
}

void InventoryComponent::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(Container))
	{
		const char * tempMessage = message.getVariable(0);
		if (strcmp(tempMessage, containerString.c_str()) == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "swap") == 0)
			{
				int slot1 = message.getVariable(2);
				int slot2 = message.getVariable(3);

				Actor* tempItem = items[slot1];
				//int tempStack = stacksNumbers[slot1];

				items[slot1] = items[slot2];
				//stacksNumbers[slot1] = stacksNumbers[slot2];

				items[slot2] = tempItem;
				//stacksNumbers[slot2] = tempStack;

			}
			if (strcmp(tempMessage, "move") == 0)
			{
				int item1 = message.getVariable(2);
				int item2 = message.getVariable(3);
				int i = 0;

				std::map<int, Actor*>::iterator it = items.find(item1);

				if (it != items.end())
				{
					Actor* temp = it->second;
					items.erase(it->first);

					items.insert(std::make_pair(item2, temp));
				}
			}
			if (strcmp(tempMessage, "boxActivated") == 0)
			{
				int slot = message.getVariable(2);

				for (auto it : items)
				{
					dynamic_cast<IPickUpComponent*>(it.second->getComponent("PickUpComponent"))->onDeactivation();
				}
				std::map<int, Actor*>::iterator it = items.find(slot);

				if (it != items.end())
				{
					dynamic_cast<IPickUpComponent*>(it->second->getComponent("PickUpComponent"))->onActivation();
				}

			}
			if (strcmp(tempMessage, "destroy") == 0)
			{
				int destroyItem = message.getVariable(2);

				std::map<int, Actor*>::iterator it = items.find(destroyItem);

				if (it != items.end())
				{
					dynamic_cast<IPickUpComponent*>(it->second->getComponent("PickUpComponent"))->onToss();
					dynamic_cast<IPickUpComponent*>(it->second->getComponent("PickUpComponent"))->onDeactivation();
					if (dropable)
					{
						glm::vec3 parentActorPosition = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
						dynamic_cast<PhysicsComponent*>(it->second->getComponent("PhysicsComponent"))->setPosition(parentActorPosition);
						it->second->setActive(true);
						dynamic_cast<IPickUpComponent*>(it->second->getComponent("PickUpComponent"))->restartSpawnTimer();
					}
					else
					{
						items.erase(it);	
					}
				}
			}
		}
	}
	if (message.getSenderType() == typeid(NetworkServerHandler))
	{
		if (static_cast<const char*>(message.getVariable(0)) == "mouseUpdate")
		{
			activeItem->fire(message.getVariable(1));
		}
		
	}
}

std::string InventoryComponent::getContainer()
{
	return containerString;
}

std::map<int, Actor*> InventoryComponent::getItems()
{
	return items;
}

void InventoryComponent::setActive(IPickUpComponent * itemToSetActive)
{
	activeItem = itemToSetActive;
}

