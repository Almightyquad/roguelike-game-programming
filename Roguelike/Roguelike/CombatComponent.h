#pragma once
#include "ICombatComponent.h"
/**
* @class	ILifeComponent
*
* @brief	A life component.
* @defails	Tracks whether the actor is alive.
* 			Dies whenever health or timer reacher zero(whichever occurs first).	
*/
class CombatComponent : public ICombatComponent
{
public:
	CombatComponent(tinyxml2::XMLElement *xmlElement);
	virtual ~CombatComponent();
	void init(Engine* parentEngine);
	void update(float deltaTime) override;
	void reduceHealth(int damage);
	void reduceMana(int manaCost);
	void increaseHealth(int healthValue);
	void increaseMana(int manaValue);
	
	int getMana();
	int getHealth();
	int getDamage();
	void kill();
public:
};

