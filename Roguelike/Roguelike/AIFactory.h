#pragma once
#include <map>
#include <string>
#include "AICreator.h"

class AIFactory
{
public:
	std::unique_ptr<IAIComponent> create(const std::string& itemName, tinyxml2::XMLElement* xmlElement);

	void registerIt(const std::string& itemName, AICreator* aICreator);

private:
	std::map<std::string, AICreator*> table;
};