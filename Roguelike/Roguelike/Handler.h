#pragma once
#include <memory>
class Engine;
/**
 * @class	Handler
 *
 * @brief	The base class for some handlers(hopefully all in the future).
 * @details Has basic funtions and variables which all handlers need to operate.
 */
class Handler
{
public:
	Handler() {};
	~Handler() {};
	virtual void update(float deltaSeconds) {};

	void setParent(Engine& parentPtr);
	Engine* getParent() {};
protected:
	Engine* parentEngine;
private:
};
