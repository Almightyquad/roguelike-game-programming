#pragma once
#include "IAudioComponent.h"

/**
 * @class	AudioComponent
 *
 * @brief	An audio component.
 */
class AudioComponent : public IAudioComponent
{
public:
	AudioComponent(tinyxml2::XMLElement *xmlElement);
	virtual ~AudioComponent();
	void init(Engine* parentEngine) override;
	void receiveMessage(const Message &message) override;
	void onSpawn() override;
	void onDeath() override;
	void onHit() override;
private:
	std::unordered_map<std::string, std::string> soundMap;
	std::unordered_map<std::string, std::string> musicMap;
};