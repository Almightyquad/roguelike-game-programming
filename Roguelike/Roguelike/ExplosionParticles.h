#pragma once
#include "ParticleSystem.h"

class ExplosionParticles : public ParticleSystem
{
public:
	ExplosionParticles()
		: ParticleSystem() {};

	/**
	 * @fn	void ExplosionParticles::update(float deltaTime) override;
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;
};