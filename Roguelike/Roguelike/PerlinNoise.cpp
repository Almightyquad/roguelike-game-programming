#include "PerlinNoise.h"

//static gradients in 2d for improved perlin
krem::Vector2i g2[] = 
{
	{1,0},{-1,0},
	{0,1},{ 0,-1 }
};

//static gradients in 3d for improved perlin
krem::Vector3i g3[] =
{
	{ 1,1,0 },{ -1,1,0 },{ 1,-1,0 },{ -1,-1,0 },
	{ 1,0,1 },{ -1,0,1 },{ 1,0,-1 },{ -1,0,-1 },
	{ 0,1,1 },{ 0,-1,1 },{ 0,1,-1 },{ 0,-1,-1 },
	//for padding
	{ 1,1,0 },{ -1,1,0 },{ 0,-1,1 },{ 0,-1,-1 }
};

float perlinNoise2d(float x, float y)
{
	return 0.0f;
}
