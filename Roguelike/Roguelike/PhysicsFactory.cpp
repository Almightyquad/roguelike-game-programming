#include "PhysicsFactory.h"

std::unique_ptr<IPhysicsComponent> PhysicsFactory::create(const std::string & itemName, tinyxml2::XMLElement* xmlElement)
{
	auto it = table.find(itemName);
	if (it != table.end())
	{
		return it->second->create(xmlElement);
	}
	else
	{
		return nullptr;
	}
}

void PhysicsFactory::registerIt(const std::string & itemName, PhysicsCreator * pickUpCreator)
{
	table[itemName] = pickUpCreator;
}