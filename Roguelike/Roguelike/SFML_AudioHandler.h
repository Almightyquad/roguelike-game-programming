#pragma once
#include "IAudioHandler.h"
//#include "SFML\Audio.hpp"

class SoundCache;

class SFML_AudioHandler : public IAudioManager
{
public:
	SFML_AudioHandler();
	~SFML_AudioHandler();
	virtual void playSound(std::string filepath) = 0;
	virtual void playMusic(std::string filepath) = 0;
	virtual void stopMusic() = 0;
	virtual void stopAll() = 0;
	virtual void setVolume(int volume) = 0;
	virtual void setPosition(float x, float y) = 0;
private:
//	SoundCache soundCache;
	/*sf::SoundBuffer soundBuffer;
	sf::Sound sound;
	sf::Music music;*/
};

class SoundCache
{

};