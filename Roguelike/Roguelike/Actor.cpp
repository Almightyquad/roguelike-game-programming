#include "Actor.h"
#include "Engine.h"
#include "Message.h"
#ifdef __ANDROID__
#include <SDL.h>
#define printf SDL_Log
#endif
Actor::Actor(tinyxml2::XMLElement *xmlElement):
	active(true),
	category(""),
	equippedWeapon(nullptr)
{

	this->xmlElement = xmlElement;
	
}

Actor::~Actor()
{
	removeComponentSubscriber("InputComponent", "PhysicsComponent");
	removeComponentSubscriber("InputComponent", "AIComponent");
	removeComponentSubscriber("PhysicsComponent", "GraphicsComponent");
	removeComponentSubscriber("PhysicsComponent", "CombatComponent");
	removeComponentSubscriber("AIComponent", "PhysicsComponent");
	removeComponentSubscriber("InputComponent", "AudioComponent");
	removeComponentSubscriber("AnimationComponent", "GraphicsComponent");
	removeComponentSubscriber("GraphicsComponent", "AnimationComponent");

	for (auto &it : actorComponentMap)
	{
		it.second.reset();
	}
	actorComponentMap.clear();
}

void Actor::init(Engine* parentEngine, glm::vec3 position, glm::vec2 tilePosition)
{
	this->parentEngine = parentEngine;
	//this->position = position;
	tinyxml2::XMLElement *componentElement;
	if (xmlElement->FirstChildElement("Components")->FirstChildElement("GraphicsComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding GraphicsComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("GraphicsComponent");
		std::unique_ptr<ActorComponent> actorComponent = std::make_unique<GraphicsComponent>(componentElement);
		addChild(actorComponent.get());
		dynamic_cast<GraphicsComponent*>(actorComponent.get())->setTilePosition(tilePosition);
		actorComponent->init(this->parentEngine);
		dynamic_cast<GraphicsComponent*>(actorComponent.get())->setPosition(position);
		//this->parentEngine->getGraphicsHandler()->addComponent(*actorComponent.get());
		actorComponentMap.insert(std::make_pair("GraphicsComponent", std::move(actorComponent)));
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("PhysicsComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding PhysicsComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("PhysicsComponent");
		std::string category = componentElement->FirstChildElement("category")->GetText();
		std::unique_ptr<ActorComponent> actorComponent = parentEngine->getActorFactory()->getPhysicsFactory()->create(category, componentElement);
		//std::unique_ptr<ActorComponent> actorComponent = std::make_unique<PhysicsComponent>(componentElement);
		addChild(actorComponent.get());
		
		actorComponent->init(this->parentEngine);
		dynamic_cast<PhysicsComponent*>(actorComponent.get())->setPosition(position);
		actorComponentMap.insert(std::make_pair("PhysicsComponent", std::move(actorComponent)));
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("AnimationComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding AnimationComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("AnimationComponent");
		std::unique_ptr<ActorComponent> actorComponent = std::make_unique<AnimationComponent>(componentElement);
		addChild(actorComponent.get());

		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("AnimationComponent", std::move(actorComponent)));
	}
	
	if (xmlElement->FirstChildElement("Components")->FirstChildElement("AIComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding AIComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("AIComponent");
		std::unique_ptr<ActorComponent> actorComponent = parentEngine->getActorFactory()->getAIFactory()->create(actorId.actorName, componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("AIComponent", std::move(actorComponent)));
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("PickUpComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding PickUpComponent\n");
		}

		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("PickUpComponent");
		std::unique_ptr<ActorComponent> actorComponent = parentEngine->getActorFactory()->getPickUpFactory()->create(actorId.actorName, componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("PickUpComponent", std::move(actorComponent)));
		parentEngine->getActorFactory()->addMessageListener(getActorId());
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("InputComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding InputComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("InputComponent");
		std::string category = componentElement->FirstChildElement("category")->GetText();
		std::unique_ptr<ActorComponent> actorComponent = parentEngine->getActorFactory()->getInputFactory()->create(category, componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("InputComponent", std::move(actorComponent)));
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("CombatComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding CombatComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("CombatComponent");
		std::unique_ptr<ActorComponent> actorComponent = std::make_unique<CombatComponent>(componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("CombatComponent", std::move(actorComponent)));
		
	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("AudioComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding AudioComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("AudioComponent");
		std::unique_ptr<ActorComponent> actorComponent = std::make_unique<AudioComponent>(componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("AudioComponent", std::move(actorComponent)));

	}
	if (xmlElement->FirstChildElement("Components")->FirstChildElement("InventoryComponent"))
	{
		if (xmlDebug)
		{
			printf("Adding InventoryComponent\n");
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("InventoryComponent");
		std::unique_ptr<InventoryComponent> actorComponent = std::make_unique<InventoryComponent>(componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("InventoryComponent", std::move(actorComponent)));

	}

	if (xmlElement->FirstChildElement("Components")->FirstChildElement("ParticleComponent"))
	{
		if (xmlDebug)
		{
			std::cout << "Adding ParticleComponent\n";
		}
		componentElement = xmlElement->FirstChildElement("Components")->FirstChildElement("ParticleComponent");
		std::unique_ptr<ParticleComponent> actorComponent = std::make_unique<ParticleComponent>(componentElement);
		addChild(actorComponent.get());
		actorComponent->init(this->parentEngine);
		actorComponentMap.insert(std::make_pair("ParticleComponent", std::move(actorComponent)));
	}
	
	addComponentSubscriber("InputComponent", "PhysicsComponent");
	addComponentSubscriber("InputComponent", "AIComponent");
	addComponentSubscriber("PhysicsComponent", "GraphicsComponent");
	addComponentSubscriber("PhysicsComponent", "CombatComponent");
	addComponentSubscriber("AIComponent", "PhysicsComponent");
	addComponentSubscriber("InputComponent", "AudioComponent");
	addComponentSubscriber("AnimationComponent", "GraphicsComponent");
	addComponentSubscriber("GraphicsComponent", "AnimationComponent");

	//TEMPORARY:
	//if (actorId.actorName == "Player")
	//{
	//	parentEngine->getServerHandler()->addSubscriber(getComponent("InputComponent"));
	//}
	//onSpawn();
}

void Actor::update(float deltaTime)
{
	for (auto& it : actorComponentMap)
	{
		it.second->update(deltaTime);
	}}

void Actor::onDeath()
{
	for (auto& it : actorComponentMap)
	{
		it.second->onDeath();
	}
}

void Actor::onHit()
{
	for (auto& it : actorComponentMap)
	{
		it.second->onHit();
	}
}

void Actor::onSpawn()
{
	for (auto& it : actorComponentMap)
	{
		it.second->onSpawn();
	}
}

void Actor::setParent(ActorFactory& parentPtr)
{
	this->parentActorFactory = &parentPtr;
}

ActorComponent* Actor::getComponent(std::string componentId)
{
	auto it = actorComponentMap.find(componentId);
	if (it != actorComponentMap.end())
	{
		return it->second.get();

	}
	else
	{
		return nullptr;
	}
}

actorID Actor::getActorId()
{
	return actorId;
}

void Actor::setActorId(actorID actorId)
{
	this->actorId = actorId;
}

std::string Actor::getCategory()
{
	return category;
}

void Actor::setCategory(std::string category)
{
	this->category = category;
}

void Actor::addComponentSubscriber(const char* messenger, const char* subscriber)
{
	ActorComponent* messengerActr = getComponent(messenger);
	ActorComponent* subscriberActr = getComponent(subscriber);
	if (messengerActr != nullptr && subscriberActr != nullptr)
	{
		messengerActr->addSubscriber(subscriberActr);
	}

}

void Actor::removeComponentSubscriber(const char * messenger, const char * subscriber)
{
	ActorComponent* messengerActr = getComponent(messenger);
	ActorComponent* subscriberActr = getComponent(subscriber);
	if (messengerActr != nullptr && subscriberActr != nullptr)
	{
		messengerActr->removeSubscriber(subscriberActr);
	}
}

void Actor::setActive(bool flag)
{
	PhysicsComponent* physicsComponentptr = dynamic_cast<PhysicsComponent*>(getComponent("PhysicsComponent"));
	if (physicsComponentptr != nullptr)
	{
		physicsComponentptr->getB2Body()->SetActive(flag);
	}
	active = flag;
}

bool Actor::isActive()
{
	return active;
}

void Actor::addChild(ActorComponent* actorComponent)
{
	actorComponent->setParent(*this);
}
