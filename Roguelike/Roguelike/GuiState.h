#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Bar.h"
#include "Button.h"
#include "Slider.h"
#include "Panel.h"
#include "TextBox.h"
#include "CheckBox.h"
#include "DropDown.h"
#include "Container.h"


class Engine;
class GuiHandler;

/** A graphical user interface state. This can be a main menu, a setting menu or the gui displayed in game */
class GuiState
{
public:
	GuiState(tinyxml2::XMLElement *xmlElement);
	~GuiState();

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/** Updates this object./ */
	void update();

	/**
	 * Displays a container found by name.
	 *
	 * @param	name	The name.
	 */
	void displayContainer(std::string name);

	/**
	 * Sets bar value by name.
	 *
	 * @param	name	The name.
	 * @param	size	The size.
	 */
	void setBarValue(std::string name, float size);

	/**
	 * Sets bar maximum value by name.
	 *
	 * @param	name	The name.
	 * @param	size	The size.
	 */
	void setBarMaxValue(std::string name, float size);

	/**
	 * Checks if any elements in the current game state is left clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Checks if any elements in the current game state is right clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Determines if any elements in the current game state has mouse released inside them
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Searches for bar by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found bar.
	 */
	Bar* findBar(std::string name);

	/**
	 * Searches for button by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found button.
	 */
	Button* findButton(std::string name);

	/**
	 * Searches for slider by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found slider.
	 */
	Slider* findSlider(std::string name);

	/**
	 * Searches for text box by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found text box.
	 */
	TextBox* findTextBox(std::string name);

	/**
	 * Searches for check box by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found check box.
	 */
	CheckBox* findCheckBox(std::string name);

	/**
	 * Searches for drop down by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found drop down.
	 */
	DropDown* findDropDown(std::string name);

	/**
	 * Searches for container by name.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the found container.
	 */
	Container* findContainer(std::string name);

	/** Clears this object to its blank/initial state. */
	void clear();

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiHandler& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiHandler* getParent();
private:
	void addChild(Bar* bar); 
	void addChild(Button* button);
	void addChild(Slider* slider);
	void addChild(Panel* panel);
	void addChild(TextBox* textBox);
	void addChild(CheckBox* checkBox);
	void addChild(DropDown* dropDown);
	void addChild(Container* container);

	GuiHandler* parentGuiHandler;
	Engine* parentEngine;

	tinyxml2::XMLElement *originNode;

	std::vector<std::unique_ptr<Bar>> bars;
	std::vector<std::unique_ptr<Button>> buttons;
	std::vector<std::unique_ptr<Slider>> sliders;
	std::vector<std::unique_ptr<Panel>> panels;
	std::vector<std::unique_ptr<TextBox>> textBoxes;
	std::vector<std::unique_ptr<CheckBox>> checkBoxes;
	std::vector<std::unique_ptr<DropDown>> dropDowns;
	std::vector<std::unique_ptr<Container>> containers;
};

