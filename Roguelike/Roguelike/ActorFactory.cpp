#include "ActorFactory.h"
#include "Actor.h"
#include "Engine.h"
#include "Message.h"

using namespace tinyxml2;

ActorFactory::ActorFactory():
	currentActorId(-1)
{
	pickUpFactory = std::make_unique<PickUpFactory>();
	aIFactory = std::make_unique<AIFactory>();
	physicsFactory = std::make_unique<PhysicsFactory>();
	inputFactory = std::make_unique<InputFactory>();
}

ActorFactory::~ActorFactory()
{

}

Actor* ActorFactory::createActor(std::string xmlPath, glm::vec3 position, glm::vec3 velocity)
{

	doc = parentEngine->getXmlHandler()->loadXml(xmlPath);
	Actor* returnActorPtr = nullptr;

	if (doc != nullptr)
	{
		XMLElement *titleElement = doc->FirstChildElement();
		std::string actorType = titleElement->FirstChildElement("Type")->GetText();
		std::string actorName = titleElement->FirstChildElement("ActorName")->GetText();
		std::string category;
		if (titleElement->FirstChildElement("Category"))
		{
			category = titleElement->FirstChildElement("Category")->GetText();
		}
		
		if (xmlDebug)
		{
			printf("\nCreating actor: %s, with actorType: %s\n", actorName.c_str(), actorType.c_str());
		}
		if (actorType == "Tile")
		{
			actorVector.push_back(std::make_unique<Actor>(titleElement));
			actorVector.back()->init(parentEngine, position);
			addChild(actorVector.back().get());
			currentActorId++;
			printf("%i\n", currentActorId);
			returnActorPtr = actorVector.back().get();
		}
		else if (actorType == "MovingObject")
		{
			actorID actor = { actorName, getNextActorId() };
			
			if (xmlDebug)
			{
				printf("ActorID:%i\n", actor.actorId);
			}
			
			actorMap[actor] = std::make_unique<Actor>(titleElement);
			actorMap[actor]->setActorId(actor);
			actorMap[actor]->setCategory(category);
			actorMap[actor]->init(parentEngine, position);
			addChild(actorMap[actor].get());
			returnActorPtr = actorMap[actor].get();
		}
		else
		{
			printf("Missing actorType\n");
		}
	}
	dynamic_cast<PhysicsComponent*>(returnActorPtr->getComponent("PhysicsComponent"))->setVelocity(velocity);
	return returnActorPtr;
}

void ActorFactory::createActor(std::string xmlPath, glm::vec3 position, glm::vec2 tilePosition)
{
	doc = parentEngine->getXmlHandler()->loadXml(xmlPath);
	Actor* returnActorPtr = nullptr;

	if (doc != nullptr)
	{
		XMLElement *titleElement = doc->FirstChildElement();
		std::string actorType = titleElement->FirstChildElement("Type")->GetText();
		std::string actorName = titleElement->FirstChildElement("ActorName")->GetText();
		if (xmlDebug)
		{
			printf("\nCreating actor: %s, with actorType: %s\n", actorName.c_str(), actorType.c_str());
		}
		if (actorType == "TileAtlas")
		{
			std::unique_ptr<Actor> newActor = std::make_unique<Actor>(titleElement);
			newActor->init(parentEngine, position, tilePosition);
			addChild(newActor.get());

			actorVector.push_back(std::move(newActor));
			currentActorId++;
			if (debug)
			{
				printf("Created actor with ID:%i\n", currentActorId);
			}
		}
		else
		{
			printf("Failed creating tile from atlas. Wrong actortype");
		}
	}
}

Actor * ActorFactory::insertActorFromNetwork(const std::string xmlPath, const glm::vec3 position, const actorID customActorID, const glm::vec3 velocity)
{
	doc = parentEngine->getXmlHandler()->loadXml(xmlPath);
	Actor* returnActorPtr = nullptr;

	if (doc != nullptr)
	{
		XMLElement *titleElement = doc->FirstChildElement();
		std::string actorType = titleElement->FirstChildElement("Type")->GetText();
		std::string actorName = titleElement->FirstChildElement("ActorName")->GetText();
		if (xmlDebug)
		{
			printf("\nCreating actor: %s, with actorType: %s\n", actorName.c_str(), actorType.c_str());
		}
		if (actorType == "Tile")
		{
			actorVector.push_back(std::make_unique<Actor>(titleElement));
			actorVector.back()->init(parentEngine, position);
			addChild(actorVector.back().get());
			currentActorId++;
			printf("%i\n", currentActorId);
			returnActorPtr = actorVector.back().get();
		}
		else if (actorType == "MovingObject")
		{
			actorID actor = customActorID;

			if (xmlDebug)
			{
				printf("ActorID:%i\n", actor.actorId);
			}

			actorMap[actor] = std::make_unique<Actor>(titleElement);
			actorMap[actor]->setActorId(actor);
			actorMap[actor]->init(parentEngine, position);
			if (actor.actorName == "Player")
			{
				parentEngine->setPlayer(*actorMap[actor].get());
			}
			addChild(actorMap[actor].get());
			returnActorPtr = actorMap[actor].get();
		}
		else
		{
			printf("Missing actorType\n");
		}
	}
	return returnActorPtr;
}


void ActorFactory::deleteActor(Actor* actorToDelete)
{
	actorID actorId = actorToDelete->getActorId();
	auto it = actorMap.find(actorId);
	if (it != actorMap.end())
	{
		if (debug)
		{
			printf("Deleted actor: %s, %i\n", actorId.actorName.c_str(), actorId.actorId);
		}

		it->second.reset();	//reset (kills) object
		
		actorMap.erase(it);
	}
}

void ActorFactory::updateAll(float deltaTime)
{
 	for (auto it: deleteQueue)
	{
		it->removeSubscriber(this);
		deleteActor(it);
	}

	for (auto it : activeSwapQueue)
	{
		it->setActive(!it->isActive());
	}
	activeSwapQueue.clear();

	deleteQueue.clear();
	for (auto& it : actorMap)
	{
		if (it.second->isActive())
		{
			it.second->update(deltaTime);
		}
	}
}

Actor* ActorFactory::getActor(actorID actorId)
{
	auto it = actorMap.find(actorId);
	if (it != actorMap.end())
	{
		return it->second.get();
	}
	else
	{
		std::cout << "Invalid actorID." << std::endl;
		return nullptr;
	}
}

PickUpFactory * ActorFactory::getPickUpFactory()
{
	return pickUpFactory.get();
}

AIFactory * ActorFactory::getAIFactory()
{
	return aIFactory.get();
}

PhysicsFactory * ActorFactory::getPhysicsFactory()
{
	return physicsFactory.get();
}

InputFactory * ActorFactory::getInputFactory()
{
	return inputFactory.get();
}

void ActorFactory::clearFactory()
{
	deleteQueue.clear();
	actorMap.erase(actorMap.begin(), actorMap.end());
	actorVector.erase(actorVector.begin(), actorVector.end());
}

unsigned int ActorFactory::getCurrentActorID(void)
{
	return currentActorId;
}


std::vector<Actor*> ActorFactory::getActors()
{
	std::vector<Actor*> temp;

	for (auto& it : actorMap)
	{
		temp.emplace_back(it.second.get());
	}
	return temp;
}

void ActorFactory::addMessageListener(actorID actorId)
{
	//getActor(actorId)->addSubscriber(this);
}

void ActorFactory::receiveMessage(const Message &message)
{
	
}

void ActorFactory::draw()
{
	Camera * mainCamera = parentEngine->getGraphicsHandler()->getCamera();
	glm::mat4 viewProjection = mainCamera->getProjectionMatrix() * mainCamera->getViewMatrix();
	for (auto& it : actorMap)
	{
		if (it.second->isActive())
		{
			if (dynamic_cast<GraphicsComponent*>(it.second->getComponent("GraphicsComponent")) != nullptr)
			{
				dynamic_cast<GraphicsComponent*>(it.second->getComponent("GraphicsComponent"))->draw(viewProjection);
			}

			if (dynamic_cast<ParticleComponent*>(it.second->getComponent("ParticleComponent")) != nullptr)
			{
				dynamic_cast<ParticleComponent*>(it.second->getComponent("ParticleComponent"))->draw(viewProjection);
			}
		}
	}

	for (auto& it : actorVector)
	{
		//if (it->isActive()) //will we ever deactivate static objects?
		{
			if (dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent")) != nullptr)
			{
				dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->draw(viewProjection);
			}

			if (dynamic_cast<ParticleComponent*>(it->getComponent("ParticleComponent")) != nullptr)
			{
				dynamic_cast<ParticleComponent*>(it->getComponent("ParticleComponent"))->draw(viewProjection);
			}
		}
		
	}
}

void ActorFactory::addToDeleteQueue(Actor* actorToDelete)
{
	for (auto it : deleteQueue)
	{
		if (it == actorToDelete)
		{
			return;
		}
	}
	this->deleteQueue.emplace_back(actorToDelete);
}

void ActorFactory::addToSwapActiveQueue(Actor* actorToSwapActive)
{
	for (auto it : activeSwapQueue)
	{
		if (it == actorToSwapActive)
		{
			return;
		}
	}
	this->activeSwapQueue.emplace_back(actorToSwapActive);
}

void ActorFactory::addChild(Actor* actor)
{
	actor->setParent(*this);
}

unsigned int ActorFactory::getNextActorId(void)
{
	return ++currentActorId;
}

void ActorFactory::setParent(Engine& parentPtr)
{
	this->parentEngine = &parentPtr;
}

Engine* ActorFactory::getParent()
{
	return parentEngine;
}
