#include "ParticleComponent.h"
#include "Engine.h"

ParticleComponent::~ParticleComponent()
{
}

void ParticleComponent::init(Engine * parentEngine)
{
	std::string texturePath;

	if (this->originNode->FirstChildElement("amountPerSecond"))
	{
		this->originNode->FirstChildElement("amountPerSecond")->QueryFloatText(&amountPerSecond);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing amountPerSecond\n";
	}

	if (this->originNode->FirstChildElement("maxLife"))
	{
		this->originNode->FirstChildElement("maxLife")->QueryIntText(&maxLife);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing maxLife\n";
	}

	if (this->originNode->FirstChildElement("velocityX"))
	{
		this->originNode->FirstChildElement("velocityX")->QueryFloatText(&velocity.x);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorR\n";
	}

	if (this->originNode->FirstChildElement("velocityY"))
	{
		this->originNode->FirstChildElement("velocityY")->QueryFloatText(&velocity.y);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorG\n";
	}

	if (this->originNode->FirstChildElement("velocityZ"))
	{
		this->originNode->FirstChildElement("velocityZ")->QueryFloatText(&velocity.z);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorB\n";
	}

	if (this->originNode->FirstChildElement("maxVelocity"))
	{
		this->originNode->FirstChildElement("maxVelocity")->QueryFloatText(&maxVelocity);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorB\n";
	}

	if (this->originNode->FirstChildElement("size"))
	{
		this->originNode->FirstChildElement("size")->QueryFloatText(&size);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing size\n";
	}

	if (this->originNode->FirstChildElement("colorR"))
	{
		this->originNode->FirstChildElement("colorR")->QueryFloatText(&color.r);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorR\n";
	}

	if (this->originNode->FirstChildElement("colorG"))
	{
		this->originNode->FirstChildElement("colorG")->QueryFloatText(&color.g);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorG\n";
	}

	if (this->originNode->FirstChildElement("colorB"))
	{
		this->originNode->FirstChildElement("colorB")->QueryFloatText(&color.b);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing colorB\n";
	}

	if (this->originNode->FirstChildElement("gravity"))
	{
		this->originNode->FirstChildElement("gravity")->QueryBoolText(&gravity);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing gravity\n";
	}

	if (this->originNode->FirstChildElement("gravityMultiplier"))
	{
		this->originNode->FirstChildElement("gravityMultiplier")->QueryFloatText(&gravityMultiplier);
	}
	else if (xmlDebug)
	{
		std::cout << "Missing gravityMultiplier\n";
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		texturePath = this->originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		std::cout << "Missing texturePath\n";
	}

	ParticleSystem::init(parentEngine, size, parentEngine->getTextureHandler()->loadTexture(texturePath), this->maxVelocity, this->gravity);
}

void ParticleComponent::update(float deltaTime)
{
	glm::vec3 position = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
	createPoint(this->amountPerSecond, deltaTime, position, this->maxLife, this->color, this->velocity);

	for (std::size_t i = 0; i < particles.size(); i++)
	{
		if (gravity == true)
		{
			float maximum = this->maxVelocity * gravityMultiplier;
			if (particles[i].velocity.y > -maximum)
			{
				particles[i].velocity.y -= maximum / 4.f;
			}
			else
			{
				particles[i].velocity.y = -maximum;
			}
		}
		positions[i] += (particles[i].velocity * deltaTime);

		particles[i].life -= (1.f * deltaTime);
		if (particles[i].life <= 0.f)
		{
			particles.erase(particles.begin() + i);
			colors.erase(colors.begin() + i);
			positions.erase(positions.begin() + i);
		}
	}
}

void ParticleComponent::draw(glm::mat4 viewProjection)
{
	ParticleSystem::draw(viewProjection);
}
