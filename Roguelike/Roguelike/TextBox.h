#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"

class Engine;
class GuiState;

/** A text box. Used to create a text field on the screen that the user can input text into */
class TextBox : public IObservable
{
public:
	TextBox(tinyxml2::XMLElement *xmlElement);
	~TextBox();
	/** Updates this object. */
	void update();

	/**
	 * Executes the focus action.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool onFocus();

	/**
	 * runs if focus is lost.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool lostFocus();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	std::string name;
	std::string input;
	std::string defaultValue;

	bool focus;

	int currentTexture;
	int maxLength;

	tinyxml2::XMLElement *originNode;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

