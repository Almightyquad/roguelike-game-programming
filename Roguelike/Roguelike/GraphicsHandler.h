#pragma once
#include "IGraphicsHandler.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <glm\glm.hpp>
#include "Camera.h"
#include "GraphicsComponent.h"
#include <memory>
#include <vector>
/**
 * @class	GraphicsHandler
 *
 * @brief	The graphics handler is primarily used to clear/draw everything.
 * @details	Holds the cameraPosition and SDL_Window
 * 			Also initializes everything related to graphics
 */
class GraphicsHandler : public IGraphicsHandler
{
public:

	/**
	 * @fn	GraphicsHandler::GraphicsHandler(const std::string& windowname);
	 *
	 * @brief	Constructor.
	 *
	 * @param	windowname	Title of window name.
	 */
	GraphicsHandler(const std::string& windowname);

	/**
	 * @fn	GraphicsHandler::~GraphicsHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~GraphicsHandler();

	/**
	 * @fn	void GraphicsHandler::init(const std::string& windowname);
	 *
	 * @brief	Initialises this object.
	 *
	 * @param	windowname	Title of window name.
	 */
	void init(const std::string& windowname);

	/**
	 * @fn	void GraphicsHandler::swapBuffers() override;
	 *
	 * @brief	Swap buffers.
	 */
	void swapBuffers() override;

	/**
	 * @fn	void GraphicsHandler::clearWindow() override;
	 *
	 * @brief	Clears the window.
	 *
	 */
	void clearWindow() override;

	/**
	 * @fn	Camera * GraphicsHandler::getCamera();
	 *
	 * @brief	Gets the camera.
	 *
	 * @return	pointer to camera.
	 */
	Camera * getCamera();

	/**
	 * @fn	glm::vec2 GraphicsHandler::getDesktopResolution();
	 *
	 * @brief	Gets desktop resolution.
	 *
	 * @return	The desktop resolution.
	 */
	glm::vec2 getDesktopResolution();

	/**
	 * @fn	glm::vec2 GraphicsHandler::getWindowResolution();
	 *
	 * @brief	Gets window resolution.
	 *
	 * @return	The window resolution.
	 */
	glm::vec2 getWindowResolution();

	/**
	 * @fn	void GraphicsHandler::setCameraDirection(glm::vec2 position, float dt);
	 *
	 * @brief	Sets camera direction.
	 *
	 * @param	position	The position.
	 * @param	dt			The deltatime.
	 */
	void setCameraDirection(glm::vec2 position, float dt);

	/**
	 * @fn	void GraphicsHandler::setCameraPosition(bool horizontalDirection, float forwardMotion);
	 *
	 * @brief	Sets camera position.
	 *
	 * @param	horizontalDirection	true to horizontal direction.
	 * @param	forwardMotion	   	The forward motion.
	 */
	void setCameraPosition(bool horizontalDirection, float forwardMotion);

	/**
	 * @fn	void GraphicsHandler::setCameraPosition(glm::vec3 cameraPosition);
	 *
	 * @brief	Sets camera position.
	 *
	 * @param	cameraPosition	The camera position.
	 */
	void setCameraPosition(glm::vec3 cameraPosition);

	/**
	 * @fn	void GraphicsHandler::updateWindow(Uint32 flag);
	 *
	 * @brief	Updates the window described by flag.
	 *
	 * @param	flag	SDL flags.
	 */
	void updateWindow(Uint32 flag);

private:
	SDL_Window* window {};

	SDL_GLContext glContext;
	std::unique_ptr<Camera> mainCamera;
	glm::vec2 resolution;
};
