#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "common.h"
#include "Actor.h"
#include "SavedFrames.h"
#include "Timer.h"
#include <unordered_map>

class Engine;
/**
 * @class	TimeRewinder
 *
 * @brief	A time rewinder.
 * @details	Used to rewind the state of every object 
 */
class TimeRewinder
{
public:
	TimeRewinder();
	~TimeRewinder();
	void init();
	void update();
	void rewind();
	void setStatus(bool status);
	void setParent(Engine &parentPtr);
	Engine* getParent();
	float getTimer();
private:
	int indexValue;
	int vectorLimit = 1000000;
	const int timeLimitInSeconds = 5;
	int numberOfActors;
	bool isRewinding;
	Timer timer;
	std::vector<SavedFrames> savedFrames;
	std::vector<Actor*> actorVector;
	Engine* parentEngine;
};

