#pragma once
#include "PhysicsComponent.h"

class PhysicsProjectileEnemy final : public PhysicsComponent
{
public:
	PhysicsProjectileEnemy(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};