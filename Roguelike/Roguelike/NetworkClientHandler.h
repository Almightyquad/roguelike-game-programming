#pragma once
#include "NetworkComponent.h"
#include <SDL_net.h>
#include "INetworkHandler.h"

class NetworkClientHandler : public INetworkHandler
{
public:
	NetworkClientHandler();
	~NetworkClientHandler();
	void init();
	void sendPacket(int gameState) override;
	void receivePacket(int gameState) override;
	void receiveActorStructure();
	void receiveItemStructure();
	actorID getPlayerActorID();
	//void sendInit();
	void sendJoining();
	void sendRunning();
	//void receiveInit();
	void receiveJoining();
	void receiveRunning();
	void unpackActorId(actorID* actorIdToUnpackTo, NetworkActorData actorData);

private:
	static const int MAXINPUT = 5;
	int input[MAXINPUT];
	char* buffer;
	actorID playerActorID;
	int packetLength;
	TCPsocket socket;
	NetworkActorData actorFromServer[200];
	int numberOfHostActors;
	int numberOfitems;
	SDLNet_SocketSet socketSet;
};