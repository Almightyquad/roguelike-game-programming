#include "EventHandler.h"
#include "Engine.h"
#include "common.h"
#include <memory>
#include "Actor.h"
#include "Message.h"

#ifdef __ANDROID__
#define SDL_MOUSEMOTION SDL_FINGERMOTION
#define SDL_MOUSEBUTTONDOWN SDL_FINGERDOWN
#define SDL_MOUSEBUTTONUP SDL_FINGERUP
#endif

EventHandler::EventHandler()
{
	consoleActive = false;
	inputActive = false;
	inputText = "";
	consoleInputText = "";
	mouseX = mouseY = 1;
	prevMouseX = prevMouseY = -1;
}

EventHandler::~EventHandler()
{

}

void EventHandler::handleEvents()
{
	SDL_Event SDLEvent;
	int inputhandling = -1;
	//switch (SDLevent);
	
	while (SDL_PollEvent(&SDLEvent))
	{
		switch (SDLEvent.type)
		{
		case SDL_QUIT:
			parentEngine->setGameState(STATE_QUITTING);
			break;
			//Runs any time a key is pressed
		case SDL_KEYDOWN:
			//Console doesn't work, so this pretty much does nothing
			if (SDLEvent.key.keysym.sym == SDLK_BACKQUOTE)
			{
				swapConsoleStatus();
				consoleInputText = "";
			}
			if (!consoleActive && !inputActive)
			{
				if (SDLEvent.type == SDL_KEYDOWN)
				{
					//Sends messages to InputPlayer. This needs to have a string as the second arg. 
					//TODO: Fix this to have (this, "somestringidentifier, (int)etc, true)
					this->postMessage(Message(this, static_cast<int>(SDLEvent.key.keysym.sym), true));	
				}
			}
			if (consoleActive)
			{
				handleConsoleTextActions(SDLEvent);
			}
			if (inputActive)
			{
				handleTextActions(SDLEvent);
			}
			//if (SDLEvent.key.keysym.sym == SDLK_UP)
			//{
			//	this->parentEngine->getGraphicsHandler()->getCamera()->updatePosition(true, 1.f);
			//}
			break;
			//Runs anytime a key is released
		case SDL_KEYUP:
			//TODO: Fix this to have (this, "somestringidentifier, (int)etc, false)
			this->postMessage(Message(this, static_cast<int>(SDLEvent.key.keysym.sym), false));
			if (SDLEvent.key.keysym.sym == SDLK_q)
			{
					
				parentEngine->getTimeKeeper()->setStatus(false);
			}
			break;

		case SDL_TEXTINPUT:

			if (inputActive)
			{
				handleInputText(SDLEvent);
			}
			else if (consoleActive)
			{
				handleConsoleInputText(SDLEvent);
			}
			break;

		case SDL_MOUSEMOTION:
			handleMouseMotion(SDLEvent);
			break;
		case SDL_MOUSEBUTTONDOWN:
			handleMouseDown(SDLEvent);
			break;
		case SDL_MOUSEBUTTONUP:
			handleMouseUp(SDLEvent);
			break;

		case SDL_WINDOWEVENT:
			handleWindowEvents(SDLEvent);
			break;

		default:
			if (SDLEvent.type == 4352)
			{
				std::cout << "We don't handle event: "
						  << "SDL_AudioDeviceAdded (4352)\n";
			}
			else if (SDLEvent.type == 770)
			{
				std::cout << "We don't handle event: "
						  << "SDL_TextEditing (770)\n";
			}
			else
			{
				std::cout << "We don't handle this type of events: " << SDLEvent.type << std::endl;
			}
			
			break;
		}	
	}
}

void EventHandler::init()
{
#ifdef __ANDROID__
	touchLocation.x = 1;
	touchLocation.y = 1;
#endif
	screenResolutionX = parentEngine->getGraphicsHandler()->getWindowResolution().x;
	screenResolutionY = parentEngine->getGraphicsHandler()->getWindowResolution().y;
}

int EventHandler::handleKeyboardEvents(SDL_Event & SDLEvent)
{
	int returnValue = -1;
	//string takeInput;
	return SDLEvent.key.keysym.sym;
}

void EventHandler::handleWindowEvents(SDL_Event & SDLEvent)
{
	switch (SDLEvent.window.event)
	{
	case SDL_WINDOWEVENT_RESIZED:
		glViewport(0, 0, SDLEvent.window.data1, SDLEvent.window.data2);
		screenResolutionX = parentEngine->getGraphicsHandler()->getWindowResolution().x;
		screenResolutionY = parentEngine->getGraphicsHandler()->getWindowResolution().y;
		break;
	default:
		break;
	}
}

void EventHandler::getMousePosition(SDL_Event & SDLEvent)
{
	//This thing is a clusterfuck
	//TODO: Fix this clusterfuck
	//transform to OPENGL space
#ifdef __ANDROID__
	if (((SDLEvent.tfinger.x * screenResolutionX) != mouseX) || ((SDLEvent.tfinger.y * screenResolutionY) != mouseY))
	{
#else
	SDL_GetMouseState(&mouseX, &mouseY);
	if(prevMouseX != mouseX && mouseY != prevMouseY)
	{
#endif
#ifdef __ANDROID__
	touchLocation.x = (SDLEvent.tfinger.x * screenResolutionX);
	touchLocation.y = (SDLEvent.tfinger.y * screenResolutionY);
	mousePos.x = static_cast<float>(touchLocation.x) / screenResolutionX * 2 - 1;
	mousePos.y = static_cast<float>(touchLocation.y) / screenResolutionY * 2 - 1;
	//Because androids y is different than opengl, so we just flip the Y
	mousePos.y = -mousePos.y;
	prevMouseX = touchLocation.x;
	prevMouseY = touchLocation.y;
#else
	SDL_GetMouseState(&mouseX, &mouseY);
	mousePos.x = static_cast<float>(mouseX) / screenResolutionX * 2 - 1;
	mousePos.y = -(static_cast<float>(mouseY) / screenResolutionY * 2 - 1);
	prevMouseX = mouseX;
	prevMouseY = mouseY;
#endif
	}
}

void EventHandler::handleMouseMotion(SDL_Event & SDLEvent)
{
	getMousePosition(SDLEvent);
	//Sends a message to InputPlayer, add the string please
	this->postMessage(Message(this, static_cast<int>(SDLEvent.type), mousePos));
	//parentEngine->getGraphicsHandler()->setCameraDirection(glm::vec2(SDLEvent.motion.xrel, SDLEvent.motion.yrel), parentEngine->getDeltaTime());
}

void EventHandler::handleMouseDown(SDL_Event & SDLEvent)
{
	getMousePosition(SDLEvent);
#ifdef __ANDROID__
#else
	//Checks if the left mouse button is clicked.
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
#endif
	{
		bool guiClick; //checks if the mouse click is clicking on a gui element
		guiClick = parentEngine->getGuiHandler()->checkMouseClickLeft();

		if (!guiClick) //dont want player to shoot if he is pressing gui elements
		{
			//Sends a message to InputPlayer, add the string please
			this->postMessage(Message(this, static_cast<int>(SDLEvent.type), true));
		}
		guiClick = false;
	}
#ifdef __ANDROID
#else
	//Check if the right mouse button is clicked
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
	{
		bool guiClick; //checks if the mouse click is clicking on a gui element
		guiClick = parentEngine->getGuiHandler()->checkMouseClickRight();

		if (!guiClick) //dont want player to shoot if he is pressing gui elements
		{

		}
		guiClick = false;
	}
#endif

	//TODO: ADD LEFT AND RIGHT BUTTON SUPPORT

}

void EventHandler::handleMouseUp(SDL_Event & SDLEvent)
{
	getMousePosition(SDLEvent);
	//Sends message to InputPlayer
	this->postMessage(Message(this, static_cast<int>(SDLEvent.type), false, mousePos)); //sets mouseinput to false for playeractor
																						//TODO: ADD LEFT AND RIGHT BUTTON SUPPORT
	mouseButtonLeftRelease();
}



bool EventHandler::mouseInside(Transform trans)		//used very deeply inside UI some place
{
	Transform temp = trans;
	temp.getPos() -= temp.getScale() / 2.f;
	bool inside = true;
	//Mouse is left of the button
	if (mousePos.x < temp.getPos().x)
	{
		inside = false;
	}
	//Mouse is right of the button
	else if (mousePos.x > temp.getPos().x + temp.getScale().x)
	{
		inside = false;
	}
	//Mouse above the button
	else if (mousePos.y < temp.getPos().y)
	{
		inside = false;
	}
	//Mouse below the button
	else if (mousePos.y > temp.getPos().y + temp.getScale().y)
	{
		inside = false;
			
	}
	return inside;
}

void EventHandler::handleConsoleTextActions(SDL_Event & SDLevent)
{
	//This is for all the useful commands when you do text editing, like backspace, ctrl+c, ctrl+v and enter.
	if (SDLevent.type == SDL_KEYDOWN)
	{
		//Handle backspace
		if (SDLevent.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0)
		{
			//lop off character
			consoleInputText.pop_back();
		}
		//Handle copy
		else if (SDLevent.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
		{
			SDL_SetClipboardText(consoleInputText.c_str());
		}
		//Handle paste
		else if (SDLevent.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
		{
			consoleInputText = SDL_GetClipboardText();
		}
		else if (SDLevent.key.keysym.sym == SDLK_RETURN)
		{
			inputText = "";
			consoleInputText;
		}

	}
}

//Slight duplicate of the one over.
//TODO, fix a general function for these keyboard commands and functions
void EventHandler::handleTextActions(SDL_Event & SDLevent)
{
	//This is for all the useful commands when you do text editing, like backspace, ctrl+c, ctrl+v and enter.
	if (SDLevent.type == SDL_KEYDOWN)
	{
		//Handle backspace
		if (SDLevent.key.keysym.sym == SDLK_BACKSPACE && inputText.length() > 0)
		{
			//lop off character
			inputText.pop_back();
		}
		//Handle copy
		else if (SDLevent.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL)
		{
			SDL_SetClipboardText(inputText.c_str());

		}
		//Handle paste
		else if (SDLevent.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL)
		{
			inputText = SDL_GetClipboardText();

		}
		else if (SDLevent.key.keysym.sym == SDLK_RETURN)
		{
			//inputText = "";
		}
	}
}

//TODO fix a general function for the function below and the one below that
void EventHandler::handleConsoleInputText(SDL_Event &SDLevent)
{
	if (SDLevent.type == SDL_TEXTINPUT)
	{
		//Not copy or pasting
		
		if (!((SDLevent.text.text[0] == 'c' || SDLevent.text.text[0] == 'C') && (SDLevent.text.text[0] == 'v' || SDLevent.text.text[0] == 'V') && SDL_GetModState() & KMOD_CTRL))
		{
			//Append character
			if (!(SDLevent.text.text[0] == '|'))
			{
				consoleInputText += SDLevent.text.text;
			}
		}
	
	}
}

void EventHandler::handleInputText(SDL_Event &SDLevent)
{
	if (SDLevent.type == SDL_TEXTINPUT)
	{
		//Not copy or pasting

		if (!((SDLevent.text.text[0] == 'c' || SDLevent.text.text[0] == 'C') && (SDLevent.text.text[0] == 'v' || SDLevent.text.text[0] == 'V') && SDL_GetModState() & KMOD_CTRL))
		{
			//Append character
			if (!(SDLevent.text.text[0] == '|'))
			{
				inputText += SDLevent.text.text;			
			}
		}
	}
}

void EventHandler::swapConsoleStatus()
{
	consoleActive = !consoleActive;
}

void EventHandler::setInputStatus(bool status)
{
	inputActive = status;
	inputText = "";
}

glm::ivec2 EventHandler::getMousePos()
{
	return glm::ivec2(mouseX, mouseY);
}

glm::vec3 EventHandler::getMouseOpenGLPos()
{
	return mousePos;
}

void EventHandler::mouseButtonLeftClicked()
{
	
}

void EventHandler::mouseButtonLeftRelease()
{
	parentEngine->getGuiHandler()->checkMouseRelease();
}

std::string EventHandler::getInputText()
{		 
	return inputText;
}
