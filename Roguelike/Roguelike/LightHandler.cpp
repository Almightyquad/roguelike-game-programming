#include "LightHandler.h"
#include "Engine.h"


LightHandler::LightHandler()
{
}

LightHandler::~LightHandler()
{
	glDeleteTextures(1, &texture);
	glDeleteFramebuffers(1, &fbo);
}

void LightHandler::init()
{
	resolution = { 640, 360 };
	shader = parentEngine->getShaderHandler()->loadShader("res/shaders/basicShader");
	mesh = parentEngine->getMeshHandler()->loadModel("res/models/quad.obj");
	lightTexture = parentEngine->getTextureHandler()->loadTexture("res/texture/light.png");
	createFBO();
	createTextureAttachment(static_cast<int>(resolution.x), static_cast<int>(resolution.y));	//generate low-reso texture for light
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void LightHandler::update(glm::mat4 view)
{
	bindFramebuffer();
	Transform transform;
	for (auto it : lights)
	{
		transform.getScale() *= it.w;
		transform.setPos(glm::vec3(it.x, it.y, it.z));
		shader->bind();
		shader->loadTransform(transform, view);
		mesh->draw();
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glm::vec2 reso = parentEngine->getGraphicsHandler()->getWindowResolution();
	glViewport(0, 0, static_cast<GLsizei>(reso.x), static_cast<GLsizei>(reso.y));
}

void LightHandler::addLight(glm::vec3 position, float size)
{
	glm::vec4 light{ position, size };
	lights.emplace_back(light);
}

GLuint LightHandler::getTexture()
{
	return texture;
}

void LightHandler::createFBO()
{
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
}

void LightHandler::createTextureAttachment(int width, int height)
{
	unsigned char* imageData = {  };
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);
}

void LightHandler::bindFramebuffer()
{
	glBindTexture(GL_TEXTURE_2D, 0); //unbind previous texture
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glViewport(0, 0, static_cast<int>(resolution.x), static_cast<int>(resolution.y));	//need this?
}
