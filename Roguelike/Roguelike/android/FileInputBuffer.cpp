/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "FileInputBuffer.h"

#include <algorithm>
#include <cassert>
#include <cstring>

namespace krem { namespace android {

FileInputBuffer::FileInputBuffer(JNIEnv* javaEnv, jobject fileInputStreamJava, const std::size_t bufferSize, const std::size_t putBackSize):
	putBackSize(std::max(putBackSize, 1u)),
	buffer(std::max(bufferSize, putBackSize)),
	javaEnv(javaEnv),
	fileInputStreamJava(fileInputStreamJava)
{
	assert(javaEnv != nullptr);

	auto end = this->buffer.data() + this->buffer.size();

	/*
	 * Set all pointers to end to indicate that the buffer
	 * is empty.
	 */
	this->setg(end, end, end);

	const auto bufferSizeJava = static_cast<jsize>(bufferSize);
	this->bufferJava = javaEnv->NewByteArray(bufferSizeJava);

	this->fileInputStreamClass = this->javaEnv->FindClass("java/io/FileInputStream");
	this->readMethodId = this->javaEnv->GetMethodID(this->fileInputStreamClass, "read", "([BII)I");
}

FileInputBuffer::FileInputBuffer(JNIEnv* javaEnv, jobject fileInputStreamJava):
	FileInputBuffer(javaEnv, fileInputStreamJava, 256, 8)
{
}

FileInputBuffer::~FileInputBuffer()
{
	this->javaEnv->DeleteLocalRef(this->bufferJava);
}

FileInputBuffer::int_type FileInputBuffer::underflow()
{
	/*
	 * Buffer is not exhausted, so we return the next character
	 * in the buffer.
	 */
	if (this->gptr() < this->egptr())
	{
		return traits_type::to_int_type(*this->gptr());
	}

	auto base = this->buffer.data();
	auto startOffset = std::size_t{0};

	if (this->eback() == base)
	{
		std::memmove(base, this->egptr() - this->putBackSize, this->putBackSize);
		startOffset += this->putBackSize;
	}

	const auto numBytesToRead = this->buffer.size() - startOffset;

	const auto numBytesToReadJava = static_cast<jint>(numBytesToRead);
	const auto bytesReadJava = this->javaEnv->CallIntMethod(this->fileInputStreamJava, this->readMethodId, this->bufferJava, 0, numBytesToReadJava);

	if (bytesReadJava <= 0)
	{
		return traits_type::eof();
	}

	auto bufferInput = reinterpret_cast<jbyte*>(this->buffer.data() + startOffset);
	this->javaEnv->GetByteArrayRegion(this->bufferJava, 0, bytesReadJava, bufferInput);

	const auto bytesRead = static_cast<std::size_t>(bytesReadJava);

	auto start = base + startOffset;
	this->setg(base, start, start + bytesRead);

	return traits_type::to_int_type(*this->gptr());
}

} }
