#include "Engine.h"
#include "Message.h"
#include "Actor.h"

//doing another test with branching

void Engine::initMainmenu()
{
	hosting = false;
	joining = false;
	audioHandler->loadMusic("res/sounds/mainMenu.wav");
	audioHandler->playMusic("res/sounds/mainMenu.wav");
}

void Engine::updateMainmenu()
{
	while (gameState == STATE_MAINMENU)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initSettings()
{

}

void Engine::updateSettings()
{
	while (gameState == STATE_SETTINGS)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initCharacterSelect()
{

}

void Engine::updateCharacterSelect()
{
	while (gameState == STATE_CHARACTERSELECT)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initHosting()
{
#ifdef __ANDROID__
#else
	srand(randomSeed);
	networkServerHandler = std::make_unique<NetworkServerHandler>();
	hosting = true;
	addChild(networkServerHandler.get());
	networkServerHandler->init();
#endif
}

void Engine::updateHosting()
{
#ifdef __ANDROID__
#else
	std::thread listener(&Engine::connectionlistener, this);
	while (gameState == STATE_HOSTING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		if (netUpdateTimer.hasEnded())
		{
			/*networkServerHandler->receivePacket(gameState);
			networkServerHandler->sendPacket(gameState);*/
			netUpdateTimer.restart();
		}
		genericDraw();
	}
	networkServerHandler->initClients();
//	listener.join();
	//this allows users to continue joining even after the game has "started". not sure how to do this in a good way 
	listener.detach();
	return;
#endif
}

void Engine::initJoining()
{
#ifdef __ANDROID__
#else
	networkClientHandler = std::make_unique<NetworkClientHandler>();
	joining = true;
	addChild(networkClientHandler.get());
	std::cout << "\n*DOES NOTHING ATM*Enter remote IP ( 127.0.0.1  for local connections ) : ";
	//std::cin >> IP;
	std::cout << "\n*DOES NOTHING ATM*Enter remote port : ";
	//std::cin >> remotePort;
	networkClientHandler->init();
#endif
}

void Engine::updateJoining()
{
#ifdef __ANDROID__
#else
	while (gameState == STATE_JOINING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		if (netUpdateTimer.hasEnded())
		{
			networkClientHandler->sendPacket(gameState);
			networkClientHandler->receivePacket(gameState);
			netUpdateTimer.restart();
		}
		genericDraw();
	}
#endif
}



void Engine::initRunning()
{
	eventHandler->setInputStatus(false);
	printf("seed: is %i\n", randomSeed);
	srand(randomSeed);

	audioHandler->loadMusic("res/sounds/gameMusic.wav");
	//audioHandler->playMusic("res/sounds/gameMusic.wav");
	levelLoader->setActorFactory(actorFactory.get());
	levelLoader->setPhysicsHandler(physicsHandler.get());
	levelLoader->loadLevel();
	IInputComponent* playerInput;
	InventoryComponent* playerInventory;
	if (!joining) //== if singleplayer or hosting
	{
		setPlayer(*actorFactory->createActor(playerString, glm::vec3(10.f, 0.f, 0.f))); //game needs atleast one player
		actorFactory->createActor("res/actors/Pickup/PickupHealthPotion.xml", glm::vec3(11.f, 2.f, 0.f));
		actorFactory->createActor("res/actors/Pickup/PickupManaPotion.xml", glm::vec3(8.f, 2.f, 0.f));
		//actorFactory->createActor("res/actors/Enemy/bossChuck.xml", glm::vec3(15.f, 0.f, 0.f), { 0.f, 0.f, 0.f }); //removed boss for now
#ifndef __ANDROID__
		if (hosting)
		{
			//dnetworkServerHandler->sendPacket(gameState);
		}
#endif
	}
#ifdef __ANDROID__
#else
	if (joining)
	{
		actorID playerID = networkClientHandler->getPlayerActorID();
		networkClientHandler->receivePacket(STATE_JOINING); //TODO: fix this when rewriting network
		setPlayer(*actorFactory->getActor(playerID));
	}
#endif
	playerInput = dynamic_cast<IInputComponent*>(player->getComponent("InputComponent"));
	playerInventory = dynamic_cast<InventoryComponent*>(player->getComponent("InventoryComponent"));

 	guiHandler->setBarMaxValue("healthBar", static_cast<float>(dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getHealth()));
	guiHandler->setBarMaxValue("manaBar", static_cast<float>(dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getMana()));
	//SUBSCRIBE PLAYER TO EVENTHANDLER FOR DIRECT INPUTHANDLING
	eventHandler->addSubscriber(playerInput);
	guiHandler->getContainer("inventory")->addSubscriber(playerInventory);
	if (physicsDebug)
	{
		physicsHandler->dump();
	}


	for (std::size_t i = 0; i < possibleItems.size(); i++)
	{
		randomSpawn->spawnRandomItem(possibleItems[i]);
	}
	enemyTimer.start();
	itemTimer.start();
}

void Engine::updateRunning()
{
	while (gameState == STATE_RUNNING)
	{
		if (enemiesKilled >= killsNeededToWin)
		{
			std::cout << "You have killed " << enemiesKilled << " enemies." << std::endl;
			std::cout << "You had to kill " << killsNeededToWin << " enemies." << std::endl;
		}
		genericUpdate();
#ifdef __ANDROID__
#else
		if (netUpdateTimer.hasEnded())
		{
			if (hosting)
			{
				networkServerHandler->sendPacket(gameState);
			}
			if (joining)
			{
				networkClientHandler->sendPacket(gameState);
			}
			netUpdateTimer.restart();
		}
		if (hosting)
		{
			networkServerHandler->receivePacket(gameState);
		}
		if (joining)
		{
			networkClientHandler->receivePacket(gameState);
		}		
#endif
		glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
		this->physicsHandler->update(this->deltaTime);
		this->actorFactory->updateAll(this->deltaTime);		
		this->skyboxHandler->update(viewProjection, this->deltaTime);
		weatherParticles->update(this->deltaTime);
		particleSystem->update(this->deltaTime);

		//this->timeRewinder->update(); //doesn't work
		glm::vec3 playerPos = dynamic_cast<PhysicsComponent*>(player->getComponent("PhysicsComponent"))->getPosition();
		glm::vec3 camera = playerPos;
		//TODO do this smarter/more flexible
		if (camera.x < 11.6f) camera.x = 11.6f;
		if (camera.x > 81.4f) camera.x = 81.4f;
		if (camera.y < -8.15f) camera.y = -8.15f;

		graphicsHandler->setCameraPosition(glm::vec3{ camera.x, camera.y, 15.f });
		this->skyboxHandler->draw(viewProjection);	//updates and draws
		this->actorFactory->draw();

#ifdef __ANDROID__
#else
		//this->lightHandler->update(viewProjection);
#endif
		this->weatherParticles->draw(viewProjection);
		this->particleSystem->draw(viewProjection);
		if (!joining)
		{
			if (enemyTimer.hasEnded())
			{
				randomSpawn->spawnRandomEnemy(possibleEnemies[rand() % possibleEnemies.size()]);
				enemyTimer.restart();
			}
			if (itemTimer.hasEnded())
			{
				randomSpawn->spawnRandomItem(possibleItems[rand() % possibleItems.size()]);
				itemTimer.restart();
			}
		}
		
		////// test light
		/*Transform transform;
		transform.setPos(glm::vec3(5, -5, 0));
		transform.setScale(glm::vec3(5, 5, 1));
		Mesh *mesh = getMeshHandler()->loadModel("res/models/quad.obj");
		Shader *shader = getShaderHandler()->loadShader("res/shaders/basicShader");
		shader->loadTransform(transform, viewProjection);
		mesh->draw();*/
		genericDraw();
		if (dynamic_cast<CombatComponent*>(player->getComponent("CombatComponent"))->getHealth() <= 0)
		{
			setGameState(STATE_GAMEOVER);
		}
	}
	eventHandler->removeSubscriber(dynamic_cast<IInputComponent*>(player->getComponent("InputComponent")));
	guiHandler->clear(STATE_RUNNING);
//	eventHandler->removeSubscriber(dynamic_cast<InputComponent*>(player->getComponent("InputComponent")));
	actorFactory->clearFactory();
	physicsHandler->clear();
	
	return;
}

void Engine::initGameover()
{

}

void Engine::updateGameover()
{
	while (gameState == STATE_GAMEOVER)
	{
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initQuitting()
{
	physicsHandler->clear();
}

void Engine::updateQuitting()
{
//stuffsibuffsi
}



Engine::Engine()
	:
	deltaTime(0),
	previousTime(0),
	netUpdateTimer(1.f / 30.f), //sets network to update every 1.f/x.f second (or x times a second). NOTE: this feels unprecise. should get rid of sdl timers anyways 1
	oneSecondTimer(1),
	enemyTimer(5),
	itemTimer(10),
	joining(false),
	hosting(false),
	enemiesKilled(0),
	killsNeededToWin(20),
	playerString("res/actors/player0.xml")
{
	randomSeed = static_cast<unsigned int>(time(NULL));
	srand(randomSeed);
	running = true;
	
	actorFactory = std::make_unique<ActorFactory>();
	graphicsHandler = std::make_unique<GraphicsHandler>("Roguelike");
	physicsHandler = std::make_unique<PhysicsHandler>();
	audioHandler = std::make_unique<AudioHandler>();
	textureHandler = std::make_unique<TextureHandler>();
	meshHandler = std::make_unique<MeshHandler>();
	guiHandler = std::make_unique<GuiHandler>();
	shaderHandler = std::make_unique<ShaderHandler>();
	eventHandler = std::make_unique<EventHandler>();
	//timeRewinder = std::make_unique<TimeRewinder>();
	fontHandler = std::make_unique<FontHandler>();
	terrainHandler = std::make_unique<TerrainHandler>();
	skyboxHandler = std::make_unique<SkyboxHandler>();
	xmlHandler = std::make_unique<XmlHandler>();

	levelLoader = std::make_unique<LevelLoader>();
#ifdef __ANDROID__
	sdlAndroidActivity = std::make_unique<krem::app::SdlAndroidActivity>();
	sdlAndroidActivity->initialize();
	javaEnv = sdlAndroidActivity->getJavaEnvironment();
	javaAssetManager = sdlAndroidActivity->getJavaAssetManager();
	assetMgr = std::make_unique<krem::android::AssetManager>(javaEnv, javaAssetManager);
#else
	//lightHandler = std::make_unique<LightHandler>();
#endif
	weatherParticles = std::make_unique<WeatherParticles>();
	particleSystem = std::make_unique<ParticleSystem>();
	randomSpawn = std::make_unique<RandomSpawn>();
	possibleItems.push_back("BowAndArrow");
	possibleItems.push_back("Sword");
	possibleItems.push_back("Fireball");
	possibleItems.push_back("HealthPotion");
	possibleItems.push_back("ManaPotion");
	possibleEnemies.push_back("esmeralda");
	possibleEnemies.push_back("kappa");
	possibleEnemies.push_back("chuck");
	gameState = STATE_MAINMENU;
}

Engine::~Engine()
{
}

void Engine::init()
{
	addChild(fontHandler.get());
	addChild(shaderHandler.get());
	addChild(meshHandler.get());
	addChild(actorFactory.get());
	addChild(guiHandler.get());
	addChild(eventHandler.get());
	//addChild(timeRewinder.get());
	addChild(terrainHandler.get());
	addChild(skyboxHandler.get());
	addChild(textureHandler.get());
	addChild(xmlHandler.get());
	addChild(randomSpawn.get());
#ifdef __ANDROID__
#else
	//addChild(lightHandler.get());
#endif
	eventHandler->init();
	guiHandler->loadStatesFromFiles();
	terrainHandler->init();
	terrainHandler->generateTerrain(-0.5f, -0.5f);
	skyboxHandler->init();
	skyboxHandler->loadTexture("res/textures/skybox/", SKYBOX_DAY);
	skyboxHandler->loadTexture("res/textures/nightbox/", SKYBOX_NIGHT);

	weatherParticles->init(this, 0.12f, this->textureHandler->loadTexture("res/textures/snowflake.png"), 0.35f, true);
	weatherParticles->createPoint(1000, glm::vec3(0.f, 0.f, 0.f), 1);
	particleSystem->init(this, 0.02f, this->textureHandler->loadTexture("res/textures/snowball.png"), 0.50f, true);

	//lightHandler->init();
	//lightHandler->addLight(glm::vec3(0.f, 0.f, 0.f), 15.f);

	//timeRewinder->init();
	this->deltaTime = 0.f;

	previousTime = SDL_GetTicks();


	addListeners();
}
void Engine::run()
{
	gameLoop();
}

void Engine::gameLoop()
{
	SDL_StartTextInput();
	while (gameState != STATE_QUITTING)
	{
		switch (gameState)
		{
		case STATE_MAINMENU:
			initMainmenu();
			updateMainmenu();
			break;
		case STATE_CHARACTERSELECT:
			initCharacterSelect();
			updateCharacterSelect();
			break;
		case STATE_HOSTING:
			initHosting();
			updateHosting();
			break;
		case STATE_JOINING:
			initJoining();
			updateJoining();
			break;
		case STATE_RUNNING:
			initRunning();
			updateRunning();
			break;
		case STATE_GAMEOVER:
			initGameover();
			updateGameover();
			break;
		case STATE_SETTINGS:
			initSettings();
			updateSettings();
			break;
		default:
			break;
		}
	}
	//one function should be enough
	initQuitting();
	//updateQuitting();
	
	physicsHandler.reset(nullptr);
	SDL_StopTextInput();
}


void Engine::genericUpdate()
{
	
	eventHandler->handleEvents();
	deltaTime = (SDL_GetTicks() - previousTime) / 1000.f; //1000 = ms per second
	previousTime = SDL_GetTicks();
	guiHandler->update();
	
	if (debug)
	{
		if (oneSecondTimer.hasEnded())
		{
			printf("FPS: %i\n", fps);
			fps = 0;
			oneSecondTimer.restart();
		}
		else
		{
			fps++;
		}
	}
}

void Engine::genericDraw()
{
	guiHandler->draw(gameState);
	graphicsHandler->swapBuffers();
	graphicsHandler->clearWindow();
}

ActorFactory* Engine::getActorFactory() const
{
	return actorFactory.get();
}

PhysicsHandler* Engine::getPhysicsHandler() const
{
	return physicsHandler.get();
}

AudioHandler* Engine::getAudioHandler() const
{
	return audioHandler.get();
}

GraphicsHandler* Engine::getGraphicsHandler() const
{
	return graphicsHandler.get();
}

TextureHandler* Engine::getTextureHandler() const
{
	return textureHandler.get();
}

MeshHandler* Engine::getMeshHandler() const
{
	return meshHandler.get();
}

GuiHandler* Engine::getGuiHandler() const
{
	return guiHandler.get();
}

ShaderHandler* Engine::getShaderHandler() const
{
	return shaderHandler.get();
}

EventHandler* Engine::getEventHandler() const
{
	return eventHandler.get();
}

TimeRewinder* Engine::getTimeKeeper() const
{
	return timeRewinder.get();
}

FontHandler * Engine::getFontHandler() const
{
	return fontHandler.get();
}
TerrainHandler * Engine::getTerrainHandler() const
{
	return terrainHandler.get();
}
SkyboxHandler * Engine::getSkyboxHandler() const
{
	return skyboxHandler.get();
}
ParticleSystem * Engine::getParticleSystem() const
{
	return particleSystem.get();
}
#ifdef __ANDROID__
#else
NetworkClientHandler * Engine::getClientHandler() const
{
	return networkClientHandler.get();
}

NetworkServerHandler * Engine::getServerHandler() const
{
	return networkServerHandler.get();
}
#endif


LevelLoader * Engine::getLevelLoader() const
{
	return levelLoader.get();
}

XmlHandler * Engine::getXmlHandler() const
{
	return xmlHandler.get();
}

RandomSpawn * Engine::getRandomSpawn() const
{
	return randomSpawn.get();
}

#ifdef __ANDROID__
krem::android::AssetManager * Engine::getAssetMgr() const
{
	return assetMgr.get();
}
#endif
template<typename T>
void Engine::addChild(T t)
{
	t->setParent(*this);
}

void Engine::setPlayer(Actor& newplayer)
{
	Actor* tempactor = actorFactory->getActor(newplayer.getActorId());
	this->player = tempactor;
	//this->player = &newplayer;
}

Actor * Engine::getPlayer() const
{
	return this->player;
}

int Engine::getSelectedCharacter() const
{
	return selectedCharacter;
}

void Engine::setGameState(int gameState)
{
	this->gameState = gameState;
}

int Engine::getGameState()
{
	return gameState;
}

void Engine::setSeed(unsigned int seed)
{
	randomSeed = seed;
	srand(randomSeed);
}

unsigned int Engine::getSeed()
{
	return randomSeed;
}

float Engine::getDeltaTime()
{
	return deltaTime;
}

void Engine::connectionlistener()
{
#ifdef __ANDROID__
#else
	//while(STATE_HOSTING)
	while (1) //fix this when states are chikorita
	{
		networkServerHandler->listen();
	}
#endif
}
//TODO: Decouple this and have all the listeners be in a logical place instead of just smashing them in the engine
void Engine::addListeners()
{
	//main menu screen
	if (guiHandler->getButton("mainsingleplayer") != nullptr)
	{
		guiHandler->getButton("mainsingleplayer")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainjoingame") != nullptr)
	{
		guiHandler->getButton("mainjoingame")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainhostgame") != nullptr)
	{
		guiHandler->getButton("mainhostgame")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainsettings") != nullptr)
	{
		guiHandler->getButton("mainsettings")->addSubscriber(this);
	}
	if (guiHandler->getButton("mainexitgame") != nullptr)
	{
		guiHandler->getButton("mainexitgame")->addSubscriber(this);
	}

	//character select screen
	if (guiHandler->getButton("selectPlay") != nullptr)
	{
		guiHandler->getButton("selectPlay")->addSubscriber(this);
	}
	if (guiHandler->getButton("selectExit") != nullptr)
	{
		guiHandler->getButton("selectExit")->addSubscriber(this);
	}

	if (guiHandler->getContainer("characterSelect") != nullptr)
	{
		guiHandler->getContainer("characterSelect")->addSubscriber(this);
	}
	if (guiHandler->getTextBox("selectName") != nullptr)
	{
		guiHandler->getTextBox("selectName")->addSubscriber(this);
	}

	//setting screen
	if (guiHandler->getButton("settingback") != nullptr)
	{
		guiHandler->getButton("settingback")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingmastervolume") != nullptr)
	{
		guiHandler->getSlider("settingmastervolume")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingmusic") != nullptr)
	{
		guiHandler->getSlider("settingmusic")->addSubscriber(this);
	}
	if (guiHandler->getSlider("settingsoundeffects") != nullptr)
	{
		guiHandler->getSlider("settingsoundeffects")->addSubscriber(this);
	}
	
	if (guiHandler->getCheckBox("settingvsync") != nullptr)
	{
		guiHandler->getCheckBox("settingvsync")->addSubscriber(this);
	}

	if (guiHandler->getDropDown("settingresolution") != nullptr)
	{
		guiHandler->getDropDown("settingresolution")->addSubscriber(this);
	}
	if (guiHandler->getDropDown("settingdisplay") != nullptr)
	{
		guiHandler->getDropDown("settingdisplay")->addSubscriber(this);
	}

	//running screen
	if (guiHandler->getButton("runningpause") != nullptr)
	{
		guiHandler->getButton("runningpause")->addSubscriber(this);
	}

	//hosting screen
	if (guiHandler->getButton("hostingback") != nullptr)
	{
		guiHandler->getButton("hostingback")->addSubscriber(this);
	}
	if (guiHandler->getButton("hostingstart") != nullptr)
	{
		guiHandler->getButton("hostingstart")->addSubscriber(this);
	}

	if (guiHandler->getTextBox("hostingipaddress") != nullptr)
	{
		guiHandler->getTextBox("hostingipaddress")->addSubscriber(this);
	}
	if (guiHandler->getTextBox("hostingport") != nullptr)
	{
		guiHandler->getTextBox("hostingport")->addSubscriber(this);
	}

	//joining screen
	if (guiHandler->getButton("joiningback") != nullptr)
	{
		guiHandler->getButton("joiningback")->addSubscriber(this);
	}
	if (guiHandler->getButton("joiningconnect") != nullptr)
	{
		guiHandler->getButton("joiningconnect")->addSubscriber(this);
	}
	if (guiHandler->getTextBox("joiningipaddress") != nullptr)
	{
		guiHandler->getTextBox("joiningipaddress")->addSubscriber(this);
	}
	if (guiHandler->getTextBox("joiningport") != nullptr)
	{
		guiHandler->getTextBox("joiningport")->addSubscriber(this);
	}

	//game over screen
	if (guiHandler->getButton("gameoverplayagain") != nullptr)
	{
		guiHandler->getButton("gameoverplayagain")->addSubscriber(this);
	}
	if (guiHandler->getButton("gameovermainmenu") != nullptr)
	{
		guiHandler->getButton("gameovermainmenu")->addSubscriber(this);
	}
}

//TODO: same as function above. Remove the shit out of this so it is in a better place
// All of these are messaged received from different gui elements 
void Engine::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(Button))
	{
		const char * tempMessage = message.getVariable(0);
		if (strcmp(tempMessage, "mainsingleplayer") == 0)
		{
			nextGameState = STATE_RUNNING;
			gameState = STATE_CHARACTERSELECT;
		}
		if (strcmp(tempMessage, "mainjoingame") == 0)
		{
			nextGameState = STATE_JOINING;
			gameState = STATE_CHARACTERSELECT;
		}
		if (strcmp(tempMessage, "mainhostgame") == 0)
		{
			nextGameState = STATE_HOSTING;
			gameState = STATE_CHARACTERSELECT;
		}
		if (strcmp(tempMessage, "selectPlay") == 0)
		{
			gameState = nextGameState;
		}
		if (strcmp(tempMessage, "mainsettings") == 0)
		{
			gameState = STATE_SETTINGS;
		}
		if (strcmp(tempMessage, "mainexitgame") == 0)
		{
			gameState = STATE_QUITTING;
		}
		if (strcmp(tempMessage, "settingback") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		
		if (strcmp(tempMessage, "selectExit") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "runningpause") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "hostingback") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "hostingstart") == 0)
		{
#ifdef __ANDROID__
#else
			networkServerHandler->sendPacket(gameState);
#endif
			gameState = STATE_RUNNING;
		}
		if (strcmp(tempMessage, "joiningback") == 0)
		{
			gameState = STATE_MAINMENU;
		}
		if (strcmp(tempMessage, "joiningconnect") == 0)
		{
			std::cout << "hehehehe doesn't work lololol" << std::endl;
		}
		if (strcmp(tempMessage, "gameoverplayagain") == 0)
		{
			gameState = STATE_RUNNING;
		}
		if (strcmp(tempMessage, "gameovermainmenu") == 0)
		{
			gameState = STATE_MAINMENU;
		}
	}
	if (message.getSenderType() == typeid(CheckBox))
	{
		const char * tempMessage = message.getVariable(0);
		bool tempStatus = message.getVariable(1);
		if (strcmp(tempMessage, "settingvsync") == 0)
		{
			if (tempStatus)
			{
				SDL_GL_SetSwapInterval(1);
			}
			else
			{
				SDL_GL_SetSwapInterval(0);
			}
		}
	}
	if (message.getSenderType() == typeid(DropDown))
	{
		const char * tempMessage = message.getVariable(0);
		const char * temp = message.getVariable(1);
		if (strcmp(tempMessage, "settingresolution") == 0)
		{
			
			glm::vec2 resolution = graphicsHandler->getWindowResolution();
			//glViewport(0, 0, 640, 360);
		}
		if (strcmp(tempMessage, "settingdisplay") == 0)
		{
			if (strcmp(temp, "Fullscreen") == 0)
			{
				//graphicsHandler->updateWindow(SDL_WINDOW_FULLSCREEN);
			}
			else if (strcmp(temp, "Windowed") == 0)
			{
				graphicsHandler->updateWindow(0);
			}
			else if (strcmp(temp, "Borderless") == 0)
			{
				graphicsHandler->updateWindow(SDL_WINDOW_BORDERLESS);
			}
		}
	}
	if (message.getSenderType() == typeid(Slider))
	{
		const char * tempMessage = message.getVariable(0);
		int temp = message.getVariable(1);
		if (strcmp(tempMessage, "settingmastervolume") == 0)
		{
			getAudioHandler()->setMasterVolume(temp);
		}
		if (strcmp(tempMessage, "settingmusic") == 0)
		{
			getAudioHandler()->setMusicVolume(temp);
		}
		if (strcmp(tempMessage, "settingsoundeffects") == 0)
		{
			getAudioHandler()->setSoundVolume(temp);
		}

	}
	if (message.getSenderType() == typeid(TextBox))
	{
		const char * tempMessage = message.getVariable(0);

		if (strcmp(tempMessage, "selectName") == 0)
		{
			tempMessage = message.getVariable(1);
		}
		if (strcmp(tempMessage, "hostingipaddress") == 0)
		{

		}
		if (strcmp(tempMessage, "hostingport") == 0)
		{

		}
		if (strcmp(tempMessage, "joiningipaddress") == 0)
		{

		}
		if (strcmp(tempMessage, "joiningport") == 0)
		{

		}
	}
	if (message.getSenderType() == typeid(Container))
	{
		const char * tempMessage = message.getVariable(0);

		if (strcmp(tempMessage, "characterSelect") == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "boxSelected") == 0)
			{
				selectedCharacter = message.getVariable(2);

				playerString = "res/actors/player" + std::to_string(selectedCharacter) + ".xml";
			}
		}
	}
}

void Engine::increaseEnemiesKilled()
{
	enemiesKilled++;
}

bool Engine::getJoining()
{
	return joining;
}

void Engine::drawMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	this->skyboxHandler->draw(viewProjection);	//updates and draws
	this->terrainHandler->draw(viewProjection);
	this->weatherParticles->draw(viewProjection);
}

void Engine::updateMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	this->skyboxHandler->update(viewProjection, this->deltaTime);
	this->terrainHandler->update();
	this->weatherParticles->update(this->deltaTime);
}


