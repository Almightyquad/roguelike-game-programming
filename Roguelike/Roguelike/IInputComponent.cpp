#include "IInputComponent.h"
#include "Engine.h"
IInputComponent::~IInputComponent()
{
	if (parentActor->getCategory() == "Pickup")
	{
		parentEngine->getEventHandler()->removeSubscriber(this);
	}
	
}
void IInputComponent::init(Engine * parentEngine)
{
	this->parentEngine = parentEngine;
	if (parentActor->getCategory() == "Pickup")
	{
		parentEngine->getEventHandler()->addSubscriber(this);
	}
	

	// //old way of subscribing to input. fucks with input over network
}

InputValues IInputComponent::getInputValues()
{
	return input;
}
