#pragma once
#include <map>
#include <string>
#include "InputCreator.h"

class InputFactory
{
public:
	std::unique_ptr<IInputComponent> create(const std::string& itemName, tinyxml2::XMLElement* xmlElement);

	void registerIt(const std::string& itemName, InputCreator* physicsCreator);

private:
	std::map<std::string, InputCreator*> table;
};