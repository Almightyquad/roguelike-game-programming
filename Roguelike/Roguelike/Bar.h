#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"

class Engine;
class GuiState;
/** A bar. Examples: health bar, mana bar, loading bar */
class Bar
{
public:
	Bar(tinyxml2::XMLElement *xmlElement);
	~Bar();

	/** Updates this object. */
	void update();

	/**
	 * Sets current value of the bar.
	 *
	 * @param	size	The current value of the bar.
	 */
	void setCurrentValue(float size);

	/**
	 * Sets maximum value of the bar
	 *
	 * @param	size	The maximum size of the bar.
	 */
	void setMaxValue(float size);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The gui view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	sets the engine as its parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Gets the name of the bar.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	float maxValue;
	float currentValue;

	float width;
	float height;

	glm::vec4 color;

	tinyxml2::XMLElement *originNode;

	std::string name;

	Transform defaultBarTransform;
	Transform backgroundTransform;
	Transform barTransform;

	Mesh *mesh;

	Shader *shader;
	Shader *shaderBar;

	Texture *backgroundTexture;
	Texture *barTexture;
};

