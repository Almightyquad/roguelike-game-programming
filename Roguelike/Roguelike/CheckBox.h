#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"

class Engine;
class GuiState;
/** A check box. Can be used to create check boxes that is in a true or false state */
class CheckBox : public IObservable
{
public:
	CheckBox(tinyxml2::XMLElement *xmlElement);
	~CheckBox();

	/** Updates this object. */
	void update();

	/**
	 * Determines if mouse click left is inside the checkbox.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Determines if mouse click right is inside the checkbox.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Determines if mouse release is inside button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	std::string name;

	bool clicked;
	bool status;
	bool defaultStatus;

	int currentTexture;
	int textureAtlasSizeX;
	int textureAtlasSizeY;

	tinyxml2::XMLElement *originNode;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

