[header]
width=8
height=8
tilewidth=32
tileheight=32
orientation=orthogonal

[tilesets]
tileset=../../textures/tiles/castleFg.png,32,32,0,0

[layer]
type=Background
data=
0,93,95,96,0,0,0,0,
0,108,110,111,0,0,0,0,
109,109,109,110,96,0,0,0,
109,125,109,103,104,0,0,0,
109,109,109,118,119,0,0,0,
109,109,109,133,134,0,0,0,
0,0,0,109,109,110,0,0,
0,0,0,0,0,0,0,0

[layer]
type=Props
data=
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0

[layer]
type=Collision
data=
0,0,0,0,0,0,0,0,
25,58,29,20,20,20,0,0,
40,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,19,25,0,0,0,0,0,
20,37,40,58,58,59,25,0,
35,35,36,20,20,20,40,0,
35,35,35,35,35,35,40,0

[layer]
type=Foreground
data=
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0

