#version 330 core

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform vec2 size;
uniform int spriteNo;
uniform float scale = 1.0f;

float offset = (scale - 1.0) * 0.5;

void main()
{

	int columns = int(size.x / 1.f);
	float x = spriteNo % columns;
	float y = spriteNo / columns;
	
	vec4 button	= texture2D(texture0, (vec2(x, y) + vec2(1.f, 1.f) * texCoord) / size);
	vec4 font = texture2D(texture1, (texCoord * scale) - offset);
	
	if (button.a <= 0.9)
	{
		discard;
	}
	
	color = mix(button, font, font.a);
}