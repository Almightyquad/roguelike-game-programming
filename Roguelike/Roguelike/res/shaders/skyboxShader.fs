#version 330 core

in vec3 textureCoords;

out vec4 color;

uniform samplerCube texture0;
uniform samplerCube texture1;
uniform float scale;

void main(void)
{
	vec4 day = texture(texture0, textureCoords);
	vec4 night = texture(texture1, textureCoords);
    color = mix(day, night, scale);
}