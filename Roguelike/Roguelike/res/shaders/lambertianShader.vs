#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;

out vec2 texCoord0;
out vec3 normal0;

uniform mat4 transform;

void main()
{
	texCoord0 = texCoords;
	normal0 = (transform * vec4(normal, 0.0)).xyz; //might need to invert and transpose
	gl_Position = transform * vec4(position, 1.0);
}