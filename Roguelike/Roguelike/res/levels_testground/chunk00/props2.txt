<?xml version="1.0" encoding="UTF-8"?>
<map version="1.0" orientation="orthogonal" renderorder="right-down" width="8" height="8" tilewidth="32" tileheight="32" nextobjectid="1">
 <tileset firstgid="1" name="castleFg.png" tilewidth="32" tileheight="32">
  <image source="../../textures/tiles/castleFg.png" width="480" height="352"/>
 </tileset>
 <layer name="Background" width="8" height="8">
  <data encoding="base64" compression="zlib">
   eJxjYBjZAAABAAAB
  </data>
 </layer>
 <layer name="Props" width="8" height="8">
  <data encoding="base64" compression="zlib">
   eJxjYBjZAAABAAAB
  </data>
 </layer>
 <layer name="Collision" width="8" height="8">
  <data encoding="base64" compression="zlib">
   eJxjYBjZAAABAAAB
  </data>
 </layer>
 <layer name="Foreground" width="8" height="8">
  <data encoding="base64" compression="zlib">
   eJxjYKAO6KZQvwIRavQptAMbAACDVADb
  </data>
 </layer>
</map>
