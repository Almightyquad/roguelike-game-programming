#pragma once
#include "IInputComponent.h"
//#include "Message.h"

class InputPickup final : public IInputComponent
{
public:
	InputPickup(tinyxml2::XMLElement *xmlElement);
	~InputPickup();
	virtual void update(float deltatime) override;
	virtual void receiveMessage(const Message &message) override;
	//virtual void receiveNetworkInput(InputValues newInput) override;
	//InputValues getInputValues();
private:
	glm::vec3 mousePos;
private:
};