#pragma once
#include <SDL.h>
#include <stdio.h>
#include <string>
#include <sstream>

/**
 * @class	Timer
 *
 * @brief	A timer.
 * @details	Can be used to find elapsed time since creation.
 * 			Can be created with a duration, where hasEnded() will be true after the given time(in seconds)
 * @param	durationTakes an optional float as duration in seconds
 * @see		Timer::hasEnded()
 */
class Timer
{
public:
	//Initializes variables
	Timer(float duration = 0);
	~Timer() {};
	//The various clock actions
	
	void start();
	void stop();
	/**
	* @fn	void Timer::restart();
	*
	* @brief	Restarts timer. Duration is the same.
	*
	*/
	void restart();

	void pause();
	void unpause();
	/**
	* @fn	void Timer::restart();
	*
	* @brief	Sets Duration. Will overwrite default duration.
	* 
	* @param	duration	Desired duration.
	*/
	void setDuration(float duration);

	//Gets the timer's time
	float getTicks();

	//Checks the status of the timer
	bool isStarted();
	bool isPaused();
	bool hasEnded();
private:
	//The clock time when the timer started
	Uint32 startTicks;

	//The ticks stored when the timer was paused
	Uint32 pausedTicks;
	float duration;
	//The timer status
	bool paused;
	bool started;
	
};
