#include "PickUpBowAndArrow.h"
#include "Engine.h"

PickUpBowAndArrow::PickUpBowAndArrow(tinyxml2::XMLElement * xmlElement)
	: IPickUpComponent(xmlElement),
	arrowCooldown(0.15f),
	arrows(75)
{
}

void PickUpBowAndArrow::onToss()
{
	owner = nullptr;
	active = false;
}

void PickUpBowAndArrow::onActivation()
{
	active = true;
	dynamic_cast<InventoryComponent*>(owner->getComponent("InventoryComponent"))->setActive(this);
}

void PickUpBowAndArrow::onDeactivation()
{
	active = false;
}

void PickUpBowAndArrow::fire(krem::Vector2f mousePos)
{
	if (parentEngine->getJoining())
	{
		return;
	}
	if (active && arrowCooldown.hasEnded() && arrows > 0)
	{
		if (owner != nullptr)
		{
			int velocityMultiplier = 50;
			glm::vec3 ownerActorPosition = dynamic_cast<PhysicsComponent*>(owner->getComponent("PhysicsComponent"))->getPosition();
			parentEngine->getActorFactory()->createActor("res/actors/arrow.xml", glm::vec3(ownerActorPosition), glm::vec3(mousePos.x * velocityMultiplier, mousePos.y * velocityMultiplier, 0));
			arrowCooldown.restart();
			arrows--;
		}
	}

}