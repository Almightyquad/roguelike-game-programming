#pragma once
#include "IAudioHandler.h"
#include <SDL_mixer.h>
#include <iostream>
#include <unordered_map>

class AudioHandler : public Handler
{
public:
	AudioHandler();
	~AudioHandler();

	/**
	 * Loads sound effect from a path.
	 *
	 * @param	soundPath	Full pathname of the sound file.
	 */
	void loadSoundEffect(std::string soundPath);

	/**
	 * Loads music files from a path.
	 *
	 * @param	musicPath	Full pathname of the music file.
	 */
	void loadMusic(std::string musicPath);

	/**
	 * Play sound using the path
	 *
	 * @param	soundPath	Full pathname of the sound file.
	 */
	void playSound(std::string soundPath);

	/**
	 * Play music using the path
	 *
	 * @param	musicPath	Full pathname of the music file.
	 */
	void playMusic(std::string musicPath);

	/** Stops the current music file that is playing. */
	void stopMusic();

	/**
	 * Sets master volume.
	 *
	 * @param	volume	The volume.
	 */
	void setMasterVolume(int volume);

	/**
	 * Sets sound volume.
	 *
	 * @param	volume	The volume.
	 */
	void setSoundVolume(int volume);

	/**
	 * Sets music volume.
	 *
	 * @param	volume	The volume.
	 */
	void setMusicVolume(int volume);
protected:

private:
	int masterVolume;
	std::string currentMusic;
	std::unordered_map<std::string, Mix_Chunk*> soundEffects;
	std::unordered_map<std::string, Mix_Music*> music;
};