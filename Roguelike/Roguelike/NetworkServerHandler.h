#pragma once
#include <SDL_net.h>
#include <iostream>
#include "INetworkHandler.h"
#include <vector>
#include "Observable.h"

class Actor;

struct Client
{
	TCPsocket socket;
	IPaddress* IP;
	Actor* actor;
};

class NetworkServerHandler : public  INetworkHandler, public IObservable
{
public:
	NetworkServerHandler();
	~NetworkServerHandler();
	void init();
	void initClients();
	void listen();
	void sendPacket(int gameState) override;
	void receivePacket(int gameState) override;
	//void receiveInit();
	void receiveHosting();
	void receiveRunning();
	//void sendInit();
	void sendHosting();
	void sendRunning();
	
	void packActorId(NetworkActorData* newNetworkData, Actor* actorToSend);

private:
	int datasize;
	SDLNet_SocketSet socketSet;
	TCPsocket socket; //listener
	std::vector<std::unique_ptr<Client>> clients;
	std::unique_ptr<Client> newClient;
	//std::vector<TCPsocket> clientSocket; //socket will be added for clients 
	int synchronizedActors;
	int numberOfClients = 0;
	char* buffer;
};