#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"
#include "common.h"

class Engine;
class GuiState;
/** A slider. Used to create sliders that go from a minimum to a maximum value. */
class Slider : public IObservable
{
public:
	Slider(tinyxml2::XMLElement *xmlElement);
	~Slider();

	/** Updates this object. */
	void update();

	/** Check mouse over to change the texture to give feedback to the user*/
	void checkMouseOver();

	/**
	 * Determines if mouse click left is inside either left/right arrow of slider or the slider it self..
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Determines if mouse click right is inside either left/right arrow of slider or the slider it self..
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Determines if mouse release is inside either left/right arrow of slider or the slider it self.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	void setParent(GuiState& parentPtr);

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	std::vector<float> values;

	tinyxml2::XMLElement *originNode;

	std::string name;

	bool slider;
	bool left;
	bool right;
	bool fontDisplay;
	float distance;

	int maxValue;
	int minValue;
	int currentValue;
	int valueInterval;
	int textureAtlasSize;
	int currentSliderTexture;
	int currentRightTexture;
	int currentLeftTexture;

	//TODO: rewrite this to be dynamic as fuck
	Transform transform[3];
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
};

