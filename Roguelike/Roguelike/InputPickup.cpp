#include "InputPickup.h"
#include "Engine.h"
#include "Message.h"
#include "Actor.h"

InputPickup::InputPickup(tinyxml2::XMLElement * xmlElement):
	IInputComponent(xmlElement)
{
}

InputPickup::~InputPickup()
{
	parentEngine->getEventHandler()->removeSubscriber(this);
}

void InputPickup::update(float deltatime)
{
	if (input.mouse)
	{
		if (input.mousePosition.x > -1 && input.mousePosition.x < 1 || input.mousePosition.y > -1 && input.mousePosition.y < 1)
		{
			dynamic_cast<IPickUpComponent*>(parentActor->getComponent("PickUpComponent"))->fire(input.mousePosition);
		}
	}
}

void InputPickup::receiveMessage(const Message & message)
{
	glm::vec3 temp;
	if (message.getSenderType() == typeid(EventHandler))
	{
		int number = message.getVariable(0);
		inputChanged = true;
		switch (number)
		{
		case SDL_MOUSEBUTTONDOWN:
			input.mouse = message.getVariable(1);
			inputChanged = false;
			break;
		case SDL_MOUSEBUTTONUP:
			input.mouse = message.getVariable(1);
			inputChanged = false;
			break;
		case SDL_MOUSEMOTION:
			temp = message.getVariable(1);
			input.mousePosition = { temp.x,temp.y };
			break;
		}
	}
	if (input.mouse)
	{
		dynamic_cast<IPickUpComponent*>(parentActor->getComponent("PickUpComponent"))->fire(input.mousePosition);
		//this->postMessage(Message(this, "mouseEvent", input.mouse, input.mousePosition));
		//fireballCooldown.restart();
	}
}


