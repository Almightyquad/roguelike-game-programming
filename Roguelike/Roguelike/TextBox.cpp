#include "TextBox.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


TextBox::TextBox(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	currentTexture = 0;
	focus = false;
	input = " ";
	int maxLength = 100;
}

TextBox::~TextBox()
{
}

void TextBox::update()
{
	if (focus)
	{
		std::string tempUserInput = parentEngine->getEventHandler()->getInputText();
		input = tempUserInput;
		
		//clears the textbox when gaining focus (Not ideal but works for now)
		if (input != "")
		{
			const char * tempInput = input.c_str();
			const char * tempName = name.c_str();

			//sends the name of the text box and its current entered text.
			this->postMessage(Message(this, tempName, tempInput));
		}
		else
		{
			input = defaultValue;
		}
	}
	else
	{
		
	}
	
}

bool TextBox::onFocus()
{
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		focus = true;
		parentEngine->getEventHandler()->setInputStatus(true);
		return focus;
	}
	else
	{
		lostFocus();
	}
	return focus;
}

bool TextBox::lostFocus()
{
	focus = false;
	//parentEngine->getEventHandler()->setInputStatus(false);
	return false;
}

void TextBox::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("fontPath"))
	{
		std::string fontPathTemp = this->originNode->FirstChildElement("fontPath")->GetText();
		font = parentEngine->getFontHandler()->loadFont(fontPathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}
	
	if (this->originNode->FirstChildElement("maxLength"))
	{
		this->originNode->FirstChildElement("maxLength")->QueryIntText(&maxLength);
	}
	else if (xmlDebug)
	{
		printf("Missing maxLength\n");
	}

	if (this->originNode->FirstChildElement("defaultValue"))
	{
		defaultValue = this->originNode->FirstChildElement("defaultValue")->GetText();
		input = defaultValue;
	}
	else if (xmlDebug)
	{
		printf("Missing defaultValue\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		pathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->texture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	transform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
}

void TextBox::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
	shader->loadFloat(U_SCALE, 1.5f);

	if (input == defaultValue)
	{
		font->update(input, glm::vec3(50, 50, 50));
	}
	else
	{
		font->update(input, glm::vec3(0, 0, 0));
	}

	shader->loadInt(U_TEXTURE1, 1);
	font->bind(1);

	texture->bind(0);
	mesh->draw();
}

void TextBox::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

GuiState* TextBox::getParent()
{
	return parentGuiState;
}

std::string TextBox::getName()
{
	return name;
}
