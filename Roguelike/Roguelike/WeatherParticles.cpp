#include "WeatherParticles.h"

void WeatherParticles::update(float deltaTime)
{
	for (std::size_t i = 0; i < particles.size(); i++)
	{
		particles[i].life -= (1.f * deltaTime);	//lose 1hp/sec

		if (particles[i].life <= 0)	//kill particle
		{
			particles[i].velocity = { particles[i].velocity.x + (rand()%2 - 1.f) * 0.1f, particles[i].velocity.y, particles[i].velocity.z + (rand()%2 - 1.f) * 0.1f };
			positions[i].y = ((rand() % 500) / 100.f) + 15.f;
			positions[i].x = static_cast<float>(rand() % 320);
			positions[i].z = (rand() % 50) - 15.f;
			particles[i].life = static_cast<float>(rand() % 100);
			float color = (rand() % 70) / 100.f;
			colors[i] = glm::vec4(color, color, 0.7f, 1.f);
		}

		if (this->gravity == true)
		{
			if (particles[i].velocity.y > -this->maxVelocity)
			{
				particles[i].velocity.y -= this->maxVelocity / 4.f;
			}
			else
			{
				particles[i].velocity.y = -this->maxVelocity;
			}
		}

		positions[i] += (particles[i].velocity * deltaTime);
	}
}