#pragma once
#include "Mesh.h"
#include <iostream>
#include <unordered_map>
#include <memory>
#include "Handler.h"
/**
 * @class	MeshHandler
 *
 * @brief	A mesh handler.
 */
class MeshHandler : public Handler
{
public:

	/**
	 * @fn	MeshHandler::MeshHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	MeshHandler();

	/**
	 * @fn	MeshHandler::~MeshHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~MeshHandler();

	/**
	 * @fn	Mesh* MeshHandler::loadModel(const std::string & filePath);
	 *
	 * @brief	Loads a model.
	 *
	 * @author	Snikur
	 * @date	12/17/15
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	pointer to model.
	 */
	Mesh* loadModel(const std::string & filePath);

private:
	std::unordered_map<std::string, std::unique_ptr<Mesh>> meshes;
};

