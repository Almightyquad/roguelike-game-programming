#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include "tinyxml2-master\tinyxml2.h"
#include "ActorComponent.h"

class IInventoryComponent : public ActorComponent
{
public:
	IInventoryComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	virtual ~IInventoryComponent(){};
private:
	int maxNumberOfItems;
	int maxStack;
	std::map<int, std::string> items;
	//std::vector<std::string> items;
	std::vector<int> numberOfItems;
};

