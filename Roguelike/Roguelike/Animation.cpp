#include "Animation.h"


Animation::Animation()
{
}
Animation::~Animation()
{
}

void Animation::config(float animRate, int animFrames, int frameStart)
{
	this->animRate = animRate;
	this->animFrames = animFrames;
	this->frameStart = frameStart;
}

void Animation::update(float deltaTime)
{
	this->curAnim += deltaTime * this->animRate;
}

void Animation::reset()
{
	this->curAnim = 0.0f;
}

int Animation::getFrame()
{
	return this->frameStart + (static_cast<int>(this->curAnim) % this->animFrames);
}
