#pragma once
#include "ActorComponent.h"
#include "ParticleSystem.h"

class IParticleComponent : public ActorComponent
{
public:
	IParticleComponent(tinyxml2::XMLElement* xmlElement)
		:ActorComponent(xmlElement) {};
	virtual ~IParticleComponent() {};
};