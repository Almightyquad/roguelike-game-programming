#pragma once
#include "IPickUpComponent.h"

class PickUpManaPotion final : public IPickUpComponent
{
public:
	PickUpManaPotion(tinyxml2::XMLElement *xmlElement) : IPickUpComponent(xmlElement) {
	};
	virtual void onPickup(Actor* newOwner) override;
private:
	int manaValue = 50;
};