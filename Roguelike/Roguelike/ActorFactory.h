#pragma once
#include <string>
#include <memory>
#include <map>
#include <unordered_map>
#include <vector>
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "common.h"
#include "Receiver.h"
#include "XmlHandler.h"

#include "PickUpFactory.h"
#include "AIFactory.h"
#include "PhysicsFactory.h"
#include "InputFactory.h"

class Actor;
class Engine;
/**
 * @class	ActorFactory
 *
 * @brief	The Actor Factory creates and contains all actors.
 * @details	There is only one ActorFactory, held by engine.
 * 			All actors need to be created by calling createActor.
 * 			All actors are created from xml files.
 */
class ActorFactory : public IReceiver
{
public:
	ActorFactory();
	~ActorFactory();
	
	/**
	 * @fn	void ActorFactory::createActor(const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity =
	 *
	 * @brief	Creates an actor.
	 *
	 * @param	xmlPath 	Full pathname of the XML file.
	 * @param	position	The position.
	 * @param	velocity	Initial velocity of the created actor
	 */
	Actor* createActor(const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity = { 0, 0, 0 });
	
	/**
	 * @fn	void ActorFactory::createActor(const std::string xmlPath, const glm::vec3 position, const glm::vec2 tilePosition =
	 *
	 * @brief	Creates an actor with graphics from a tileAtlas.
	 *
	 * @param	xmlPath 	Full pathname of the XML file.
	 * @param	position	The position.
	 * @param	tilePosition	Position of the tile in the atlas(in x and y). size per tile is specified in the xml file
	 */
	void createActor(const std::string xmlPath, const glm::vec3 position, const glm::vec2 tilePosition);
	

	Actor* insertActorFromNetwork(const std::string xmlPath, const glm::vec3 position, const actorID customActorID, const glm::vec3 velocity = { 0, 0, 0 } );

	/**
	 * @fn	void ActorFactory::deleteActor(actorID actorId);
	 *
	 * @brief	Deletes the actor.
	 * @details Deletes the actor. Should only be called when deletequeue is cleaned unless you know what you're doing.
	 *
	 * @param	actorToDelete will be deleted.
	 */
	void deleteActor(Actor* actorToDelete);
	
	/**
	 * @fn	void ActorFactory::updateAll(const float deltaTime);
	 *
	 * @brief	Updates all actors in actorMap.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void updateAll(const float deltaTime);
	
	/**
	 * @fn	void ActorFactory::init();
	 *
	 * @brief	Initialises the actorFactory.
	 */
	void init();
	Actor* getActor(actorID actorId);
	
	void setParent(Engine& parentPtr);
	Engine* getParent();
	


	std::vector<Actor*> getActors();
	
	unsigned int getCurrentActorID(void);
	void addMessageListener(actorID actorId);
	void receiveMessage(const Message &message);
	void draw();
	
	void addToDeleteQueue(Actor* actorToDelete);
	void addToSwapActiveQueue(Actor* actorToSwapActive);

	
	PickUpFactory* getPickUpFactory();
	AIFactory* getAIFactory();
	PhysicsFactory* getPhysicsFactory();
	InputFactory* getInputFactory();

	void clearFactory();

private:
	tinyxml2::XMLDocument* doc;
	void addChild(Actor* actor);
	unsigned int currentActorId;
	unsigned int getNextActorId(void);
	std::map<actorID, std::unique_ptr<Actor>> actorMap; //contains all movable objects (player(s), enemies, projectiles++)
	std::vector<std::unique_ptr<Actor>> actorVector; //contains all static objects (tiles, flowers, static lights++)
	std::vector<Actor*> deleteQueue;
	std::vector<Actor*> activeSwapQueue;
	Engine* parentEngine;

	std::unique_ptr<PickUpFactory> pickUpFactory;
	std::unique_ptr<AIFactory> aIFactory;
	std::unique_ptr<PhysicsFactory> physicsFactory;
	std::unique_ptr<InputFactory> inputFactory;
};