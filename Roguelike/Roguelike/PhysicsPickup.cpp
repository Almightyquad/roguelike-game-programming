#include "PhysicsPickup.h"
#include "Actor.h"

void PhysicsPickup::onCollision(int fixtureUserData, Actor * collidedActor)
{
	switch (fixtureUserData)
	{
	case CATEGORY_PLAYER:
		parentActor->onHit();
		break;
	default:
		break;
	}
}
