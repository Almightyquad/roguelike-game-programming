#pragma once
#include "IParticleComponent.h"
#include "ParticleSystem.h"
#include <memory>

class ParticleComponent : public IParticleComponent, ParticleSystem
{
public:
	ParticleComponent(tinyxml2::XMLElement* xmlElement)
		: IParticleComponent(xmlElement), ParticleSystem(),
		amountPerSecond(0),
		maxLife(1),
		size(1.0),
		color(1.0, 1.0, 1.0, 1.0),
		gravity(true),
		gravityMultiplier(1.0),
		velocity(0.0, 0.0, 0.0)	{};

	~ParticleComponent();
	void init(Engine* parentEngine) override;
	void update(float deltaTime) override;
	void draw(glm::mat4 viewProjection);

private:
	float amountPerSecond;
	int maxLife;
	float size;
	glm::vec4 color;
	bool gravity;
	float gravityMultiplier;
	glm::vec3 velocity;
};