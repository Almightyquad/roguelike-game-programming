#include "AIKappa.h"
#include "ActorFactory.h"
#include "Message.h"
#include "Engine.h"
#include "Actor.h"

bool inits = false;
AIKappa::AIKappa(tinyxml2::XMLElement * xmlElement)
	: IAIComponent(xmlElement)
{
}

AIKappa::~AIKappa()
{
	parentEngine->increaseEnemiesKilled();
}

void AIKappa::update(float deltatime)
{
	PhysicsComponent* physicsSibling = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"));
	if (!inits)
	{
		physicsSibling->getB2Body()->SetLinearVelocity({ 250,0 });
		inits = true;
	}
	

	if (physicsSibling->wallCollision())
	{
		physicsSibling->flipVelocity();
	}

	this->postMessage(Message(dynamic_cast<IAIComponent*>(this), "velocityUpdate", 1*deltatime, dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getVelocity().y*deltatime));
	this->postMessage(Message(dynamic_cast<IAIComponent*>(this), "jump"));
}