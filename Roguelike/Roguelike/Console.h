#pragma once
#include <map>
#include <string>
/**
 * @class	Console
 *
 * @brief	The console.
 */
/*static*/ class Console
{
private:
	
	//Typedefining function pointer which takes an argument or several arguments
	//To add a new one, simply give it a fair name and add your arguments, then make a map of those functionpointers.
	typedef void(*strfuncptr)(std::string);
	typedef void(*intfuncptr)(int);
	typedef void(*floatfloatfuncptr)(float,float);
	/*	Map for storing function pointers
	example of usage:

	void myfunc(std::string stuff){	std::cout << stuff; }

	Console console;
	//storing a function pointer in the map
	console.funcs["func1"] = &myfunc;
	//Prints out say hello
	console.funcs["func1"]("say hello");

	*/
	//I will make functions soon, woop
	std::map<std::string, strfuncptr> strfuncs;
	std::map<std::string, intfuncptr> intfuncs;
	std::map<std::string, floatfloatfuncptr> floatfloatfuncs;



public:
	Console();
	~Console();
	bool stringParser(std::string inputstring, std::string &returnstring);
};

