#pragma once
#include "PhysicsComponent.h"

class PhysicsPlayer final : public PhysicsComponent
{
public:
	PhysicsPlayer(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};