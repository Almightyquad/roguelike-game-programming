#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "Font.h"
#include "Observable.h"

class Engine;
class GuiState;

/** A drop down menu. Used to display a lot of options when clicked. Clicking away hides them */
class DropDown : public IObservable
{
public:
	DropDown(tinyxml2::XMLElement *xmlElement);
	~DropDown();

	/** Updates this object. */
	void update();

	/**
	 * Determines if mouse click left is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Determines if mouse click right is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Determines if mouse release is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	void init(Engine* parentEngine);

	/**
	 * Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(GuiState& parentPtr);

	/**
	 * Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	GuiState* getParent();

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();
private:
	GuiState* parentGuiState;
	Engine* parentEngine;

	std::string name;
	std::string currentChoice;
	std::string defaultChoice;

	std::vector<std::string> choices;

	int numberOfChoices;

	bool open;
	bool closing;
	bool fontDisplay;

	tinyxml2::XMLElement *originNode;

	Transform transform;
	std::vector<Transform> choicesTransform;
	std::vector<Mesh*> choicesMesh;

	Font * font;
	Mesh *mesh;
	Shader *shader;

	Texture *defaultTexture;
	Texture *choiceTexture;
};

