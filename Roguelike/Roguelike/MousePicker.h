#pragma once
#include <glm\glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <glu.h>
#else
#include <GL\glew.h>
#endif
/**
 * @class	MousePicker
 *
 * @brief	The mouse picker.
 */
class MousePicker
{
public:

	/**
	 * @fn	MousePicker::MousePicker();
	 *
	 * @brief	Default constructor.
	 */
	MousePicker();

	/**
	 * @fn	MousePicker::~MousePicker();
	 *
	 * @brief	Destructor.
	 */
	~MousePicker();

	/**
	 * @fn	void MousePicker::calculateMouseInSpace(float mouseX, float mouseY);
	 *
	 * @brief	Calculates the mouse in space.
	 *
	 * @author	Snikur
	 * @date	12/17/15
	 *
	 * @param	mouseX	The mouse x coordinate.
	 * @param	mouseY	The mouse y coordinate.
	 * 			Sets position to mouse position on screen.
	 */
	void calculateMouseInSpace(float mouseX, float mouseY);

	/**
	 * @fn	const glm::vec3 MousePicker::getPosition() const;
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	const glm::vec3 getPosition() const;
private:
	glm::vec3 position;
};

