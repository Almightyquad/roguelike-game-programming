#include "Camera.h"

Camera::Camera(const glm::vec3 & pos, float fov, float aspect, float zNear, float zFar)
{
	projectionMatrix = glm::perspective(fov, aspect, zNear, zFar);
	this->position = pos;
	forward = glm::vec3(0, 0, -1);
	up = glm::vec3(0, 1, 0);
	viewMatrix = glm::mat4(glm::lookAt(position, position + forward, up));
}

void Camera::updateCamera(float fov, float aspect, float zNear, float zFar)
{
	projectionMatrix = glm::perspective(fov, aspect, zNear, zFar);
	viewMatrix = glm::mat4(glm::lookAt(position, position + forward, up));
}

glm::mat4 Camera::getViewProjection() const
{
	return projectionMatrix * viewMatrix;
}

glm::mat4 Camera::getViewMatrix() const
{
	return viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix() const
{
	return projectionMatrix;
}

void Camera::updateDirection(glm::vec2 rel, float deltaTime)
{
	if (rel.x >= -100.f && rel.x <= 100.f)
	{
		this->angle.x -= (float)rel.x * deltaTime;
	}
	if (rel.y >= -100.f && rel.y <= 100.f)
	{
		this->angle.y -= (float)rel.y * deltaTime;
	}

	this->forward = glm::vec3(cos(this->angle.y) * sin(this->angle.x), sin(this->angle.y), cos(this->angle.y) * cos(this->angle.x));
	this->right = glm::vec3(sin(this->angle.x - glm::half_pi<float>()), 0.f, cos(this->angle.x - glm::half_pi<float>()));
	this->up = glm::cross(this->right, this->forward);
	updateViewMatrix();
}

void Camera::updatePosition(bool forward, float direction)
{
	if (forward)
	{
		this->position += this->forward * direction;
	}
	else
	{
		this->position += this->right * direction;
	}
	updateViewMatrix();
}

void Camera::updateViewMatrix()
{
	viewMatrix = glm::mat4(glm::lookAt(position, position + forward, up));
}

void Camera::setPosition(glm::vec3 newPosition)
{
	this->position = newPosition;
	updateViewMatrix();
}

glm::vec3 Camera::getPosition()
{
	return this->position;
}

Camera * Camera::getCamera()
{
	return this;
}
