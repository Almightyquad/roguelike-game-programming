-- cvar is set from C app
io.write( "[LUA]: cvar = ", cvar, "\n" );

-- calling a C Function
var = testFunction( 6, 7 );
io.write( "[LUA]: Call to testFunction returned: ", var, "\n" );

-- set a global variable (which is then retrieved in C)
gvar = "Some string from LUA";