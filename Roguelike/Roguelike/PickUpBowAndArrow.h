#pragma once
#include "IPickUpComponent.h"

class PickUpBowAndArrow final : public IPickUpComponent
{
public:
	PickUpBowAndArrow(tinyxml2::XMLElement *xmlElement);
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
	virtual void fire(krem::Vector2f mousePos) override;
private:
	Timer arrowCooldown;
	int arrows;
};