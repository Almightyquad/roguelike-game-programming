#include "FontHandler.h"
#include "Engine.h"


FontHandler::FontHandler()
{

}


FontHandler::~FontHandler()
{
	fonts.clear();	
}

Font * FontHandler::loadFont(const std::string filePath)
{
	
	auto it = fonts.find(filePath);
	if (it == fonts.end())		//check if font already exists in map
	{
		this->fonts[filePath] = std::make_unique<Font>(filePath); //saves new font if the old one isn't already in the map
	}
	return fonts[filePath].get(); //returns old font if exists in map, new font if not.
}