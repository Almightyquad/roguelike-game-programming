#include "PhysicsHandler.h"

PhysicsHandler::PhysicsHandler() : IPhysicsHandler(),
	gravity(b2Vec2(0,-9.81f)),
	world(gravity)
{
	world.SetContactListener(&contactListener);
	this->scale = 1.0f;
}

PhysicsHandler::~PhysicsHandler()
{
	b2Body* body = world.GetBodyList();

	while (body)
	{
		b2Body* bodyToDelete = body;
		body = body->GetNext();
		world.DestroyBody(bodyToDelete);
	}
}

void PhysicsHandler::dump()
{
	world.Dump();
}

void PhysicsHandler::clear()
{
	
	b2Body* body = world.GetBodyList();
	
	while (body)
	{
		b2Body* bodyToDelete = body;
		body = body->GetNext();
		world.DestroyBody(bodyToDelete);
	}
}

void PhysicsHandler::applyTerrainCollision(std::vector<CollisionData> collisionDataVector)
{
	int** collisionDataArray;
	
	signed char** cellData;
	bool** cellDataTraversed;

	//this assumes that the final element of the vector is in the bottom right
	krem::Vector2i terrainSize{collisionDataVector.back().position.x+1,abs(collisionDataVector.back().position.y ) + 1 };

	collisionDataArray = new int*[terrainSize.y];
	cellData = new signed char*[terrainSize.y - 1];
	cellDataTraversed = new bool*[terrainSize.y - 1];

	for (auto i = 0; i < abs(terrainSize.y); i++)
	{
		collisionDataArray[i] = new int[terrainSize.x];
		if (i < abs(terrainSize.y))
		{
			cellData[i] = new signed char[terrainSize.x - 1];
			cellDataTraversed[i] = new bool[terrainSize.x - 1];
		}
	}

	//this is kind of slow, but shouldn't matter as it's only during init
	for (auto it : collisionDataVector)
	{
		//original value will be our iso-value
		collisionDataArray[abs(it.position.y)][it.position.x] = it.originalValue;
	}

	for (int i = 0; i < terrainSize.y; i++)
	{
		for (int j = 0; j < terrainSize.x; j++)
		{
			printf("TD: %i ", collisionDataArray[i][j]);
		}
		printf("\n");
	}

	for (int i = 0; i < terrainSize.y - 1; i++)
	{
		for (int j = 0; j < terrainSize.x - 1; j++)
		{
			cellDataTraversed[i][j] = false;
			cellData[i][j] = 0;
			cellData[i][j] += collisionDataArray[i][j];
			cellData[i][j] = cellData[i][j] << 1;
			cellData[i][j] += collisionDataArray[i][j+1];
			cellData[i][j] = cellData[i][j] << 1;
			cellData[i][j] += collisionDataArray[i+1][j + 1];
			cellData[i][j] = cellData[i][j] << 1;
			cellData[i][j] += collisionDataArray[i+1][j];
			printf("CD: %i ", cellData[i][j]);
		}
		printf("\n");
	}

	for (int i = 0; i < terrainSize.y - 1; i++)
	{
		for (int j = 0; j < terrainSize.x - 1; j++)
		{
			if (!cellDataTraversed[i][j])
			{
				if (cellData[i][j] != 15 && cellData[i][j] != 0)
				{
					b2Body* chainBody;
					b2BodyDef bodyDef;
					bodyDef.position = { static_cast<float>(j)-0.5f, -static_cast<float>(i) };
					bodyDef.type = static_cast<b2BodyType>(b2_staticBody);
					chainBody = world.CreateBody(&bodyDef);
					b2FixtureDef chainFixture;
					b2ChainShape chainShape;
					setCollisionCategory(&chainFixture, "Scenery");

					
					std::vector<b2Vec2> chainVectors = createChain(cellData, cellDataTraversed,  i, j);

					//b2Vec2* vectorArray;
					//vectorArray = new b2Vec2[chainVectors.size()];
					b2Vec2 vectorArray[150];
					
					for (int i = 0; i < chainVectors.size(); i++)
					{
						vectorArray[i] = chainVectors[i];
					}
					chainShape.CreateChain(vectorArray, chainVectors.size());
					chainFixture.shape = &chainShape;
					chainBody->CreateFixture(&chainFixture);
				}
				//cellDataTraversed[i][j] = true;
			}
		}
	}

}

void PhysicsHandler::update(float deltaSeconds)
{
	world.Step(deltaSeconds, 8, 3);
	
	//world.Dump();
	//printf("(x: %f, y: %f)\n", world->GetBodyList()->GetPosition().x, world->GetBodyList()->GetPosition().y);
}

b2Body* PhysicsHandler::createBox(glm::vec2 position, PhysicsDefinition physicsDefinition)
{
	
	b2Body* body;
	b2BodyDef bodyDef;
	b2FixtureDef fixtureDef;
	b2MassData massData;

	b2PolygonShape polygonShape; //shapes have to be declared before switch
	b2CircleShape circleShape;
	
	bodyDef.position = { position.x, position.y };
	bodyDef.fixedRotation = physicsDefinition.fixedRotation;
	bodyDef.type = static_cast<b2BodyType>(physicsDefinition.bodyType); //0=static 1=dynamic 2=kinematic
	
	body = world.CreateBody(&bodyDef);
	//body->GetMassData(&massData);


	fixtureDef.density = physicsDefinition.density;  // Sets the density of the body
	fixtureDef.restitution = physicsDefinition.restitution; //sets bouncyness
	fixtureDef.friction = physicsDefinition.friction; 
	
	
	switch (physicsDefinition.shape) //0=box/rectangular 1=circle 2=custom vertices
	{
	case physicsDefinition.BOX:
		polygonShape.SetAsBox((physicsDefinition.size.x / 2) / scale, (physicsDefinition.size.y / 2) / scale); // Creates a box shape. Divide your desired width and height by 2.
		fixtureDef.shape = &polygonShape; // Sets the shape
		//massData.center = { 0,0 };
		break;
	case physicsDefinition.CIRCLE:
		circleShape.m_radius = (physicsDefinition.size.x / 2 / scale);
		fixtureDef.shape = &circleShape; // Sets the shape
		//body->GetMassData(&massData);
		//massData.center.x = 0.25f;
		//massData.center.y = -0.f;

		break;
	case physicsDefinition.POLYGON: //some default options in case someone really fucks up. obviously needs work if we need this
		polygonShape.SetAsBox((physicsDefinition.size.x / 2) / scale, (physicsDefinition.size.y / 2) / scale); // Creates a box shape. Divide your desired width and height by 2.
		fixtureDef.shape = &polygonShape; // Sets the shape
		std::printf("You sir, fucked up. check your physicsdefinition.shape(tried to create a custom shape before implementing it, OR forgot to remove this message after implementing it!)\n");
		break;
	default:
		break;
	}
	fixtureDef.density = physicsDefinition.density;  //Sets the density of the body
	setCollisionCategory(&fixtureDef, physicsDefinition.category);
	 // Apply the fixture definition

	body->CreateFixture(&fixtureDef);
	//entities with a jump ability will have a small box below them which will act as a sensor to see how many objects the "feet" collide with. jumps if that number > 0
	if (physicsDefinition.jumpSensor)
	{
		polygonShape.SetAsBox((physicsDefinition.size.x / 2.f)*scale-0.1f, 0.1f, b2Vec2(0, (-physicsDefinition.size.y/2.f)*scale), 0);
		fixtureDef.isSensor = true;
		fixtureDef.shape = &polygonShape;
		b2Fixture* footSensorFixture = body->CreateFixture(&fixtureDef);
		footSensorFixture->SetUserData((void*)"jump");
	}

	if (physicsDefinition.wallSensor)
	{
		polygonShape.SetAsBox(0.08f*scale, (physicsDefinition.size.y /2.f)*scale - 0.05f, b2Vec2((-physicsDefinition.size.x / 2.f)*scale, 0), 0);
		fixtureDef.isSensor = true;
		fixtureDef.shape = &polygonShape;
		b2Fixture* wallLeftSensorFixture = body->CreateFixture(&fixtureDef);
		wallLeftSensorFixture->SetUserData((void*)"wallLeft");
		
		polygonShape.SetAsBox(0.08f*scale, (physicsDefinition.size.y  / 2.f)*scale - 0.05f, b2Vec2((physicsDefinition.size.x/2.f)*scale, 0), 0);
		fixtureDef.isSensor = true;
		fixtureDef.shape = &polygonShape;
		b2Fixture* wallRightSensorFixture = body->CreateFixture(&fixtureDef);
		wallRightSensorFixture->SetUserData((void*)"wallRight");
	}

	if (physicsDefinition.category == "PickUp")
	{
		fixtureDef.isSensor = true;
		b2Fixture* pickUpSensorFixture = body->CreateFixture(&fixtureDef);
		pickUpSensorFixture->SetUserData((void*)"pickUpSensor");
	}
	if (!physicsDefinition.gravity)
	{
		body->SetGravityScale(0);
	}
	return body;
}

std::vector<b2Vec2> PhysicsHandler::createChain(signed char ** cellData, bool** cellDataTraversed,int i, int j)
{
	b2Vec2 newVector;
	b2Vec2 originPoint = { 0,0 };
	b2Vec2 direction = { 0,1 };
	std::vector<b2Vec2> returnVector;
	returnVector.push_back(originPoint);
	
	//half of the cases are handled the same. may change in the future if we use isobands for filling
	// the != operator is not oveloaded for b2Vec2, which explains why the if-statements are stupid
	do
	{
		cellDataTraversed[i][j] = true;
		switch (cellData[i][j])
		{
		case 1:
		case 14: //same as 1
			if (direction.x > 0)
			{
				returnVector.back().x += 0.5;
				newVector = returnVector.back();
				newVector.y -= 0.5;
				returnVector.push_back(newVector);
				direction = { 0,-1 };
				if (!(newVector == originPoint))
				{
					i += 1;
				}
			}
			if (direction.y > 0)
			{
				returnVector.back().y += 0.5;
				newVector = returnVector.back();
				newVector.x -= 0.5;
				returnVector.push_back(newVector);
				direction = { -1,0 };
				if (!(newVector == originPoint))
				{
					j -= 1;
				}
			}
			break;			
		case 2:
		case 13: //same as 2
			if (direction.x < 0)
			{
				returnVector.back().x -= 0.5;
				newVector = returnVector.back();
				newVector.y -= 0.5;
				returnVector.push_back(newVector);
				direction = { 0,-1 };
				if (!(newVector == originPoint))
				{
					i += 1;
				}
			}
			if (direction.y > 0)
			{
				returnVector.back().y += 0.5;
				newVector = returnVector.back();
				newVector.x += 0.5;
				returnVector.push_back(newVector);
				direction = { 1,0 };
				if (!(newVector == originPoint))
				{
					j += 1;
				}
			}
			break;
		case 3:
		case 12: //same as 3
			if (direction.x > 0)
			{
				returnVector.back().x += 1;

				if (!(newVector == originPoint))
				{
					j += 1;
				}
			}
			if (direction.x < 0)
			{
				returnVector.back().x -= 1;
				if (!(newVector == originPoint))
				{
					j -= 1;
				}
			}
			break;
		case 4:
		case 11: //same as 4
			if (direction.x < 0)
			{
				returnVector.back().x -= 0.5;
				newVector = returnVector.back();
				newVector.y += 0.5;
				returnVector.push_back(newVector);
				direction = { 0,1 };
				if (!(newVector == originPoint))
				{
					i -= 1;
				}
			}
			if (direction.y < 0)
			{
				returnVector.back().y -= 0.5;
				newVector = returnVector.back();
				newVector.x += 0.5;
				returnVector.push_back(newVector);
				direction = { 1,0 };
				if (!(newVector == originPoint))
				{
					j += 1;
				}
			}
			break;
		case 5: //no handling for 5 and 10. they are also not symetrical like the other "pairs".
			if (direction.x > 0)
			{
				returnVector.back().x += 0.5;
				newVector = returnVector.back();
				newVector.y += 0.5;
				returnVector.push_back(newVector);
				direction = { 0,1 };
				if (!(newVector == originPoint))
				{
					i -= 1;
				}
			}
			else if (direction.y < 0)
			{
				returnVector.back().y -= 0.5;
				newVector = returnVector.back();
				newVector.x -= 0.5;
				returnVector.push_back(newVector);
				direction = { -1,0 };
				if (!(newVector == originPoint))
				{
					j -= 1;
				}
			}
			else if (direction.x < 0)
			{
				returnVector.back().x -= 0.5;
				newVector = returnVector.back();
				newVector.y -= 0.5;
				returnVector.push_back(newVector);
				direction = { 0,-1 };
				if (!(newVector == originPoint))
				{
					i += 1;
				}
			}
			else if (direction.y > 0)
			{
				returnVector.back().y += 0.5;
				newVector = returnVector.back();
				newVector.x += 0.5;
				returnVector.push_back(newVector);
				direction = { 1,0 };
				if (!(newVector == originPoint))
				{
					j += 1;
				}
			}
			break;
		case 10:
			if (direction.x < 0)
			{
				returnVector.back().x -= 0.5;
				newVector = returnVector.back();
				newVector.y += 0.5;
				returnVector.push_back(newVector);
				direction = { 0,1 };
				if (!(newVector == originPoint))
				{
					i -= 1;
				}
			}
			else if (direction.y < 0)
			{
				returnVector.back().y -= 0.5;
				newVector = returnVector.back();
				newVector.x += 0.5;
				returnVector.push_back(newVector);
				direction = { 1,0 };
				if (!(newVector == originPoint))
				{
					j += 1;
				}
			}
			else if (direction.x > 0)
			{
				returnVector.back().x += 0.5;
				newVector = returnVector.back();
				newVector.y -= 0.5;
				returnVector.push_back(newVector);
				direction = { 0,-1 };
				if (!(newVector == originPoint))
				{
					i += 1;
				}
			}
			else if (direction.y > 0)
			{
				returnVector.back().y += 0.5;
				newVector = returnVector.back();
				newVector.x -= 0.5;
				returnVector.push_back(newVector);
				direction = { -1,0 };
				if (!(newVector == originPoint))
				{
					j -= 1;
				}
			}
			break;
		case 6:
		case 9: //same as 6
			if (direction.y < 0)
			{
				returnVector.back().y -= 1;
				if (!(newVector == originPoint))
				{
					i += 1;
				}
			}
			if (direction.y > 0)
			{
				returnVector.back().y += 1;

				if (!(newVector == originPoint))
				{
					i -= 1;
				}
			}
			break;
		case 7:
		case 8: //same as 7
			if (direction.x > 0)
			{
				returnVector.back().x += 0.5;
				newVector = returnVector.back();
				newVector.y += 0.5;
				returnVector.push_back(newVector);
				direction = { 0,1 };
				if (!(newVector == originPoint))
				{
					i -= 1;
				}
			}
			if (direction.y < 0)
			{
				returnVector.back().y -= 0.5;
				newVector = returnVector.back();
				newVector.x -= 0.5;
				returnVector.push_back(newVector);
				direction = { -1,0 };
				if (!(newVector == originPoint))
				{
					j -= 1;
				}
			}
			break;
		default:
			break;
		}
	} while (!(returnVector.back() == originPoint));

	return returnVector;
}

void PhysicsHandler::setCollisionCategory(b2FixtureDef *fixtureDef, std::string category)
{
	//bits = what category the object is
	//mask = which categories the category should collide with 
	//to make objects collide with them self just add it's own category to the mask
	//put -1 to collide with everything
	//If something should collide both categories needs to have the other category in their mask
	if (category == "Player")
	{
		fixtureDef->userData = ((void*)CATEGORY_PLAYER);
		fixtureDef->filter.categoryBits = CATEGORY_PLAYER;
		fixtureDef->filter.maskBits =  CATEGORY_ENEMY | CATEGORY_ITEM | CATEGORY_ENEMY_PROJECTILE | CATEGORY_PICKUP | CATEGORY_SCENERY;
	}
	if (category == "Enemy")
	{
		fixtureDef->userData = ((void*)CATEGORY_ENEMY);
		fixtureDef->filter.categoryBits = CATEGORY_ENEMY;
		fixtureDef->filter.maskBits = CATEGORY_PLAYER | CATEGORY_ITEM | CATEGORY_PROJECTILE | CATEGORY_SCENERY | CATEGORY_MELEE;
	}
	if (category == "Item")
	{
		fixtureDef->userData = ((void*)CATEGORY_ITEM);
		fixtureDef->filter.categoryBits = CATEGORY_ITEM;
		fixtureDef->filter.maskBits =  CATEGORY_ENEMY | CATEGORY_SCENERY;
	}
	if (category == "Projectile")
	{
		fixtureDef->userData = ((void*)CATEGORY_PROJECTILE);
		fixtureDef->filter.categoryBits = CATEGORY_PROJECTILE;
		fixtureDef->filter.maskBits = CATEGORY_ENEMY | CATEGORY_SCENERY;
	}
	if (category == "ProjectileEnemy")
	{
		fixtureDef->userData = ((void*)CATEGORY_ENEMY_PROJECTILE);
		fixtureDef->filter.categoryBits = CATEGORY_ENEMY_PROJECTILE;
		fixtureDef->filter.maskBits = CATEGORY_PLAYER | CATEGORY_SCENERY;
	}
	if (category == "Pickup")
	{
		fixtureDef->userData = ((void*)CATEGORY_PICKUP);
		fixtureDef->filter.categoryBits = CATEGORY_PICKUP;
		fixtureDef->filter.maskBits = CATEGORY_PLAYER | CATEGORY_SCENERY;	
	}
	if (category == "Scenery")
	{
		fixtureDef->userData = ((void*)CATEGORY_SCENERY);
		fixtureDef->filter.categoryBits = CATEGORY_SCENERY;
		fixtureDef->filter.maskBits = -1;
	}

	if (category == "Melee")
	{
		fixtureDef->userData = ((void*)CATEGORY_MELEE);
		fixtureDef->filter.categoryBits = CATEGORY_MELEE;
		fixtureDef->filter.maskBits = CATEGORY_ENEMY;
	}

}

b2RevoluteJoint* PhysicsHandler::addJoint(b2Body* bodyA, b2Body* bodyB)
{
	glm::vec3 yo;
	b2RevoluteJointDef revoluteJointDef;
	revoluteJointDef.bodyA = bodyA;
	revoluteJointDef.bodyB = bodyB;
	revoluteJointDef.collideConnected = false;
	
	revoluteJointDef.localAnchorA.Set(0.3f, -0.3f);
	revoluteJointDef.localAnchorB.Set(0.f, -0.4f);
	revoluteJointDef.referenceAngle = 0.f * (3.14159288f / 180.f);
	revoluteJointDef.enableLimit = true;
	revoluteJointDef.upperAngle = 5.f * (3.14159288f / 180.f);
	revoluteJointDef.lowerAngle = -80.f*(3.14159288f/180.f);
	
	revoluteJointDef.motorSpeed = 360.f* (3.14159288f / 180.f);
	revoluteJointDef.maxMotorTorque = 20;
	revoluteJointDef.enableMotor = true;
	return (b2RevoluteJoint*)world.CreateJoint(&revoluteJointDef);
}