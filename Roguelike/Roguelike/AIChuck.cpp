#include "AIChuck.h"
#include "ActorFactory.h"
#include "Message.h"
#include "Engine.h"
#include "Actor.h"

AIChuck::~AIChuck()
{
	parentEngine->increaseEnemiesKilled();
}

void AIChuck::update(float deltatime)
{
	if (player == nullptr)
	{
		player = parentEngine->getPlayer();
	}

	if (scriptPath != "")
	{
		glm::vec3 parentActorPosition = dynamic_cast<PhysicsComponent*>(parentActor->getComponent("PhysicsComponent"))->getPosition();
		lua_getglobal(luaState, "normalizeVector");

		lua_pushnumber(luaState, dynamic_cast<PhysicsComponent*>(player->getComponent("PhysicsComponent"))->getPosition().x);
		lua_pushnumber(luaState, dynamic_cast<PhysicsComponent*>(player->getComponent("PhysicsComponent"))->getPosition().y);
		lua_pushnumber(luaState, parentActorPosition.x);
		lua_pushnumber(luaState, parentActorPosition.y);
		lua_pushnumber(luaState, deltatime);
		if (lua_pcall(luaState, 5, 3, 0) != 0)
		{
			if (luaDebug)
			{
				printf("error calling luafunction normalizeVector:%s\n", lua_tostring(luaState, -1));
			}
			else
			{
				lua_tostring(luaState, -1);
			}
		}
		else
		{
			std::string newActorPath = lua_tostring(luaState, -1);
			if (lua_type(luaState, -1) == LUA_TSTRING)
			{
				newActorPath = lua_tostring(luaState, -1);
			}
			int test = lua_gettop(luaState);
			if (newActorPath != "")
			{
				printf("throwing rock\n");
				Actor* newActor = parentEngine->getActorFactory()->createActor(newActorPath, glm::vec3{ parentActorPosition.x, parentActorPosition.y + 0.5f, 0.f });
				dynamic_cast<PhysicsComponent*>(newActor->getComponent("PhysicsComponent"))->setVelocity(glm::vec3{ lua_tonumber(luaState,-3), lua_tonumber(luaState,-2),0 });
			}
			lua_pop(luaState, lua_gettop(luaState));
		}
	}
}

void AIChuck::onDeath()
{
	printf("killed chuck\n");
}