#include "ShaderHandler.h"
#include "Engine.h"

ShaderHandler::ShaderHandler()
{
}


ShaderHandler::~ShaderHandler()
{
	shaders.clear();
}

Shader* ShaderHandler::loadShader(const std::string & filePath)
{
	std::unordered_map<std::string, std::unique_ptr<Shader>>::iterator it = shaders.find(filePath);
	if (it == shaders.end())
	{
#ifdef __ANDROID__
		krem::android::AssetManager * mgr = parentEngine->getAssetMgr();
		shaders[filePath] = std::make_unique<Shader>(filePath, mgr);
#else
		std::cout << "Shader: " << filePath << std::endl;
		shaders[filePath] = std::make_unique<Shader>(filePath);
#endif
	}
	return shaders[filePath].get();
}