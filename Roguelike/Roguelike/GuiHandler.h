#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include "GuiState.h"
#include "tinyxml2-master\tinyxml2.h"
#include "common.h"
#include "Handler.h"

class GuiState;
class Engine;
/**
 * @class	GuiHandler
 *
 * @brief	A graphical user interface handler.
 */
class GuiHandler : public Handler
{
public:
	GuiHandler();
	~GuiHandler();

	/** Loads gui states from files. */
	void loadStatesFromFiles();

	/**
	 * Creates a gui state.
	 *
	 * @param	xmlPath	Full pathname of the XML file.
	 */
	void createState(std::string xmlPath);

	/**
	 * Draws.
	 *
	 * @param	gamestate	The gamestate.
	 */
	void draw(int gamestate);

	/** Updates this object. */
	void update();

	/**
	 * Displays a container found by name.
	 *
	 * @param	name	The name.
	 */
	void displayContainer(std::string name);

	/**
	 * Sets bar value found by name.
	 *
	 * @param	name	The name.
	 * @param	size	The size.
	 */
	void setBarValue(std::string name, float size);

	/**
	 * Sets bar maximum value found by name.
	 *
	 * @param	name	The name.
	 * @param	size	The size.
	 */
	void setBarMaxValue(std::string name, float size);

	/**
	 * Checks if any elements in the current state is clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Checks if any element in the current state is right clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Checks if any element in the current state has the mouse released inside of them
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * Gets a bar.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the bar.
	 */
	Bar* getBar(std::string name);

	/**
	 * Gets a button.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the button.
	 */
	Button* getButton(std::string name);

	/**
	 * Gets a slider.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the slider.
	 */
	Slider* getSlider(std::string name);

	/**
	 * Gets text box.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the text box.
	 */
	TextBox* getTextBox(std::string name);

	/**
	 * Gets check box.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the check box.
	 */
	CheckBox* getCheckBox(std::string name);

	/**
	 * Gets drop down.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the drop down.
	 */
	DropDown* getDropDown(std::string name);

	/**
	 * Gets a container.
	 *
	 * @param	name	The name.
	 *
	 * @return	null if it fails, else the container.
	 */
	Container* getContainer(std::string name);
	void clear(int gameState);
private:
	void addChild(GuiState* guiState);
	std::string loaderFile;
	tinyxml2::XMLDocument * doc;
	int lastGuiId;
	int getNextGuiId(void);
	std::vector<std::unique_ptr<GuiState>> states;
};