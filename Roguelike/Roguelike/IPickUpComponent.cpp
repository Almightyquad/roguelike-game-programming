#include "IPickUpComponent.h"
#include "Actor.h"
#include "Engine.h"

IPickUpComponent::IPickUpComponent(tinyxml2::XMLElement * xmlElement) :
	ActorComponent(xmlElement),
	pickedUp(false),
	active(false),
	spawnTimer(0.5),
	owner(nullptr)
{
}

IPickUpComponent::~IPickUpComponent()
{
}

void IPickUpComponent::init(Engine * parentEngine)
{
	this->parentEngine = parentEngine;
	if (this->originNode->FirstChildElement("active"))
	{
		this->originNode->FirstChildElement("active")->QueryBoolText(&active);
	}
	else if (xmlDebug)
	{
		printf("Missing active\n");
	}

	if (this->originNode->FirstChildElement("savable"))
	{
		this->originNode->FirstChildElement("savable")->QueryBoolText(&saveable);
	}
	else if (xmlDebug)
	{
		printf("Missing savableItem\n");
	}
	if (saveable)
	{
		if (this->originNode->FirstChildElement("stackable"))
		{
			this->originNode->FirstChildElement("stackable")->QueryBoolText(&stackable);
		}
		else if (xmlDebug)
		{
			printf("Missing stackable\n");
		}
	}
	spawnTimer.start();
}

void IPickUpComponent::onPickup(Actor * newOwner)
{
	owner = newOwner;
}

void IPickUpComponent::onToss()
{
	owner = nullptr;
	active = false;
}

void IPickUpComponent::setPickUpStatus(bool status)
{
	pickedUp = status;
}

bool IPickUpComponent::checkSavable()
{
	if (spawnTimer.hasEnded())
	{
		return saveable;
	}
	return false;
}

bool IPickUpComponent::getStackable()
{
	return stackable;
}

void IPickUpComponent::restartSpawnTimer()
{
	spawnTimer.restart();
}

Actor * IPickUpComponent::getOwner()
{
	return owner;
}


