#include "Engine.h"
#include <memory>


#include "PickUpDoubleSpeed.h"
#include "PickUpNewspaper.h"
#include "PickUpTuna.h"
#include "PickUpFireball.h"
#include "PickUpBowAndArrow.h"
#include "PickUpGrenade.h"
#include "PickUpSword.h"
#include "PickUpManaPotion.h"
#include "PickUpHealthPotion.h"

#include "AIRabbit.h"
#include "AIChuck.h"
#include "AIEsmeralda.h"
#include "AIKappa.h"

#include "PhysicsEnemy.h" 
#include "PhysicsPlayer.h"
#include "PhysicsPickup.h"
#include "PhysicsProjectile.h"
#include "PhysicsProjectileEnemy.h"
#include "PhysicsScenery.h"
#include "PhysicsMelee.h"

#include "InputPlayer.h"
#include "InputPickup.h"

int main(int argc, char* args[])
{

	std::unique_ptr<Engine> engine = std::make_unique<Engine>();
	engine->init();
	
	PickUpCreatorImplementation<PickUpDoubleSpeed> pickupCreator0("PickupDoubleSpeed",engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpNewspaper> pickupCreator1("PickupNewspaper", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpTuna> pickupCreator2("PickupTuna", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpFireball> pickupCreator3("PickupFireball", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpBowAndArrow> pickupCreator4("PickupBowAndArrow", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpGrenade> pickupCreator5("PickupGrenade", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpSword> pickupCreator6("PickupSword", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpHealthPotion> pickupCreator7("PickupHealthPotion", engine->getActorFactory()->getPickUpFactory());
	PickUpCreatorImplementation<PickUpManaPotion> pickupCreator8("PickupManaPotion", engine->getActorFactory()->getPickUpFactory());

	AICreatorImplementation<AIRabbit> AICreator1("Rabbit", engine->getActorFactory()->getAIFactory());
	AICreatorImplementation<AIChuck> AICreator2("Chuck", engine->getActorFactory()->getAIFactory());
	AICreatorImplementation<AIEsmeralda> AICreator3("Esmeralda", engine->getActorFactory()->getAIFactory());
	AICreatorImplementation<AIKappa> AICreator4("Kappa", engine->getActorFactory()->getAIFactory());
	
	PhysicsCreatorImplementation<PhysicsPlayer> PhysicsCreator0("Player", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsEnemy> PhysicsCreator1("Enemy", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsPickup> PhysicsCreator2("Pickup", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsProjectile> PhysicsCreator3("Projectile", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsProjectileEnemy> PhysicsCreator4("ProjectileEnemy", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsScenery> PhysicsCreator5("Scenery", engine->getActorFactory()->getPhysicsFactory());
	PhysicsCreatorImplementation<PhysicsMelee>PhysicsCreator6("Melee", engine->getActorFactory()->getPhysicsFactory());

	InputCreatorImplementation<InputPlayer> InputCreator0("Player", engine->getActorFactory()->getInputFactory());
	InputCreatorImplementation<InputPickup> InputCreator1("Pickup", engine->getActorFactory()->getInputFactory());

	engine->run();
	return 0;
}

