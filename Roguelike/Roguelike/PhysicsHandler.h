#pragma once
#include "IPhysicsHandler.h"
#include "box2d\box2d.h"
#include <glm\glm.hpp>
#include "common.h"
#include "ContactListener.h"
/**
 * @class	PhysicsHandler
 *
 * @brief	The physics handler.
 */
class PhysicsHandler : public IPhysicsHandler
{
public:
	PhysicsHandler();
	~PhysicsHandler() override;
	void update(float deltaSeconds) override;
	b2Body* createBox(glm::vec2 position, PhysicsDefinition physicsDefinition);
	b2RevoluteJoint* addJoint(b2Body* bodyA, b2Body* bodyB);
	void dump();
	void clear();
	//Uses the marching squares algorithm. We don't use linear interpolation at the end because we want our ground to feel edgy, and not rounded. We also don't cut corners.
	void applyTerrainCollision(std::vector<CollisionData> collisionDataVector);
	
	//Requires an at least one vertex BEFORE the function call (in the chainVector) as a reference point.
	std::vector<b2Vec2> createChain(signed char** cellData, bool** cellDataTraversed,int i, int j);
	void setCollisionCategory(b2FixtureDef *fixtureDef, std::string category);
private:
	
	ContactListener contactListener;
	float scale;
	b2Vec2 gravity;
	b2World world;
};
