#pragma once
#include "ActorComponent.h"
#include "Timer.h"

/**
* @class	IPickUPComponent
*
* @brief	The pickup component interface. Used by game objects that can be picked up by actors
*/
class IPickUpComponent : public ActorComponent
{
public:
	IPickUpComponent(tinyxml2::XMLElement *xmlElement);
	~IPickUpComponent();
	virtual void init(Engine* parentEngine);
	virtual void onPickup(Actor* newOwner);
	virtual void onToss();
	virtual void onActivation() {}
	virtual void onDeactivation() {};
	virtual void onUse() {};
	virtual void fire(krem::Vector2f mousePos) {};
	//virtual void stopFire(krem::Vector2f mousePos) {};
	//virtual void postPickUpUpdate();

	/**
	 * Sets pick up status.
	 *
	 * @param	status	true to status.
	 */
	void setPickUpStatus(bool status);

	/**
	 * Determines if we can save the item that is picked up.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkSavable();

	/**
	 * Checks if the item is stackable. CURRENTLY NOT IN USE
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool getStackable();

	void restartSpawnTimer();
	Actor* getOwner();
protected:
	Actor* owner;
	bool pickedUp;
	bool active;
	bool saveable;
	bool stackable;
	Timer spawnTimer;
};