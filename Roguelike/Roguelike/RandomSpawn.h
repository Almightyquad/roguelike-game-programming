#pragma once
#include <vector>
#include <string>
#include <glm\glm.hpp>
#include "Handler.h"

class RandomSpawn : public Handler
{
public:
	RandomSpawn();
	~RandomSpawn();
	bool spawnRandomEnemy(std::string actorToSpawn);
	bool spawnRandomItem(std::string actorToSpawn);
};

