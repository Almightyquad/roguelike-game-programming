#pragma once
#include <unordered_map>
#include <memory>
#include "tinyxml2-master\tinyxml2.h"
#include <iostream>
#include <string>
#include "Handler.h"
#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#endif
/**
 * @class	XmlHandler
 *
 * @brief	An XML handler.
 */
using namespace tinyxml2;
class XmlHandler : public Handler
{
public:
	XmlHandler();
	~XmlHandler();
	XMLDocument* loadXml(const std::string& filePath);
#ifdef __ANDROID__
	const std::string getXmlString(std::string path, krem::android::AssetManager * assetmgr);
#endif
private:
	std::unique_ptr<XMLDocument> doc;
	std::unordered_map<std::string, std::unique_ptr<XMLDocument>> xmlDocuments;
};