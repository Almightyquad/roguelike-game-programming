#include "PickUpSword.h"
#include "Engine.h"

PickUpSword::PickUpSword(tinyxml2::XMLElement * xmlElement)
	: IPickUpComponent(xmlElement),
	swordCooldown(1.f)
{
}

void PickUpSword::onToss()
{
	owner = nullptr;
	active = false;
}

void PickUpSword::onActivation()
{
	if (active)
	{
		return;
	}
	active = true;
	ownerPhysicsComponent = dynamic_cast<PhysicsComponent*>(owner->getComponent("PhysicsComponent"));
	krem::Vector2f swordSize{ 0.2,0.05 };
	float ownerVelX = ownerPhysicsComponent->getVelocity().x;
	krem::Vector2f swordPos;
	if (ownerVelX == 0)
	{
		swordPos = { 1.0f, 0.3f };
	}
	else
	{
		float ownerVelXDirectionalNormalized = ownerVelX / abs(ownerVelX);
		swordPos = { ownerVelXDirectionalNormalized, -0.2f };
	}
	
	swordPos.x += ownerPhysicsComponent->getPosition().x;
	swordPos.y += ownerPhysicsComponent->getPosition().y;
	
	
	b2Body* ownerBody = ownerPhysicsComponent->getB2Body();
	Actor* swordActor = parentEngine->getActorFactory()->createActor("res/actors/sword.xml", glm::vec3(ownerBody->GetPosition().x+ 0.2f, ownerBody->GetPosition().y - 0.3f, 0.01f));
	b2Body* swordBody = dynamic_cast<PhysicsComponent*>(swordActor->getComponent("PhysicsComponent"))->getB2Body();
	swordPhysicsComponent = dynamic_cast<PhysicsComponent*>(swordActor->getComponent("PhysicsComponent"));

	swordJoint = parentEngine->getPhysicsHandler()->addJoint(ownerBody, swordBody);
}

void PickUpSword::onDeactivation()
{

	//active = false;
}

void PickUpSword::fire(krem::Vector2f mousePos)
{
	if (parentEngine->getJoining())
	{
		return;
	}
	float max = 5.f * (krem::PI / 180.f);//krem::degToRad();
	float min = -85 * (krem::PI / 180.f);//degToRad();
	if (active)// && swordCooldown.hasEnded())
	{
		swordJoint->EnableMotor(true);
	/*	swordPhysicsComponent->getB2Body()->SetAngularVelocity(-krem::PI);
		if (swordPhysicsComponent->getB2Body()->GetAngle() < min)
		{
			swordPhysicsComponent->setRotation(max);*/

			//}
			//swordPhysicsComponent->setRotation(attackAngle*(krem::PI / 180.f));//krem::degToRad());
			//attackAngle*(krem::PI/180.f) > min ?
			//	attackAngle-- :
			//	attackAngle = max;
			//printf("attackangle: %f\n", attackAngle);
			//swordCooldown.restart();
	}
}