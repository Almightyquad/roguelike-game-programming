#include "PickUpFireball.h"
#include "Engine.h"

PickUpFireball::PickUpFireball(tinyxml2::XMLElement * xmlElement)
	: IPickUpComponent(xmlElement),
	fireballCooldown(0.15f)
{
}

void PickUpFireball::onToss()
{
	owner = nullptr;
	active = false;
}

void PickUpFireball::onActivation()
{
	active = true;
	dynamic_cast<InventoryComponent*>(owner->getComponent("InventoryComponent"))->setActive(this);
}

void PickUpFireball::onDeactivation()
{
	active = false;
}

void PickUpFireball::fire(krem::Vector2f mousePos)
{
	if (parentEngine->getJoining())
	{
		return;
	}
	if (active && fireballCooldown.hasEnded())
	{
		if (owner != nullptr)
		{
			if (dynamic_cast<CombatComponent*>(owner->getComponent("CombatComponent"))->getMana() >= manaCost)
			{
				dynamic_cast<CombatComponent*>(owner->getComponent("CombatComponent"))->reduceMana(manaCost);
				if (owner == parentEngine->getPlayer())
				{
					parentEngine->getGuiHandler()->setBarValue("manaBar", static_cast<float>(dynamic_cast<CombatComponent*>(owner->getComponent("CombatComponent"))->getMana()));
				}			
				float velocityMultiplier = 5.f;
				glm::vec3 ownerActorPosition = dynamic_cast<PhysicsComponent*>(owner->getComponent("PhysicsComponent"))->getPosition();
				glm::vec2 velocity = glm::normalize(glm::vec2({ mousePos.x , mousePos.y }));
				parentEngine->getActorFactory()->createActor("res/actors/fireball.xml", glm::vec3(ownerActorPosition), glm::vec3(velocity.x * velocityMultiplier, velocity.y * velocityMultiplier, 0));
				fireballCooldown.restart();
			}
			
		}
	}
}