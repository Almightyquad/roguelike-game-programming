#include "LevelLoader.h"

int getTilePos(int number, int position);
int chunkWidth = 8;

LevelLoader::LevelLoader()
{

}

void LevelLoader::setActorFactory(ActorFactory* actorFactory)
{
	this->actorFactory = actorFactory;
}

void LevelLoader::setPhysicsHandler(PhysicsHandler * physicsHandler)
{
	this->physicsHandler = physicsHandler;

}

void LevelLoader::loadLevel()
{
	krem::Vector2i chunkFiles{ 4,2 };
	int chunkVariations = 3;
	krem::Vector2i totalNumberChunks{ chunkFiles.x * 3, chunkFiles.y * 1 };

	std::string fileDir;
	std::string random;
	std::string mainChunk = "/main";
	std::string propsChunk = "/props";
	
	for (int i = 0; i < totalNumberChunks.y*chunkWidth+2; i++)
	{
		collisionDataVector.push_back(CollisionData{ { 0, -i }, 0.f });
	}
	for (int i = 1; i < totalNumberChunks.x*chunkWidth+1; i++)
	{
		collisionDataVector.push_back(CollisionData{ { i, 0}, 0.f });
	}

	for (int j = 0; j < totalNumberChunks.y; j++)
	{
		for (int i = 0; i < totalNumberChunks.x; i++)
		{
			std::string xNumber = std::to_string(i%chunkFiles.x);
			std::string yNumber = std::to_string(j%chunkFiles.y);

			random = std::to_string(rand() % chunkVariations);

			fileDir = "res/levels/chunk" + yNumber + xNumber + mainChunk + random + ".txt";
			printf("NAME = %s \n", fileDir.c_str());
#ifdef __ANDROID__
			loadChunk(fileDir, i, j, parentEngine->getAssetMgr());
#else
			loadChunk(fileDir, i, j);
#endif


			//uncomment to enable prop layer. needs proper design and has to be added. should stick to one layer
			//random = std::to_string(rand() % chunkVariations);

			//fileDir = "res/levels/chunk"  + yNumber + xNumber + propsChunk + xRandom + ".txt";
			//printf("NAME = %s \n", fileDir.c_str());
			//levelLoader.loadLevel(fileDir, i,j);
		}
	}
		
	for (int i = 1; i < totalNumberChunks.x*chunkWidth + 1; i++)
	{
		collisionDataVector.push_back(CollisionData{ { i, totalNumberChunks.y * chunkWidth + 1 }, 0.f });
	}

	for (int i = 0; i < totalNumberChunks.y*chunkWidth + 2; i++)
	{
		collisionDataVector.push_back(CollisionData{ { totalNumberChunks.x * chunkWidth + 1, -i }, 0.f });
	}

	physicsHandler->applyTerrainCollision(collisionDataVector);
}


#ifdef __ANDROID__
void LevelLoader::loadChunk(const std::string& filename, int chunkX, int chunkY, krem::android::AssetManager * assetmgr)
#else
void LevelLoader::loadChunk(const std::string& filename, int chunkX, int chunkY)
#endif
{
#ifdef __ANDROID__
	std::vector<char> tempStr = getLevelString(filename, assetmgr);
	char charBuffer[tempStr.size()];
	for (int i = 0; i < tempStr.size(); i++)
	{
		charBuffer[i] = tempStr[i];
	}
	levelfile.rdbuf()->pubsetbuf(charBuffer, tempStr.size());
	__android_log_print(ANDROID_LOG_VERBOSE, "RogueLike obj tempStr: ", "%d", (int)tempStr.size());
#else
	levelfile.open(filename);
#endif
	chunkNumberX = chunkX;
	chunkNumberY = chunkY;

#ifdef __ANDROID__
#else
	if (levelfile.is_open())
	{
#endif
		levelfile >> name;
		if (name == "[header]")
		{
			//set the levelWidth
			levelfile >> aux;
			aux.erase(0, 6);
			levelWidth = atoi(aux.c_str());
			//set the levelHeight
			levelfile >> aux;
			aux.erase(0, 7);
			levelHeight = atoi(aux.c_str());
			//set the tileWidth
			levelfile >> aux;
			aux.erase(0, 10);
			tileWidth = atoi(aux.c_str());
			//set the tileHeight
			levelfile >> aux;
			aux.erase(0, 11);
			tileHeight = atoi(aux.c_str());
		}
		else
		{
			printf("INVALID FILE READING");
		}

		while (!levelfile.eof())
		{
			levelfile >> name;

			while (name == "[layer]")
			{
				levelfile >> aux;
				aux.erase(0, 5);

				if (aux == "Foreground")
				{
					loadForeground();
				}
				else if (aux == "Collision")
				{
					loadCollision();
				}
				else if (aux == "Props")
				{
					loadProps();
				}
				else if (aux == "Background")
				{
					loadBackground();
				}
			}
		}
		for (std::size_t i = 0; i < collisionTilePositions.size(); i++)
		{
			for (std::size_t j = 0; j < tilePositions.size(); j++)
			{
				if (collisionTilePositions[i].x == tilePositions[j].x && collisionTilePositions[i].y == tilePositions[j].y)
				{
					tilePositions.erase(tilePositions.begin() + j);
				}
			}
		}
		collisionTilePositions.clear();

#ifdef __ANDROID__
	levelfile.clear();
#else
	}

	levelfile.close();
#endif
}

void LevelLoader::loadForeground()
{
	glm::vec2 position{ 0, 0 };
	levelfile >> name;
	if (name == "data=")
	{

		levelfile >> name;
		int y;
		for (y = 0; y < levelWidth; y++)
		{
			position.y = static_cast<float>(-y);
			int x = 0;

			for (std::size_t i = 0; i < name.length(); i++) {

				if (name.at(i) != ',')
				{
					tempString += name.at(i);

				}
				if (name.at(i) == ',' || (x == levelWidth - 1 && y == levelHeight - 1 && i == name.length() - 1))
				{

					position.x = static_cast<float>(x);
					content = atoi(tempString.c_str());
					if (content != 0) {
						actorFactory->createActor("res/actors/tileAtlases/castleBg.xml", glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.505f), glm::vec2(getTilePos(content, "x"), getTilePos(content, "y")));
						tilePositions.push_back(glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.505f));
					}



					tempString.clear();
					x++;
				}
			}

			levelfile >> name;
			//printf("\n");

		}	//printf("%s \n", name.c_str());
	}
	else
	{
		printf("INVALID FILE READING");
	}
}

void LevelLoader::loadBackground()
{
	glm::vec2 position{ 0, 0 };
	levelfile >> name;
	if (name == "data=")
	{

		levelfile >> name;
		int y;
		for (y = 0; y < levelWidth; y++)
		{
			position.y = static_cast<float>(-y);
			int x = 0;

			for (std::size_t i = 0; i < name.length(); i++) {

				if (name.at(i) != ',')
				{
					tempString += name.at(i);

				}
				if (name.at(i) == ',' || (x == levelWidth - 1 && y == levelHeight - 1 && i == name.length() - 1))
				{

					position.x = static_cast<float>(x);
					content = atoi(tempString.c_str());
					if (content != 0) {

						actorFactory->createActor("res/actors/tileAtlases/castleBg.xml", glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, -0.5f), glm::vec2(getTilePos(content, "x"), getTilePos(content, "y")));
						tilePositions.push_back(glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.5f));
					}
					tempString.clear();
					x++;
				}
			}

			levelfile >> name;


		}
	}
	else
	{
		printf("INVALID FILE READING");
	}

}

void LevelLoader::loadCollision()
{
	glm::vec2 position{ 0, 0 };
	levelfile >> name;
	if (name == "data=")
	{

		int y;
		for (y = 0; y < levelWidth; y++)
		{
			levelfile >> name;

			position.y = static_cast<float>(-y);
			int x = 0;

			for (std::size_t i = 0; i < name.length(); i++) {

				if (name.at(i) != ',')
				{
					tempString += name.at(i);

				}
				if (name.at(i) == ',' || (x == levelWidth - 1 && y == levelHeight - 1 && i == name.length() - 1))
				{

					position.x = static_cast<float>(x);
					content = atoi(tempString.c_str());
					if (content != 0) {

						actorFactory->createActor("res/actors/tileAtlases/castleFg.xml", glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.f), glm::vec2(getTilePos(content, "x"), getTilePos(content, "y")));
						collisionTilePositions.push_back(glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.f));
						collisionDataVector.push_back(CollisionData{ {static_cast<int>(position.x) + chunkNumberX * chunkWidth + 1, static_cast<int>(position.y) - chunkNumberY * chunkWidth - 1}, 1.f });
					}
					else
					{
						collisionDataVector.push_back(CollisionData{ { static_cast<int>(position.x) + chunkNumberX * chunkWidth + 1, static_cast<int>(position.y) - chunkNumberY * chunkWidth - 1}, 0.f });
					}
					tempString.clear();
					x++;
				}
			}

		}
	}
	else
	{
		printf("INVALID FILE READING");
	}
}

void LevelLoader::loadProps()
{
	glm::vec2 position{ 0, 0 };
	levelfile >> name;
	if (name == "data=")
	{

		levelfile >> name;
		int y;
		for (y = 0; y < levelWidth; y++)
		{
			position.y = static_cast<float>(-y);
			int x = 0;

			for (std::size_t i = 0; i < name.length(); i++) {

				if (name.at(i) != ',')
				{
					tempString += name.at(i);

				}
				if (name.at(i) == ',' || (x == levelWidth - 1 && y == levelHeight - 1 && i == name.length() - 1))
				{

					position.x = static_cast<float>(x);
					content = atoi(tempString.c_str());
					if (content != 0) {

						actorFactory->createActor("res/actors/tileAtlases/castleBg.xml", glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, -0.5f), glm::vec2(getTilePos(content, "x"), getTilePos(content, "y")));
						tilePositions.push_back(glm::vec3((position.x) + chunkNumberX * chunkWidth, position.y - chunkNumberY * chunkWidth, 0.5f));
					}
					tempString.clear();
					x++;
				}
			}

			levelfile >> name;


		}
	}
	else
	{
		printf("INVALID FILE READING");
	}
}





int LevelLoader::getTilePos(int number, std::string position)
{
	int returnValue;
	int tileWidth = 15;
	int tileHeight = 11;
	if (position == "x")
	{
		if ((number % tileWidth) == 0)
		{
			returnValue = tileWidth - 1;
			//printf("-- x = %d ", returnValue);
		}
		else
		{
			returnValue = (number % tileWidth) - 1;
			//printf("-- x = %d ", returnValue);
		}

	}
	else if (position == "y")
	{
		if ((number % tileWidth) == 0)
		{
			returnValue = (number / tileWidth) - 1;
			//printf("-- y = %d ", returnValue);
		}
		else
		{
			returnValue = (number / tileWidth);
			//printf("-- y = %d \n", returnValue);
		}
	}

	return returnValue;
}


#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
std::vector<char> LevelLoader::getLevelString(std::string path, krem::android::AssetManager * assetmgr)
{
	krem::android::AssetFile AF = assetmgr->open(path);


	std::vector<char> tempBuffer;

	tempBuffer = AF.readAll();

	return tempBuffer;
}
#endif