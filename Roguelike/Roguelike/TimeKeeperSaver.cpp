#include "TimeKeeperSaver.h"



TimeKeeperSaver::TimeKeeperSaver()
{
	positionVector.reserve(vectorLimit);
	rotationVector.reserve(vectorLimit);
}


TimeKeeperSaver::~TimeKeeperSaver()
{

}

void TimeKeeperSaver::init()
{
	
}

void TimeKeeperSaver::setPosition(glm::vec3 position)
{
	positionVector.emplace_back(position);
}

void TimeKeeperSaver::setRotation(float rotation)
{
	rotationVector.emplace_back(rotation);
}

void TimeKeeperSaver::removeTimeFrame(int timeFrame)
{
	if (timeFrame >= 0)
	{
		positionVector.erase(positionVector.begin() + timeFrame);
		rotationVector.erase(rotationVector.begin() + timeFrame);
	}
}

glm::vec3 TimeKeeperSaver::getPosition(int timeFrame)
{
	return positionVector[timeFrame];
}

float TimeKeeperSaver::getRotation(int timeFrame)
{
	return rotationVector[timeFrame];
}
