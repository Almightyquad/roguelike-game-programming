#pragma once
#include "IPhysicsComponent.h"
#include "Box2D\Box2D.h"
#include <math.h>

class PhysicsHandler;

struct SensorContacts
{
	int jump;
	int wallLeft;
	int wallRight;
};

/**
 * @class	PhysicsComponent
 *
 * @brief	The physics component.
 * @details	
 */
class PhysicsComponent : public IPhysicsComponent
{
public:
	PhysicsComponent(tinyxml2::XMLElement *xmlElement);
	~PhysicsComponent() override;
	void init(Engine* parentEngine) override;
	virtual void update(float deltaTime) override;
	void setPosition(glm::vec3 position);
	glm::vec3 getPosition();
	void setRotation(float angle);
	void setVelocity(glm::vec3 velocity);
	float getRotation();
	glm::vec2 getVelocity();

	
	void receiveMessage(const Message &message) override;

	/**
	* @fn	b2Fixture* applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void* fixturename, float angle = 0, bool sensor = false)
	*
	* @brief	Creates a new fixture which is applied to the body.
	* @return	Returns the created fixture
	*/
	b2Fixture* applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void* fixturename, float angle = 0, bool sensor = false);
	void startContact(void* sensorType);
	void endContact(void* sensorType);
	std::string getActorName();
	virtual void onCollision(int fixtureUserData, Actor* collidedActor) = 0;
	//int intCollision(std::string type);
	bool wallCollision();
	void flipVelocity();
	Actor* getParentActor();
	b2Body* getB2Body();
	
protected:
	SensorContacts sensorContacts;
	PhysicsDefinition physicsDefinition;
	PhysicsHandler *physicsHandler;
	b2Body* body;
	float defaultForce;
	float forceScale;
};