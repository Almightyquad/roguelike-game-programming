#pragma once

#include "common.h"
#include <SDL.h>
#include <memory>
#include <string>
#include "Transform.h"
#include "MousePicker.h"
#include "Handler.h"
#include "Observable.h"

/**
 * @class	EventHandler
 *
 * @brief	An event handler for window and keyboard/mouse events.
 * @details 
 */
class EventHandler : public Handler, public IObservable
{
public:
	EventHandler();
	~EventHandler();
	void handleEvents();
	void init();
	int handleKeyboardEvents(SDL_Event &SDLevent);
	void handleWindowEvents(SDL_Event &SDLevent);
	void getMousePosition(SDL_Event & SDLEvent);
	void handleMouseMotion(SDL_Event &SDLEvent);
	void handleMouseDown(SDL_Event &SDLEvent);
	void handleMouseUp(SDL_Event &SDLEvent);
	bool mouseInside(Transform trans);

	void handleConsoleTextActions(SDL_Event &SDLevent);
	void handleConsoleInputText(SDL_Event &SDLevent);

	void handleTextActions(SDL_Event &SDLevent);
	void handleInputText(SDL_Event &SDLevent);

	void swapConsoleStatus();
	void setInputStatus(bool status);
	glm::ivec2 getMousePos();
	glm::vec3 getMouseOpenGLPos();
	void mouseButtonLeftClicked();
	void mouseButtonLeftRelease();
	std::string getInputText();
private:
	glm::vec3 mousePos; //in 3d opengl space
	MousePicker mousePicker;
	int screenResolutionX, screenResolutionY;
	int mouseX, mouseY; //X Y position of mouse, have to be ints, in window pixels
	int prevMouseX, prevMouseY;
	bool consoleActive;
	bool inputActive;
	std::string inputText;
	std::string consoleInputText;

#ifdef __ANDROID__
	SDL_Point touchLocation;
#endif
};



