#include "ActorComponent.h"

ActorComponent::ActorComponent(tinyxml2::XMLElement * xmlElement)
	:alive(true)
{
	this->originNode = xmlElement;
}

ActorComponent::~ActorComponent(void)
{ 
}

void ActorComponent::setParent(Actor& parentPtr)
{
	this->parentActor = &parentPtr;
}

bool ActorComponent::isAlive()
{
	return alive;
}
