#include "Transform.h"

Transform::Transform(const glm::vec3 & pos, const glm::vec3 & rot, const glm::vec3 & scale) :
	pos(pos),
	rot(rot),
	scale(scale)
{
}

glm::mat4 Transform::getModel() const
{
	glm::mat4 posMatrix = glm::translate(pos);
	glm::mat4 scaleMatrix = glm::scale(scale);
	glm::mat4 rotXMatrix = glm::rotate(rot.x, glm::vec3(1, 0, 0));
	glm::mat4 rotYMatrix = glm::rotate(rot.y, glm::vec3(0, 1, 0));
	glm::mat4 rotZMatrix = glm::rotate(rot.z, glm::vec3(0, 0, 1));

	glm::mat4 rotMatrix = rotZMatrix * rotYMatrix * rotXMatrix;

	return posMatrix * rotMatrix * scaleMatrix;	//calculate model matrix (order important)
}


glm::vec3 & Transform::getPos()
{
	return pos;
}

glm::vec3 & Transform::getRot()
{
	return rot;
}

glm::vec3 & Transform::getScale()
{
	return scale;
}

void Transform::setRot(glm::vec3 rot)
{
	this->rot = rot;
	//might not be necessary, but it will remove errors when things rotate for a long time
	if (this->rot.x > glm::two_pi<float>() || this->rot.x < -glm::two_pi<float>())
		this->rot.x = 0;
	if (this->rot.y > glm::two_pi<float>() || this->rot.y < -glm::two_pi<float>())
		this->rot.y = 0;
	if (this->rot.z > glm::two_pi<float>() || this->rot.z < -glm::two_pi<float>())
		this->rot.z = 0;
}

void Transform::setScale(glm::vec3 scale)
{

	this->scale = scale;
}

void Transform::setPos(glm::vec3 pos)
{
	this->pos = pos;
}
