#pragma once
#include "ActorComponent.h"
//#include "Engine.h"
/**
* @class	InputComponent
*
* @brief	The input component interface. Primarily used by players and items
*/
class IInputComponent : public ActorComponent
{
public:
	IInputComponent(tinyxml2::XMLElement *xmlElement) : ActorComponent(xmlElement) {};
	virtual ~IInputComponent();
	void init(Engine* parentEngine) override;
	//virtual void update(float deltatime) = 0;
	virtual void receiveMessage(const Message &message) = 0;
	virtual void receiveNetworkInput(InputValues newInput) {};
	virtual InputValues getInputValues();

protected:
	glm::vec3 mousePos;
	bool inputChanged = false;
	InputValues input;
private:
};