#include "IAIComponent.h"
#include "ActorFactory.h"
#include "Message.h"
#include "Engine.h"
#include "Actor.h"

IAIComponent::IAIComponent(tinyxml2::XMLElement * xmlElement) :
	ActorComponent(xmlElement),
	scriptPath("")
{
}

IAIComponent::~IAIComponent()
{
	if (luaState)
	{
		lua_close(luaState);
	}
}

void IAIComponent::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	player = nullptr;

	if (this->originNode->FirstChildElement("scriptPath"))
	{
		this->scriptPath = this->originNode->FirstChildElement("scriptPath")->GetText();
		// Create a new state
		luaState = luaL_newstate();
		// Register Standard Libraries for LUA use
		luaL_openlibs(luaState);

		// Load the program
		if (luaL_loadfile(luaState, scriptPath.c_str()))
		{
			if (luaDebug)
			{
				printf("error: %s", lua_tostring(luaState, -1));
			}

		}
		lua_pcall(luaState, 0, 0, 0);
	}
	else if (xmlDebug)
	{
		printf("Missing scriptPath\n");
	}
}