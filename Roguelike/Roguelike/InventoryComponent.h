#pragma once
#include "IInventoryComponent.h"
#include "IPickUpComponent.h"

class InventoryComponent : public IInventoryComponent
{
public:
	InventoryComponent(tinyxml2::XMLElement *xmlElement);
	~InventoryComponent() override;
	void init(Engine* parentEngine) override;
	void update(float deltaTime) override;
	bool checkForFreeSpace();
	void addItem(Actor* pickUpActor);
	void receiveMessage(const Message &message);
	std::string getContainer();
	std::map<int, Actor*> getItems();
	void setActive(IPickUpComponent* itemToSetActive);
private:
	IPickUpComponent* activeItem;
	int maxNumberOfItems;
	int firstFreeSlot;
	bool drawable;
	bool dropable;
	bool preSetItems;
	std::map<int, Actor*> items;
	std::vector<Actor*> preSetItemsList;
	std::string containerString;
};

