#pragma once
#include "IAIComponent.h"

class AIRabbit final : public IAIComponent
{
public:
	AIRabbit(tinyxml2::XMLElement *xmlElement)
		: IAIComponent(xmlElement)
	{
		printf("yo\n");
	};
	~AIRabbit();
	void update(float deltaTime) override;
private:
	std::string scriptPath;
	//void(ActorFactory::*createActor) (const std::string xmlPath, const glm::vec3 position, const glm::vec3 velocity);
	lua_State* luaState;
	Actor* player;
	int noLuaParameters;
	int noLuaReturnValues;
};
