#include "DropDown.h"
#include "Engine.h"
#include "GuiState.h"
#include "Message.h"


DropDown::DropDown(tinyxml2::XMLElement *xmlElement)
{
	this->originNode = xmlElement;
	open = false;
	closing = false;
	fontDisplay = false;
}

DropDown::~DropDown()
{
}


void DropDown::update()
{
	const char * tempName = name.c_str();
	const char * tempChoice = currentChoice.c_str();

	//sends a message with the current selected option
	this->postMessage(Message(this, tempName, tempChoice));
}

bool DropDown::checkMouseClickLeft()
{
	if (open)
	{
		for (int i = 0; i < numberOfChoices; i++)
		{
			//checks is one of the options in the drop down is clicked. sets the current value to the new value if it is
			if (parentEngine->getEventHandler()->mouseInside(choicesTransform[i]))
			{
				currentChoice = choices[i];
				open = false;
				closing = true;
				return true;
			}
		}
		//if the mouse is outside the drop down the menu is closed and no new value is set
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			{
				open = false;
				return true;
			}
		}
	}
	//if the mouse click is inside the drop down. Open up the options
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		{
			open = true;
			return true;
		}	
	}	
	open = false;
	return false;
}

bool DropDown::checkMouseClickRight()
{
	return false;
}

bool DropDown::checkMouseRelease()
{
	if (closing)
	{
		for (int i = 0; i < numberOfChoices; i++)
		{
			if (parentEngine->getEventHandler()->mouseInside(choicesTransform[i]))
			{
				closing = false;
				return true;
			}
		}
	}
	
	return false;
}

void DropDown::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadFloat(U_SCALE, 1.5f);
	if (fontDisplay)
	{
		font->update(currentChoice, glm::vec3(0, 0, 0));
		shader->loadInt(U_TEXTURE1, 1);
		font->bind(1);
	}

	defaultTexture->bind(0);
	mesh->draw();

	if (open)
	{
		for (int i = 0; i < numberOfChoices; i++)
		{
			shader->loadTransform(choicesTransform[i], viewProjection);
			shader->loadInt(U_TEXTURE0, 0);
			shader->loadFloat(U_SCALE, 1.5f);
			if (fontDisplay)
			{
				font->update(choices[i], glm::vec3(0, 0, 0));
				shader->loadInt(U_TEXTURE1, 1);
				font->bind(1);
			}
			choiceTexture->bind(0);
			
			choicesMesh[i]->draw();
		}
	}

}

void DropDown::init(Engine* parentEngine)
{
	this->parentEngine = parentEngine;
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	tinyxml2::XMLElement *itemElement;

	std::string pathTemp;
	std::string meshPathTemp;

	
	if (this->originNode->FirstChildElement("name"))
	{
		name = this->originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (this->originNode->FirstChildElement("fontPath"))
	{
		std::string fontPathTemp = this->originNode->FirstChildElement("fontPath")->GetText();
		font = parentEngine->getFontHandler()->loadFont(fontPathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}

	if (this->originNode->FirstChildElement("positionX"))
	{
		this->originNode->FirstChildElement("positionX")->QueryFloatText(&tempPosX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}
	
	if (this->originNode->FirstChildElement("positionY"))
	{
		this->originNode->FirstChildElement("positionY")->QueryFloatText(&tempPosY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (this->originNode->FirstChildElement("widthInPixels"))
	{
		this->originNode->FirstChildElement("widthInPixels")->QueryFloatText(&tempWidth);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryFloatText(&tempHeight);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}
	
	if (this->originNode->FirstChildElement("meshVerticesPath"))
	{
		meshPathTemp = this->originNode->FirstChildElement("meshVerticesPath")->GetText();
		this->mesh = parentEngine->getMeshHandler()->loadModel(meshPathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (this->originNode->FirstChildElement("shaderPath"))
	{
		pathTemp = this->originNode->FirstChildElement("shaderPath")->GetText();
		this->shader = parentEngine->getShaderHandler()->loadShader(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (this->originNode->FirstChildElement("texturePath"))
	{
		pathTemp = this->originNode->FirstChildElement("texturePath")->GetText();
		this->defaultTexture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (this->originNode->FirstChildElement("textureChoicePath"))
	{
		pathTemp = this->originNode->FirstChildElement("textureChoicePath")->GetText();
		this->choiceTexture = parentEngine->getTextureHandler()->loadTexture(pathTemp);
	}
	else if (xmlDebug)
	{
		printf("Missing textureChoicePath\n");
	}

	if (this->originNode->FirstChildElement("defaultChoice"))
	{
		defaultChoice = this->originNode->FirstChildElement("defaultChoice")->GetText();
		currentChoice = defaultChoice;
	}
	else if (xmlDebug)
	{
		printf("Missing defaultChoice\n");
	}

	if (this->originNode->FirstChildElement("heightInPixels"))
	{
		this->originNode->FirstChildElement("heightInPixels")->QueryIntText(&numberOfChoices);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}
	
	transform.setPos(glm::vec3(tempPosX, tempPosY, 1.0f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));

	if (this->originNode->FirstChildElement("choices"))
	{
		std::string tempCheck;
		numberOfChoices = 0;
		itemElement = this->originNode->FirstChildElement("choices")->FirstChildElement("choice");
		if (this->originNode->FirstChildElement("choices")->FirstChildElement("choice"))
		{
		tempCheck = this->originNode->FirstChildElement("choices")->FirstChildElement("choice")->Value();
		while (tempCheck == "choice")
		{
			fontDisplay = true;
			numberOfChoices++;
			Transform tempTransform;
			Mesh *tempMesh = parentEngine->getMeshHandler()->loadModel(meshPathTemp);
			std::string tempChoice;

			tempChoice = itemElement->GetText();
			choices.emplace_back(tempChoice);

			tempTransform.setPos(glm::vec3(tempPosX, tempPosY - (tempHeight*numberOfChoices), 1.0f));
			tempTransform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
			choicesTransform.emplace_back(tempTransform);

			choicesMesh.emplace_back(tempMesh);

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				tempCheck = itemElement->Value();
			}
			else
			{
				tempCheck = "end of choices";
			}
			}
		}
		else if (xmlDebug)
		{
			printf("Missing choice\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing choices\n");
	}
}

void DropDown::setParent(GuiState& parentPtr)
{
	this->parentGuiState = &parentPtr;
}

GuiState* DropDown::getParent()
{
	return parentGuiState;
}

std::string DropDown::getName()
{
	return name;
}
