#pragma once
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <glu.h>
#else
#include <GL\glew.h>
#endif
#include <vector>
#include <glm\glm.hpp>

#include "Handler.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "Transform.h"

//TODO WIP not functional yet
class LightHandler : public Handler
{
public:

	/**
	 * @fn	LightHandler::LightHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	LightHandler();

	/**
	 * @fn	LightHandler::~LightHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~LightHandler();

	/**
	 * @fn	void LightHandler::init();
	 *
	 * @brief	Initialises this object.
	 *
	 */
	void init();

	/**
	 * @fn	void LightHandler::update(glm::mat4 view);
	 *
	 * @brief	Updates with the given view.
	 *
	 * @param	view	The view projection.
	 */
	void update(glm::mat4 view);

	/**
	 * @fn	void LightHandler::addLight(glm::vec3 position, float size);
	 *
	 * @brief	Adds a light to 'size'.
	 *
	 * @param	position	The position.
	 * @param	size		The radius.
	 */
	void addLight(glm::vec3 position, float size);

	/**
	 * @fn	GLuint LightHandler::getTexture();
	 *
	 * @brief	Gets the texture.
	 *
	 * @return	The texture.
	 */
	GLuint getTexture();
private:
	void createFBO();
	void createTextureAttachment(int width, int height);
	void bindFramebuffer();

	std::vector<glm::vec4>lights;
	glm::vec2 resolution;

	GLuint fbo;
	GLuint texture;
	Texture *lightTexture;
	Mesh *mesh;
	Shader *shader;
};

