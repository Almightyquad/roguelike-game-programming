#include "InputFactory.h"

std::unique_ptr<IInputComponent> InputFactory::create(const std::string & itemName, tinyxml2::XMLElement* xmlElement)
{
	auto it = table.find(itemName);
	if (it != table.end())
	{
		return it->second->create(xmlElement);
	}
	else
	{
		return nullptr;
	}
}

void InputFactory::registerIt(const std::string & itemName, InputCreator * pickUpCreator)
{
	table[itemName] = pickUpCreator;
}