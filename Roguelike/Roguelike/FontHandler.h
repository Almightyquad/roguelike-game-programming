#pragma once
#include <SDL_ttf.h>
#include <iostream>
#include <unordered_map>
#include <memory>
#include "Font.h"
#include "Handler.h"
#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#endif
class FontHandler : public Handler
{
public:

	/**
	 * @fn	FontHandler::FontHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	FontHandler();

	/**
	 * @fn	FontHandler::~FontHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~FontHandler();

	/**
	 * Loads a font using a path.
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	pointer to font.
	 */
	Font *loadFont(const std::string filePath);
private:
	std::unordered_map<std::string, std::unique_ptr<Font>> fonts;
};

