#pragma once
#include <string>
#include <iostream>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#else
#include <GL\glew.h>
#endif
#include "Transform.h"
#include "common.h"
/**
 * @class	Shader
 *
 * @brief	A shader.
 * @details	Holds a spesific shader for later use to prevent loading same files multiple times.
 */
class Shader
{
public:
#ifdef __ANDROID__
	Shader(const std::string & fileName, krem::android::AssetManager * assetmgr);
#else

	/**
	 * @fn	Shader::Shader(const std::string& fileName);
	 *
	 * @brief	Constructor.
	 *
	 * @param	fileName	Filepath of the file.
	 */
	Shader(const std::string& fileName);
#endif

	/**
	 * @fn	virtual Shader::~Shader();
	 *
	 * @brief	Destructor.
	 */
	virtual ~Shader();

	/**
	 * @fn	GLuint Shader::createShader(const std::string& text, GLenum shaderType);
	 *
	 * @brief	Creates a shader.
	 *
	 * @param	text	  	The filepath.
	 * @param	shaderType	Type of the shader.
	 */
	GLuint createShader(const std::string& text, GLenum shaderType);

	/**
	 * @fn	void Shader::bind();
	 *
	 * @brief	Binds this object.
	 */
	void bind();

	//Loads uniforms
	void loadTransform(const Transform& transform, const glm::mat4 viewProjection);
	void loadMat4(int location, glm::mat4 matrix);
	void loadInt(int location, int integer);
	void loadFloat(int location, float floating);
	void loadVec4(int location, glm::vec4 vector);
	void loadVec2(int location, glm::vec2 vector);



protected:
private:
	enum
	{
		VERT,
		FRAG,
		NUM_SHADERS
	};
	GLuint program; //shader program
	GLuint shaders[NUM_SHADERS];
	GLuint uniforms[NUM_UNIFORMS];

	/**
	 * @fn	std::string Shader::loadShader(const std::string & filename);
	 *
	 * @brief	Loads a shader.
	 *
	 * @param	filename	Filepath of the file.
	 *
	 * @return	The shader.
	 */
	std::string loadShader(const std::string & filename);

	/**
	 * @fn	void Shader::checkShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string & errorMessage);
	 *
	 * @brief	Check shader error.
	 *
	 * @param	shader			The shader.
	 * @param	flag			The flag.
	 * @param	isProgram   	true if this object is program.
	 * @param	errorMessage	Message describing the error.
	 */
	void checkShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string & errorMessage);
#ifdef __ANDROID__
	const std::string getShaderString(std::string path, krem::android::AssetManager * assetmgr);
#endif
	//	Shader(const Shader& other) {}
	//void operator=(const Shader& other) {};
};