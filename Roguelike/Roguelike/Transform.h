#pragma once
#include <glm/glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <glm/gtx/transform.hpp>
/**
 * @class	Transform
 *
 * @brief	Form for viewing the transaction. 
 */
class Transform
{
public:

	/**
	 * @fn	Transform::Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale);
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos  	The position.
	 * @param	rot  	The rotation.
	 * @param	scale	The scale.
	 */
	Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale);

	/**
	 * @fn	Transform::Transform()
	 *
	 * @brief	Default constructor.
	 */
	Transform() { rot = glm::vec3(0, 0, 0); };

	/**
	 * @fn	glm::mat4 Transform::getModel() const;
	 *
	 * @brief	Gets the model.
	 *
	 * @return	The model matrix.
	 */
	glm::mat4 getModel() const;

	glm::vec3& getPos();
	glm::vec3& getRot();
	glm::vec3& getScale();

	void setPos(glm::vec3 pos);
	void setRot(glm::vec3 rot);
	void setScale(glm::vec3 scale);

protected:
private:
	glm::vec3 pos = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 rot = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);
};