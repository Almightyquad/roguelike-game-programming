#pragma once

#include "Variable.h"
#include <vector>

class Message
{
public:
	//Regular constructor initialising the message with the provided arguments
	template<typename T, typename ... Args>
	Message(T* sender, Args ... args) : senderType(typeid(T))
	{
		this->sender		= static_cast<void*>(sender);
		this->variables		= std::vector<Variable>{ std::forward<Args>(args)... };
	}

	Message(const Message&)					= delete;
	Message& operator=(const Message &)		= delete;

	//Move constructor
	Message(Message &&);

	//Releases the allocated memory for this message
	~Message();

	//The sender of this message
	void*				getSender()					const;

	//The type of the sender
	const std::type_info&	getSenderType()				const;

	//The number of variables passed to the constructor
	size_t				getVariableCount()			const;

	//Retrieves the variable at the given index
	const Variable&		getVariable(size_t index)	const;
	
private:
	void* 					sender;			//Pointer to the object that sent the message
	const std::type_info&		senderType;		//Type of the sender

	std::vector<Variable>	variables;		//The list of variables passed along the message (The first should ALWAYS be a string like "NextLevel")	
};