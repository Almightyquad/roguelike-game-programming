#include "GraphicsHandler.h"

GraphicsHandler::GraphicsHandler(const std::string& windowname)
{
	init(windowname);
}

GraphicsHandler::~GraphicsHandler()
{
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	
	SDL_Quit();
	Mix_Quit();
	TTF_Quit();
}

void GraphicsHandler::swapBuffers()
{
	SDL_GL_SwapWindow(window);
}

void GraphicsHandler::clearWindow()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

Camera * GraphicsHandler::getCamera()
{
	return mainCamera.get();
}

glm::vec2 GraphicsHandler::getDesktopResolution()
{
	SDL_DisplayMode current;
	for (int i = 0; i < SDL_GetNumVideoDisplays(); i++)
	{
		int success = SDL_GetCurrentDisplayMode(i, &current);
		return glm::vec2(current.w, current.h);
	}
	std::cout << "Something went wrong in getDesktopResolution." << std::endl;
	return glm::vec2(1.f, 1.f);
}


glm::vec2 GraphicsHandler::getWindowResolution()
{
	int x, y;
	SDL_GetWindowSize(this->window, &x, &y);
	return glm::vec2(x, y);
}

void GraphicsHandler::setCameraDirection(glm::vec2 position, float dt)
{
	mainCamera->updateDirection(position, dt);
}

void GraphicsHandler::setCameraPosition(bool horizontalDirection, float forwardMotion)
{
	mainCamera->updatePosition(horizontalDirection, forwardMotion);
}

void GraphicsHandler::setCameraPosition(glm::vec3 cameraPosition)
{
	mainCamera->setPosition(cameraPosition);
}

void GraphicsHandler::updateWindow(Uint32 flag)
{
	glm::vec2 tmp = getDesktopResolution();
	if (flag == 0)
	{
		SDL_SetWindowSize(this->window, static_cast<int>(tmp.x / 2.f), static_cast<int>(tmp.y / 2.f));
		SDL_SetWindowPosition(this->window, static_cast<int>(tmp.x / 4.f), static_cast<int>(tmp.y / 4.f));
		SDL_SetWindowFullscreen(this->window, flag);
		getCamera()->updateCamera(70.f, (tmp.x /2.f) / (tmp.y/2.f), 0.1f, 1000.f);
		return;
	}
	if (flag == SDL_WINDOW_BORDERLESS)
	{
		SDL_SetWindowPosition(this->window, 0, 0);
		SDL_SetWindowSize(this->window, static_cast<int>(tmp.x), static_cast<int>(tmp.y));
	}

	SDL_SetWindowFullscreen(this->window, flag);
	getCamera()->updateCamera(70.f, tmp.x / tmp.y, 0.1f, 1000.f);
}

void GraphicsHandler::init(const std::string& windowName)
{
	if (SDL_Init(SDL_INIT_EVERYTHING))
	{
		printf("SDL failed to initialize.\n");
	}
	else
	{
#ifdef __ANDROID__
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);		//allocate 8 bits per color
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);	//allocate buffer for RGBA (8x4=32)
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE;

#ifdef __ANDROID__
		SDL_DisplayMode displayMode;
		if (SDL_GetCurrentDisplayMode(0,&displayMode) == 0)
		resolution.x = displayMode.w;
		resolution.y = displayMode.h;
		window = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, (int)resolution.x, (int)resolution.y, flags);

#else
		resolution = getDesktopResolution();
		resolution *= 0.66f;	//scale down window to 66% of the monitor to make debugging easier

		//ask OS for a window
		window = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int)resolution.x, (int)resolution.y, flags);
#endif


		if (window == NULL)
		{
			printf("Error: %s. Unable to create window\n", SDL_GetError());
		}

		//give window to GPU
		glContext = SDL_GL_CreateContext(window);
#ifdef ANDROID
#else
		GLenum status = glewInit();
		if (status != GLEW_OK)
		{
			std::cerr << "Glew failed to initialize!" << std::endl;
		}
#endif

		if (TTF_Init() == -1)
		{
			printf("SDL TTF failed to initialize.\n");
		}

		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		}

		//create camera
		mainCamera = std::make_unique<Camera>(glm::vec3(0, 10, 0), 70.0f, resolution.x / resolution.y, 0.01f, 1000.f);

		//vsync (0/1)
		SDL_GL_SetSwapInterval(1);

		/***************/
		// OpenGL inits
		/***************/

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_2D);

		glClearColor(0.0f, 0.15f, 0.3f, 0.0f);
		glCullFace(GL_BACK);

		std::string vendor = (const char*)glGetString(GL_VENDOR);
		std::string renderer = (const char*)glGetString(GL_RENDERER);
		std::string version = (const char*)glGetString(GL_VERSION);

		std::cout << "vendor: " << vendor << std::endl;
		std::cout << "renderer: " << renderer << std::endl;
		std::cout << "version: " << version << std::endl;
	}
}