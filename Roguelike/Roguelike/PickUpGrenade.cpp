#include "PickUpGrenade.h"
#include "Engine.h"

PickUpGrenade::PickUpGrenade(tinyxml2::XMLElement * xmlElement)
	: IPickUpComponent(xmlElement),
	grenadeTimer(1.f)
{
}

void PickUpGrenade::onToss()
{
	//owner = nullptr;
	active = false;
}

void PickUpGrenade::onActivation()
{
	active = true;
	dynamic_cast<InventoryComponent*>(owner->getComponent("InventoryComponent"))->setActive(this);
}

void PickUpGrenade::onDeactivation()
{
	active = false;
}

void PickUpGrenade::fire(krem::Vector2f mousePos)
{
	//if (active) 
	//{
	//	int velocityMultiplier = 50;
	//	glm::vec3 ownerActorPosition = dynamic_cast<PhysicsComponent*>(owner->getComponent("PhysicsComponent"))->getPosition();
	//	grenadeActor = parentEngine->getActorFactory()->createActor("res/actors/grenade.xml", glm::vec3(ownerActorPosition), glm::vec3(mousePos.x * velocityMultiplier, mousePos.y * velocityMultiplier, 0));
	//	//onToss();
	//	grenadeTimer.start();
	//}
	//if (grenadeTimer.hasEnded())
	//{
	//	glm::vec3 grenadeActorPosition = dynamic_cast<PhysicsComponent*>(grenadeActor->getComponent("PhysicsComponent"))->getPosition();
	//	parentEngine->getActorFactory()->createActor("res/actors/grenadeExplosion.xml", glm::vec3(grenadeActorPosition));
	//}
}
