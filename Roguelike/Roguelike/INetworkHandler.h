#pragma once
#include "Handler.h"
#include "common.h"

class INetworkHandler : public Handler
{
public:
	INetworkHandler() {};
	~INetworkHandler() {};
	//look for clients
	virtual void sendPacket(int gameState) = 0;
	virtual void receivePacket(int gameState) = 0;
protected:
	IPaddress serverIP;
	
	std::vector<Actor*> oldActors;
};

