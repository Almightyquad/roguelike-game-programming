#include "PickUpHealthPotion.h"
#include "Engine.h"

void PickUpHealthPotion::onPickup(Actor * newOwner)
{
	dynamic_cast<CombatComponent*>(newOwner->getComponent("CombatComponent"))->increaseHealth(healthValue);
	if (newOwner == parentEngine->getPlayer())
	{
		parentEngine->getGuiHandler()->setBarValue("healthBar", dynamic_cast<CombatComponent*>(newOwner->getComponent("CombatComponent"))->getHealth());
	}
}
