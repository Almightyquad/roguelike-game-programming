#pragma once
#include "IPickUpComponent.h"

class PhysicsComponent;
class b2Body;
class b2RevoluteJoint;
class PickUpSword final : public IPickUpComponent
{
public:
	PickUpSword(tinyxml2::XMLElement *xmlElement);
	virtual void onToss() override;
	virtual void onActivation() override;
	virtual void onDeactivation() override;
	virtual void fire(krem::Vector2f mousePos) override;

private:
	Timer swordCooldown;
	PhysicsComponent* ownerPhysicsComponent;
	PhysicsComponent* swordPhysicsComponent;
	b2Body* swordBody;
	float attackAngle = 0;
	b2RevoluteJoint* swordJoint;
	//b2Fixture* swordFixture;
};