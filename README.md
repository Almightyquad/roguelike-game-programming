Read Me

Instructions to compile on Android. (Currently only on windows, adapt steps for linux and mac for yourself. Too be honest, I don't know if it compiles on Linux or Mac)

Windows:

1: Download the [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) DON'T INSTALL YET

2: Download the [Android NDK](http://developer.android.com/tools/sdk/ndk/index.html) DON'T INSTALL YET

3: Download [Apache Ant](https://ant.apache.org/bindownload.cgi) DON'T INSTALL YET

4: Download [Android SDK Manager](http://developer.android.com/sdk/index.html) DON'T INSTALL YET

5: Install and extract all the things and place them in a folder where you'd like them to be. Like: "C:\AndroidDevComp". Don't use spaces in your path to them. The SDK takes a lot of space, so make sure that you save it on a disk with some space. So you probably shouldn't download it to your boot SSD if you have one.

6: Run SDK Manager and install "Android SDK Tools, Android SDK Platform Tools and Android SDK Build-Tools", under API 22, check "SDK Plathform and all system images, especially Intel and ARM", in the extras check "Google USB Driver" Downloading these can take some time, so do it early.

7: Go to environment variables (control panel -> system -> advanced system settings -> environment variables), and under "System variables" select the path variable and press edit. This is so we can run ndk-build, abd or ant commands. The OS should look for ndk-build.cmd, ant.bat or adb.exe. If you use the "C:\AndroidDevComp", they should be at:
ndk-build.cmd - C:\AndroidDevComp\android-ndk-r10e
adb.exe - C:\AndroidDevComp\AndroidSDK
ant.bat - C:\AndroidDevComp\apache-ant-1.9.6/bin

Note: You may have different versions, it should still work just fine. Just make sure that all the files mentioned above are actually there. If not, just find the folder where they are.

Make sure that the paths you add to "Path" system variable look like this:

"C:\AndroidDevComp\android-ndk-r10e;C:\AndroidDevComp\AndroidSDK\platform-tools;C:\AndroidDevComp\apache-ant-1.9.6\bin;" 

Don't remove anything from the path.

8: Add a "JAVA_HOME" in "System variables" by pressing new. Variable name should be JAVA_HOME and variable value should be C:\Program Files\Java\jdk1.8.0_65 

Make sure you add the jdk not the jre.

9: add a ANDROID_HOME environment variable, this can be in the User variables section. The path should be to: C:\AndroidDevComp\AndroidSDK

10: Move the folders in "Static-libs" in the repo to "C:\AndroidDevComp\android-ndk-r10e\sources\mylibs". You need to make a folder named mylibs. It should now look like this for example: C:\AndroidDevComp\android-ndk-r10e\sources\mylibs\glues

11: You may have a problem with the USB driver for your phone. So you'll need to install the usb driver for it. Check out number 14 at: http://lazyfoo.net/tutorials/SDL/52_hello_mobile/android_windows/index.php 

If you have any other errors check out this: http://lazyfoo.net/tutorials/SDL/52_hello_mobile/android_windows/index.php 

Or send a mail to christerpsomby@gmail.com or add me on skype at quadakachrister. Make sure to mark the contact request or mail with a reference to the android project. Like [Plue And Bink Android].

To build the project navigate to the android-project folder. Shift right click and choose open command window. Or just open a cmd window and cd to it. Then write ndk-build (if you have multiple cores you can add -jX , where X is the number of threads you want to run the compilation on. Example ndk-build -j4)

Then to deploy it to the phone call "ant debug install"