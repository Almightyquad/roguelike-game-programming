
# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information


NDK_TOOLCHAIN_VERSION := 4.9
#APP_CFLAGS := -D__GXX_EXPERIMENTAL_CXX1X__
APP_CPPFLAGS := -std=c++14 -fexceptions -frtti 
APP_STL := gnustl_static

APP_ABI := armeabi armeabi-v7a
APP_PLATFORM := android-22
#APP_ABI := all
