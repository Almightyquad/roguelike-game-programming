LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL2
ENGINE_PATH := $(LOCAL_PATH)/../../../Roguelike/Roguelike
ANDROID_PATH := $(ENGINE_PATH)/android
LUA_PATH := $(ENGINE_PATH)/tinyxml2-master

FILE_LIST := $(filter-out $(ENGINE_PATH)/LightHandler.cpp $(ENGINE_PATH)/NetworkClientHandler.cpp $(ENGINE_PATH)/NetworkClientHandler.cpp $(ENGINE_PATH)/NetworkServerHandler.cpp $(ENGINE_PATH)/TimeKeeperSaver.cpp, $(wildcard $(ENGINE_PATH)/*.cpp))
FILE_LIST += $(wildcard $(ANDROID_PATH)/*.cpp)
FILE_LIST += $(wildcard $(LUA_PATH)/*.cpp)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(LOCAL_PATH)/../SDL2_mixer $(LOCAL_PATH)/../SDL2_ttf $(GLM_HOME) 

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	$(FILE_LIST:$(LOCAL_PATH)/%=%)
	
	
	
	
LOCAL_STATIC_LIBRARIES := glues
LOCAL_STATIC_LIBRARIES += lua
LOCAL_SHARED_LIBRARIES := SDL2 SDL2_mixer SDL2_ttf box2d
//LOCAL_STATIC_LIBRARIES := glues

LOCAL_LDLIBS := -llog -lEGL -lGLESv3
LOCAL_LDLIBS +=  -landroid

include $(BUILD_SHARED_LIBRARY)
$(call import-module,mylibs/lua)
$(call import-module,mylibs/glues)