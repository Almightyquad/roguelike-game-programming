eggCooldown = 3 --constant

timeSinceLastEgg = 3 --not constant
movingRight = true

function normalizeVector (playerX, playerY, aiX, aiY, deltaTime )
	
	spawnEgg = false
	timeSinceLastEgg = timeSinceLastEgg + deltaTime
	vectorX = playerX - aiX
	vectorY = playerY - aiY
    --math.pow() doesn't seem to work for whatever reason, so USE MANUAL EXPONENTIAL CALCULATIONS
	a = vectorX*vectorX
    b = vectorY*vectorY 
	
	x = 0
	
	if (movingRight == true) then
		x = -1
		if (vectorX > 3) then
			movingRight = false
			x = 1
		end
	end
	
	if (movingRight == false) then
		x = 1
		if (vectorX < -3) then
			movingRight = true
			x = -1
		end
	end
	
	
	--not sure what the equivalent of && is in lua. will code like this for now to make sure it actually does what I want it to do
	if (timeSinceLastEgg >= eggCooldown and math.abs(vectorX) < 1) then
		spawnEgg = true
		timeSinceLastEgg = 0
	end
	
	magnitude = math.sqrt(a+b)
	if magnitude < 5 then
		normalizeX = vectorX/magnitude*2
		--normalizeY = y/magnitude
	else
		normalizeX = 0
		normalizeY = 0
	end
	
	
	if spawnEgg then
		return "./res/actors/projectile/testmeralda.xml", x, 0
	else
		return "", x, 0
	end
	
end