
function normalizeVector ( x1, y1, x2, y2 )
	
	vectorX = x1 - x2
	vectorY = y1 - y2
    a = vectorX*vectorX
    b = vectorY*vectorY
    --a = math.pow(vectorX,2)
    --b = math.pow(vectorY,2)
	magnitude = math.sqrt(a+b)
	if magnitude < 5 then
		normalizeX = vectorX/magnitude*2
		--normalizeY = y/magnitude
	else
		normalizeX = 0
		normalizeY = 0
	end
	return normalizeX, normalizeY
end

