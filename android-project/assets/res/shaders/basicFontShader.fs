#version 300 es

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform float scale;
float offset = (scale - 1.0) * 0.5;

void main()
{	
	vec4 button	= texture(texture0, texCoord);
	vec4 font = texture(texture1, (texCoord * scale) - offset);
	
	if (button.a <= 0.9)
	{
		discard;
	}
	
	color = mix(button, font, font.a);
}