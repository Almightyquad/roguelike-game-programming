#version 300 es

in vec4 colors;
out vec4 color;

void main()
{
	color = colors;
}