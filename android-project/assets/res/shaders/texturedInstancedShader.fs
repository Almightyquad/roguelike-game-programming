#version 300 es

in vec4 color;
in vec2 texCoord;

out vec4 colors;

uniform sampler2D texture0;

void main()
{
	vec4 texel = texture(texture0, texCoord);
	if (texel.a <= 0.1)
	{
		discard;
	}
	colors = color * texel;
}