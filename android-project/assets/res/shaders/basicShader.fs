#version 300 es

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;

void main()
{
	vec4 texel = texture(texture0, texCoord);
	if (texel.a <= 0.9)
		discard;
	
	color = texel;
}