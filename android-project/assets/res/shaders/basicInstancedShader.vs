#version 300 es

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 position;
layout(location = 2) in vec4 color;

out vec4 colors;

uniform mat4 transform;

void main()
{
	colors = color;
	gl_Position = transform * vec4(vertex + position, 1.0);
}