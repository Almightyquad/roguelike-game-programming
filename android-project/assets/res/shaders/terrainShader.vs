#version 300 es

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;

out vec4 vertexPosition;
out vec2 texCoordinate;
out vec4 vertexNormal;
out vec4 oLightPosition;

uniform mat4 transform;
uniform vec4 lightPosition;

void main()
{
	vertexPosition = transform * vec4(position, 1.0);
	texCoordinate = texCoords * 800.0;	//tile texture 800 times
	vertexNormal = vec4(normal, 0.0);
	oLightPosition = lightPosition;
	
	gl_Position = vertexPosition;
}