#version 300 es

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform vec2 size;
uniform int spriteNo;
uniform float scale;

float offset = (scale - 1.0) * 0.5;

void main()
{

	int columns = int(size.x / 1.f);
	float x = float(spriteNo % columns);
	float y = float(spriteNo / columns);
	
	vec4 button	= texture(texture0, (vec2(x, y) + vec2(1.f, 1.f) * texCoord) / size);
	vec4 font = texture(texture1, (texCoord * scale) - offset);
	
	if (button.a <= 0.9)
	{
		discard;
	}
	
	color = mix(button, font, font.a);
}