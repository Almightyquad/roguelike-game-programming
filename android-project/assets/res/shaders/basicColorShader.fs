#version 300 es

out vec4 color;

uniform vec3 colors;

void main()
{
	color = colors;
}